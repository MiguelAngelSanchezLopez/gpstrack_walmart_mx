//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    using System;
    
    public partial class Track_GetLastPositions_Result
    {
        public string Patente { get; set; }
        public string NombreTipoMovil { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Transportista { get; set; }
        public Nullable<int> Reporte { get; set; }
        public int Velocidad { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public int Direccion { get; set; }
        public string Cedis { get; set; }
        public string EnTiempo { get; set; }
        public string Retraso { get; set; }
        public string SinReporte { get; set; }
    }
}
