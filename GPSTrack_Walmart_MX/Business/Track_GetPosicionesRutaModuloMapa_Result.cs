//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    using System;
    
    public partial class Track_GetPosicionesRutaModuloMapa_Result
    {
        public Nullable<long> NroTransporte { get; set; }
        public Nullable<int> LocalDestino { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Patente { get; set; }
        public Nullable<int> IdTipoMovil { get; set; }
        public string NombreTipoMovil { get; set; }
        public Nullable<int> Velocidad { get; set; }
        public Nullable<int> Direccion { get; set; }
        public Nullable<int> Ignicion { get; set; }
        public string Puerta1 { get; set; }
        public string Temperatura1 { get; set; }
        public Nullable<decimal> Latitud { get; set; }
        public Nullable<decimal> Longitud { get; set; }
        public Nullable<long> IdEmbarque { get; set; }
    }
}
