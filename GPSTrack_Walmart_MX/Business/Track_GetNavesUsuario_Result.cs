//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    using System;
    
    public partial class Track_GetNavesUsuario_Result
    {
        public Nullable<int> Determinante { get; set; }
        public string IdNave { get; set; }
        public string IdOperacion { get; set; }
        public string Nomenclatura { get; set; }
        public string NomenclaturaSPT { get; set; }
        public string TipoDeterminante { get; set; }
        public string NombreNave { get; set; }
        public string IdCedis { get; set; }
        public string Tiponave { get; set; }
        public string NombreCedis { get; set; }
    }
}
