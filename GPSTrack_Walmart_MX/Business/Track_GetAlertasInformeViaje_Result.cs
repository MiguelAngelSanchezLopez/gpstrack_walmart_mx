//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    using System;
    
    public partial class Track_GetAlertasInformeViaje_Result
    {
        public Nullable<int> Id { get; set; }
        public Nullable<System.DateTime> FechaInicioAlerta { get; set; }
        public Nullable<System.DateTime> FechaHoraCreacion { get; set; }
        public Nullable<int> LocalDestino { get; set; }
        public string TextFechaCreacion { get; set; }
        public string PatenteTracto { get; set; }
        public string PatenteTrailer { get; set; }
        public string Velocidad { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string DescripcionAlerta { get; set; }
        public Nullable<int> Ocurrencia { get; set; }
        public string Puerta1 { get; set; }
        public string Temp1 { get; set; }
        public string Observaciones { get; set; }
        public Nullable<int> ZoneLocation { get; set; }
        public string NombreZona { get; set; }
        public string Tiempo { get; set; }
    }
}
