﻿using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class PaginasPorUsuario : OperacionesPaginaGeneral
    {
        ControladorPaginasPorUsuario controladorPaginasPorUsuario = new ControladorPaginasPorUsuario();
        ControladorUsuarios controladorUsuarios = new ControladorUsuarios();
        ControladorPerfiles controladorPerfiles = new ControladorPerfiles();
        ControladorPaginas controladorPaginas = new ControladorPaginas();
        ControladorPerfilesPorUsuario controladorPerfilesPorUsuario = new ControladorPerfilesPorUsuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);
            ObtenerInformacionTodosJSON();
        }

        public override void ObtenerInformacionTodosJSON()
        {

            contenedorJSONPaginasPorUsuario.Value = controladorPaginasPorUsuario.ObtenerTodosRegistrosPaginasPorUsuario();
            contenedorJSONUsuarios.Value = controladorUsuarios.ObtenerTodosRegistrosUsuarios();
            contenedorJSONPerfiles.Value = controladorPerfiles.ObtenerTodosRegistrosPerfil();
            contenedorJSONPaginas.Value = controladorPaginas.ObtenerTodosRegistrosPaginas();
            contenedorJSONPerfilesPorUsuario.Value = controladorPerfilesPorUsuario.ObtenerTodosRegistrosPerfilPorUsuario();
        }

        public void btnBorrarPaginasPorUsuario_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorUsuario.EliminarRegistroPaginasPorUsuarioPorId(Convert.ToInt32(hidden_idTemporalPaginaPorUsuario.Value));
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnActualizarPaginasPorUsuario_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorUsuario.InsertarRegistroPaginasPerfilesUsuario(hidden_IdsPaginasSeleccionadas.Value, hidden_IdsPerfilesSeleccionados.Value, hidden_IdsUsuariosSeleccionados.Value);
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnInsertarPaginasPorUsuario_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorUsuario.InsertarRegistroPaginasPorUsuarioPorId(hidden_JSONPaginaPorUsuarioTemporal.Value);
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnInsertarRegistrosPaginasPerfilesUsuario_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorUsuario.InsertarRegistroPaginasPerfilesUsuario(hidden_IdsPaginasSeleccionadas.Value, hidden_IdsPerfilesSeleccionados.Value, hidden_IdsUsuariosSeleccionados.Value);
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

    }
}