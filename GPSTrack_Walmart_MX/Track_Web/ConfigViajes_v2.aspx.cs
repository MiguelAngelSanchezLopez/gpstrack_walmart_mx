﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Track_Web.ControladoresBD;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class ConfigViajes_v2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            contenedorJSONViajesCreadosPorUsuario.Value = new ControladorViajesCreadosPorUsuario().ObtenerTodosViajesCreadosPorUsuario(Utilities.GetUsuarioSession(Session));
            contenedorNombreUsuario.Value = Session["nameUsuarioConectado"].ToString();
        }
    }
}