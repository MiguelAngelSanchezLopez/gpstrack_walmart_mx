﻿using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class PaginasPorPerfil : OperacionesPaginaGeneral
    {
        ControladorPaginasPorPerfil controladorPaginasPorPerfil = new ControladorPaginasPorPerfil();
        ControladorUsuarios controladorUsuarios = new ControladorUsuarios();
        ControladorPerfiles controladorPerfiles = new ControladorPerfiles();
        ControladorPaginas controladorPaginas = new ControladorPaginas();

        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);
            ObtenerInformacionTodosJSON();
        }

        public void btnBorrarPaginaPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorPerfil.EliminarRegistroPaginasPorPerfilPorId(Convert.ToInt32(hidden_idTemporalPaginaPorPerfil.Value));
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnActualizarPaginaPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorPerfil.ActualizarRegistroPaginasPorPerfilPorId(hidden_JSONPaginaPorPerfilTemporal.Value);
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void notificarPerfilYPaginaRepetido() {
            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" id=\"MostrarMensajeProcesoExitosoDesdeServer\">\n");

            scriptJSGenerado.Append("swal({");
            scriptJSGenerado.Append("title: 'Alerta',");
            scriptJSGenerado.Append("text: 'Esta pagina ya esta asignada a este perfil',");
            scriptJSGenerado.Append("allowOutsideClick: false,type: 'warning'");

            //scriptJSGenerado.Append("}).then((result) => {");
            //scriptJSGenerado.Append("   TodosProcesosPorPantalla();");

            scriptJSGenerado.Append("})");

            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "JSScriptBlock", scriptJSGenerado.ToString());
        }

        public void btnInsertarPaginaPorPerfil_Click(Object sender, EventArgs e)
        {
            Boolean respuesta=controladorPaginasPorPerfil.InsertarRegistroPaginasPorPerfilPorId(hidden_JSONPaginaPorPerfilTemporal.Value);

            if (respuesta)
                RecargarPaginaActual();//Session["recargado"] = "TRUE";
            else
                notificarPerfilYPaginaRepetido();//Console.WriteLine("");
        }

        public override void ObtenerInformacionTodosJSON()
        {
            contenedorJSONPaginasPorPerfil.Value = controladorPaginasPorPerfil.ObtenerTodosRegistrosPaginasPorPerfil();
            contenedorJSONUsuarios.Value = controladorUsuarios.ObtenerTodosRegistrosUsuarios();
            contenedorJSONPerfiles.Value = controladorPerfiles.ObtenerTodosRegistrosPerfil();
            contenedorJSONPaginas.Value = controladorPaginas.ObtenerTodosRegistrosPaginas();
        }
    }
}