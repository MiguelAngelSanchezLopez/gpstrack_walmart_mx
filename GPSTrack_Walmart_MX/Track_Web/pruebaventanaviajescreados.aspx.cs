﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Track_Web.ControladoresBD;

namespace Track_Web
{
    public partial class pruebaventanaviajescreados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ControladorViajesCreadosPorUsuario controladorViajesCreadosPorUsuario = new ControladorViajesCreadosPorUsuario();
            
            string resultado = controladorViajesCreadosPorUsuario.ObtenerTodosViajesCreadosPorUsuario("bkh_mm0estr");

            contenedorJSONViajesCreadosPorUsuario.Value=resultado;

            //Console.WriteLine("algo");
        }
    }
}