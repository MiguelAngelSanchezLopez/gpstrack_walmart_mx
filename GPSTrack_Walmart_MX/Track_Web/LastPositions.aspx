﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LastPositions.aspx.cs" Inherits="Track_Web.LastPositions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/markerclusterer.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
  <script src="Scripts/markerwithlabel.js" type="text/javascript"></script>
  <script src="Scripts/OverlappingMarkerSpiderfier.min.js" type="text/javascript"></script>

 <script type="text/javascript">

    var geoLayer = new Array();
    var arrayPositions = new Array();
    var markerCluster;
    var infowindow = new google.maps.InfoWindow();
     var trafficLayer = new google.maps.TrafficLayer();
     var oms;

    Ext.onReady(function () {

        Ext.QuickTips.init();
        Ext.Ajax.timeout = 3600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                               /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

        //Verifica si se debe controlar tiempo de expiración de sesión
        Ext.Ajax.request({
            url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
            success: function (data, success) {
                if (data != null) {
                    data = Ext.decode(data.responseText);

                    if (data > 0) {
                        Ext.ns('App');

                        //Session timeout in secons     
                        App.SESSION_TIMEOUT = data;

                        // Helper that converts minutes to milliseconds.
                        App.toMilliseconds = function (minutes) {
                            return minutes * 60 * 1000;
                        }

                        // Notifies user that her session has timed out.
                        App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                            Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                            Ext.MessageBox.show({
                                title: "Sesión expirada.",
                                msg: "Su sesión ha expirado.",
                                icon: Ext.MessageBox.WARNING,
                                buttons: Ext.MessageBox.OK,
                                fn: function () {
                                    window.location = "Login.aspx";
                                }
                            });

                        });

                        // Starts the session timeout workflow after an AJAX request completes.
                        Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                            // Reset the client-side session timeout timers.
                            App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                        });

                    }
                }
            }
        })

      var dateDesde = new Ext.form.DateField({
        id: 'dateDesde',
        fieldLabel: 'Desde',
        labelWidth: 100,
        allowBlank: false,
        anchor: '99%',
        format: 'd-m-Y',
        editable: false,
        hidden: true,
        value: new Date(),
        maxValue: new Date(),
        style: {
          marginTop: '5px',
          marginLeft: '5px'
        },
      });

      dateDesde.on('change', function () {
        var _desde = Ext.getCmp('dateDesde');
        var _hasta = Ext.getCmp('dateHasta');

        _hasta.setValue(_desde.getValue());
        _hasta.setMinValue(_desde.getValue());
        _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 14));
        _hasta.validate();
      });

      var hourDesde = {
        xtype: 'timefield',
        id: 'hourDesde',
        allowBlank: false,
        hidden: true,
        format: 'H:i',
        minValue: '00:00',
        maxValue: '23:59',
        increment: 10,
        anchor: '99%',
        editable: true,
        value: '00:00',
        style: {
          marginTop: '5px'
        }
      };

      var dateHasta = new Ext.form.DateField({
        id: 'dateHasta',
        fieldLabel: 'Hasta',
        labelWidth: 100,
        allowBlank: false,
        hidden: true,
        anchor: '99%',
        format: 'd-m-Y',
        editable: false,
        value: new Date(),
        minValue: Ext.getCmp('dateDesde').getValue(),
        maxValue: new Date(),
        style: {
          marginLeft: '5px'
        },
      });

      var hourHasta = {
        xtype: 'timefield',
        id: 'hourHasta',
        allowBlank: false,
        hidden: true,
        format: 'H:i',
        minValue: '00:00',
        maxValue: '23:59',
        increment: 10,
        anchor: '99%',
        editable: true,
        value: new Date(),
        style: {
          marginTop: '5px'
        },
      };

      var storeFiltroTransportista = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['Transportista'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroTransportista = new Ext.form.field.ComboBox({
        id: 'comboFiltroTransportista',
        fieldLabel: 'Línea Transporte',
        labelWidth: 100,
        forceSelection: true,
        store: storeFiltroTransportista,
        valueField: 'Transportista',
        displayField: 'Transportista',
        queryMode: 'local',
        anchor: '99%',
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: false,
        forceSelection: true,
        style: {
          marginLeft: '5px'
        },
        listeners: {
          change: function (field, newVal) {
            if (newVal != null) {
              FiltrarPatentes();
              FiltrarCedis();
            }
            Ext.getCmp('comboFiltroPatente').reset();
          }
        }
      });

      var storeFiltroPatente = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['Patente'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentesPool&Todas=False',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroPatente = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatente',
        fieldLabel: 'Placa',
        labelWidth: 100,
        store: storeFiltroPatente,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        style: {
          marginLeft: '5px'
        }
      });

      var storeFiltroCedis = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['IdCentroDistribucion', 'Cedis'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllCedisPool&Todas=False',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });


      var comboFiltroCedis = new Ext.form.field.ComboBox({
        id: 'comboFiltroCedis',
        fieldLabel: 'Cedis',
        labelWidth: 100,
        store: storeFiltroCedis,
        valueField: 'IdCentroDistribucion',
        displayField: 'Cedis',
        queryMode: 'local',
        anchor: '99%',
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        style: {
          marginLeft: '5px'
        }
      });

      var toolbarPosiciones = Ext.create('Ext.toolbar.Toolbar', {
        id: 'toolbarPosiciones',
        height: 90,
        layout: 'column',
        items: [{
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.75,
          items: [dateDesde, dateHasta]
        }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.24,
          items: [hourDesde, hourHasta]
        }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 1,
          items: [comboFiltroTransportista]
        }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 1,
          items: [comboFiltroPatente]
        }, {
           xtype: 'container',
           layout: 'anchor',
           columnWidth: 1,
           items: [comboFiltroCedis]
         }]
      });

      var chkMostrarTrafico = new Ext.form.Checkbox({
        id: 'chkMostrarTrafico',
        fieldLabel: 'Mostrar tráfico',
        labelWidth: 90,
        listeners: {
          change: function (cb, checked) {
            if (checked == true) {
              trafficLayer.setMap(map);
            }
            else {
              trafficLayer.setMap(null);
            }
          }
        }
      });

      var btnExportar = {
          id: 'btnExportar',
          xtype: 'button',
          iconAlign: 'left',
          icon: 'Images/export_black_20x20.png',
          text: 'Exportar',
          width: 60,
          height: 26,
          disabled: false,
          style: {
              marginLeft: '20px'
          },
          listeners: {
              click: {
                  element: 'el',
                  fn: function () {

                      var mapForm = document.createElement("form");
                      mapForm.target = "ToExcel";
                      mapForm.method = "POST"; // or "post" if appropriate
                      mapForm.action = 'LastPositions.aspx?Metodo=ExportExcel';

                      document.body.appendChild(mapForm);
                      mapForm.submit();

                  }
              }
          }
      };

      var btnBuscar = {
        id: 'btnBuscar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/searchreport_black_20x20.png',
        text: 'Buscar',
        width: 90,
        height: 26,
        style: {
          marginLeft: '10px'
        },
        handler: function () {
          Buscar();
        }
      };

      var btnCancelar = {
        id: 'btnCancelar',
        xtype: 'button',
        hidden: true,
        width: 90,
        height: 26,
        iconAlign: 'left',
        icon: 'Images/back_black_20x20.png',
        text: 'Cancelar',

        handler: function () {
          Cancelar();
        }
      };

      var storePosiciones = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['Patente',
                  'NombreTipoMovil',
                  'Transportista',
                  { name: 'Fecha', type: 'date', dateFormat: 'c' },
                  'Latitud',
                  'Longitud',
                  'Velocidad',
                  'Direccion',
                  'Reporte',
                  'Cedis',
                  'EnTiempo',
                  'Retraso',
                  'SinReporte'
        ],
        proxy: new Ext.data.HttpProxy({
          //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPosicionesGPS_Ruta',
          url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetLastPositions',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var gridPosiciones = Ext.create('Ext.grid.Panel', {
        id: 'gridPosiciones',
        store: storePosiciones,
        tbar: toolbarPosiciones,
        columnLines: true,
        anchor: '100% 100%',
        scroll: false,
        buttons: [chkMostrarTrafico, btnExportar, btnBuscar, btnCancelar],
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columns: [
                      { text: 'Fecha', width: 105, dataIndex: 'Fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                      { text: 'Placa', width: 60, dataIndex: 'Patente' },
                      { text: 'NombreTipoMovil', dataIndex: 'NombreTipoMovil', hidden: true },
                      { text: 'Línea Transporte', flex: 1, dataIndex: 'Transportista' },
                      { text: 'Latitud', dataIndex: 'Latitud', hidden: true },
                      { text: 'Longitud', dataIndex: 'Longitud', hidden: true },
                      { text: 'Vel.', width: 35, dataIndex: 'Velocidad' },
                      { text: 'Direccion', dataIndex: 'Direccion', hidden: true },
                      { text: 'Cedis', dataIndex: 'Cedis' },
                      { text: 'En Tiempo', width: 60, dataIndex: 'EnTiempo' },
                      { text: 'Retraso', width: 50, dataIndex: 'Retraso' },
                      { text: 'Sin Reporte +12', width: 90, dataIndex: 'SinReporte' }
                      
        ],
        listeners: {
          select: function (sm, row, rec) {

            var date = Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.Fecha.toString();

            for (var i = 0; i < markers.length; i++) {
              if (markers[i].labelText == date) {
                markers[i].setAnimation(google.maps.Animation.BOUNCE);
                setTimeout('markers[' + i + '].setAnimation(null);', 800);
              }
            }

            Ext.getCmp('textFecha').setValue(date.replace("T", " "));
            Ext.getCmp('textVelocidad').setValue(row.data.Velocidad);
            Ext.getCmp('textLatitud').setValue(row.data.Latitud);
            Ext.getCmp('textLongitud').setValue(row.data.Longitud);

            map.setCenter(new google.maps.LatLng(row.data.Latitud, row.data.Longitud));
            //map.setZoom(16);

            Ext.getCmp("gridPosiciones").getSelectionModel().deselectAll();

          }
        }
      });

      var textFecha = new Ext.form.TextField({
        id: 'textFecha',
        fieldLabel: 'Fecha',
        labelWidth: 60,
        anchor: '99%',
        readOnly: true
      });

      var textVelocidad = new Ext.form.TextField({
        id: 'textVelocidad',
        fieldLabel: 'Velocidad',
        labelWidth: 60,
        anchor: '99%',
        readOnly: true
      });

      var textLatitud = new Ext.form.TextField({
        id: 'textLatitud',
        fieldLabel: 'Latitud',
        labelWidth: 60,
        anchor: '99%',
        readOnly: true
      });

      var textLongitud = new Ext.form.TextField({
        id: 'textLongitud',
        fieldLabel: 'Longitud',
        labelWidth: 60,
        anchor: '99%',
        readOnly: true
      });

      var viewWidth = Ext.getBody().getViewSize().width;
      var viewHeight = Ext.getBody().getViewSize().height;

      

      var leftPanel = new Ext.FormPanel({
        id: 'leftPanel',
        region: 'west',
        margins: '0 0 3 3',
        border: true,
        width: 530,
        minWidth: 300,
        maxWidth: viewWidth / 2,
        layout: 'anchor',
        split: true,
        collapsible: true,
        items: [gridPosiciones]
      });

      leftPanel.on('collapse', function () {
        google.maps.event.trigger(map, "resize");
      });

      leftPanel.on('expand', function () {
        google.maps.event.trigger(map, "resize");
      });

      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        contentEl: 'dvMap'
      });

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, leftPanel, centerPanel]
      });

      viewport.on('resize', function () {
        google.maps.event.trigger(map, "resize");
      });

    });

  </script>

  <script type="text/javascript">

    Ext.onReady(function () {
      GeneraMapa("dvMap", true);
    });

    function FiltrarPatentes() {
      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

      var store = Ext.getCmp('comboFiltroPatente').store;
      store.load({
        params: {
          transportista: transportista
        }
      });
    }

    function FiltrarCedis() {
      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

      var store = Ext.getCmp('comboFiltroCedis').store;
      store.load({
        params: {
          transportista: transportista
        }
      });
    }

    function Buscar() {

      if (!Ext.getCmp('leftPanel').getForm().isValid()) {
        return;
      }

      Ext.Msg.wait('Espere por favor...', 'Generando posiciones');

      Ext.getCmp('gridPosiciones').store.removeAll();
      ClearMap();
      Ext.getCmp('textFecha').reset();
      Ext.getCmp('textVelocidad').reset();
      Ext.getCmp('textLatitud').reset();
      Ext.getCmp('textLongitud').reset();

      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();
      var patente = Ext.getCmp('comboFiltroPatente').getValue();
      var idCedis = Ext.getCmp('comboFiltroCedis').getRawValue();

      if (idCedis === null || idCedis === "") {
        idCedis = -1;
      }

      var storePos = Ext.getCmp('gridPosiciones').store;

      storePos.load({
        params: {
          patente: patente,
          transportista: transportista,
          tipomovil: -1,
          idCedis: idCedis
        },
        callback: function (r, options, success) {
          if (success) {
            MuestraRutaViaje();
            Ext.Msg.hide();
          }
        }
      });

    }

    function Cancelar() {

      arrayPositions.splice(0, arrayPositions.length);

      Ext.getCmp('dateDesde').reset();
      Ext.getCmp('hourDesde').reset();
      Ext.getCmp('dateHasta').reset();
      Ext.getCmp('hourHasta').reset();
      Ext.getCmp('comboFiltroTransportista').reset();
      Ext.getCmp('comboFiltroPatente').reset();
      Ext.getCmp('comboFiltroPatente').store.load();

      Ext.getCmp('gridPosiciones').store.removeAll();
      ClearMap();

      for (var i = 0; i < geoLayer.length; i++) {
        geoLayer[i].layer.setMap(null);
        geoLayer[i].label.setMap(null);
      }
      geoLayer.splice(0, geoLayer.length);

      Ext.getCmp('textFecha').reset();
      Ext.getCmp('textVelocidad').reset();
      Ext.getCmp('textLatitud').reset();
      Ext.getCmp('textLongitud').reset();
    }

    function MuestraRutaViaje() {

        oms = new OverlappingMarkerSpiderfier(map, { markersWontMove: true, markersWontHide: true, keepSpiderfied: true });
        oms.addListener('click', function (marker, event) {

            //console.log("marker: " + JSON.stringify(marker));

            var contentString =
                    '<br>' +
                    '<table>' +
                    '<tr><td></td><td><b>'+ marker.labelText +'</b></td><td></td></tr>'+
                    obtenerIframeVistaCalles(latitud, longitud) +
                    '</table>' +
                    '<br>';

            infowindow.setContent(contentString);

            //infowindow.setContent(marker.labelText + " -hola-");
            infowindow.open(map, marker);

        });

      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null)
      }
      markers = [];

      if (markerCluster != null) {
        markerCluster.clearMarkers();
      }

      var store = Ext.getCmp('gridPosiciones').getStore();
      var rowCount = store.count();
      var iterRow = 0;

      while (iterRow < rowCount) {

        var dir = parseInt(store.data.items[iterRow].raw.Direccion);
        var patente = store.data.items[iterRow].raw.Patente;
        var lat = store.data.items[iterRow].raw.Latitud;
        var lon = store.data.items[iterRow].raw.Longitud;
        var estadoViaje = store.data.items[iterRow].raw.Reporte;
        var Latlng = new google.maps.LatLng(lat, lon);
        var fec = store.data.items[iterRow].raw.Fecha.toString().replace("T", " ");
        var Vel = store.data.items[iterRow].raw.Velocidad;
        var transport = store.data.items[iterRow].raw.Transportista;

        arrayPositions.push({
          Fecha: store.data.items[iterRow].raw.Fecha.toString(),
          Velocidad: store.data.items[iterRow].raw.Velocidad,
          Latitud: lat,
          Longitud: lon,
          LatLng: Latlng
        });

        var state = '';
        var iconRoute;
        if (estadoViaje == '1') {
          iconRoute = 'Images/Truck_Online/';
          state = 'Online';
        }
        else if (estadoViaje == '2') {
          iconRoute = 'Images/Truck_Unreported/';
          state = 'Offline';
        }
        else {
          iconRoute = 'Images/Truck_Empty/'
          state = 'Sin Reporte';
        }

        var contentString =

                   '<br>' +
                       '<table>' +
                       '<tr>' +
                             '       <td><b>Placa:</b></td>' +
                             '       <td><pre>     </pre></td>' +
                             '       <td>' + patente + '</td>' +
                         '</tr>' +
                         '<tr>' +
                             '       <td><b>Fecha:</b></td>' +
                             '       <td><pre>     </pre></td>' +
                             '       <td>' + fec + '</td>' +
                         '</tr>' +
                         '<tr>' +
                             '        <td><b>Velocidad:</b></td>' +
                             '       <td><pre>     </pre></td>' +
                             '        <td>' + Vel + '</td>' +
                         '</tr>' +
                         '<tr>' +
                             '        <td><b>Estado:</b></td>' +
                             '       <td><pre>     </pre></td>' +
                             '        <td>' + state + '</td>' +
                         '</tr>' +
                          '<tr>' +
                             '        <td><b>Linea Transporte:</b></td>' +
                             '       <td><pre>     </pre></td>' +
                             '        <td>' + transport + '</td>' +
            '</tr>' +

            obtenerIframeVistaCalles(lat,lon)
                                    +

                       '</table>' +
                     '<br>';

        switch (true) {
          case ((dir >= 338) || (dir < 22)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '1_N_21x29.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
          case ((dir >= 22) && (dir < 67)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '2_NE_32x30.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
          case ((dir >= 67) && (dir < 112)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '3_E_30x22.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
          case ((dir >= 112) && (dir < 157)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '4_SE_30x32.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
          case ((dir >= 157) && (dir < 202)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '5_S_21x29.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
          case ((dir >= 202) && (dir < 247)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '6_SW_30x32.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
          case ((dir >= 247) && (dir < 292)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '7_W_30x22.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
          case ((dir >= 292) && (dir < 338)):
            var marker = new google.maps.Marker({
              patente: patente,
              position: Latlng,
              icon: iconRoute + '8_NW_32x30.png',
              map: map,
              infoWinMark: contentString,
              labelText: patente
            });
            break;
        }

        var label = new Label({
          map: null,
        });

        label.bindTo('position', marker, 'position');
        label.bindTo('text', marker, 'labelText');

        google.maps.event.addListener(marker, 'click', function () {
          var latLng = this.position;

          for (i = 0; i < markers.length; i++) {
            if (markers[i].position.toString() == latLng.toString()) {
              position = i;
              break;
            }
          }
            var htlmInfoWindows = markers[position].infoWinMark;

            console.log("markers[position].infoWinMark " + markers[position].infoWinMark);

          //infowindow.setContent(htlmInfoWindows + "<b>algo<b>" );
            //infowindow.setContent( "<b>algo<b>" );
            infowindow.setContent(htlmInfoWindows);
          infowindow.open(map, this);
        });

        markers.push(marker);
        labels.push(label);

        oms.addMarker(marker);

        iterRow++;
      }
      markerCluster = new MarkerClusterer(map, markers);
    }

    function DrawZone(idZona) {

      for (var i = 0; i < geoLayer.length; i++) {
        geoLayer[i].layer.setMap(null);
        geoLayer[i].label.setMap(null);
        geoLayer.splice(i, 1);
      }

      //var colorZone = "#7f7fff";

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
        params: {
          IdZona: idZona
        },
        success: function (data, success) {
          if (data != null) {
            data = Ext.decode(data.responseText);
            if (data.Vertices.length > 1) { //Polygon
              var polygonGrid = new Object();
              polygonGrid.IdZona = data.IdZona;

              var arr = new Array();
              for (var i = 0; i < data.Vertices.length; i++) {
                arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
              }

              if (data.idTipoZona == 3) {
                var colorZone = "#FF0000";
              }
              else {
                var colorZone = "#7f7fff";
              }

              polygonGrid.layer = new google.maps.Polygon({
                paths: arr,
                strokeColor: "#000000",
                strokeWeight: 1,
                strokeOpacity: 0.9,
                fillColor: colorZone,
                fillOpacity: 0.3,
                labelText: data.NombreZona
              });
              polygonGrid.label = new Label({
                position: new google.maps.LatLng(data.Latitud, data.Longitud),
                map: map
              });
              polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
              polygonGrid.layer.setMap(map);
              geoLayer.push(polygonGrid);
            }
            else
              if (data.Vertices.length = 1) { //Point
                var Point = new Object();
                Point.IdZona = data.IdZona;

                var image = new google.maps.MarkerImage("Images/greymarker_32x32.png");

                Point.layer = new google.maps.Marker({
                  position: new google.maps.LatLng(data.Latitud, data.Longitud),
                  icon: image,
                  labelText: data.NombreZona,
                  map: map
                });

                Point.label = new Label({
                  position: new google.maps.LatLng(data.Latitud, data.Longitud),
                  map: map
                });

                Point.label.bindTo('text', Point.layer, 'labelText');
                Point.layer.setMap(map);
                geoLayer.push(Point);
              }

          }
        },
        failure: function (msg) {
          alert('Se ha producido un error.');
        }
      });
    }

  </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
