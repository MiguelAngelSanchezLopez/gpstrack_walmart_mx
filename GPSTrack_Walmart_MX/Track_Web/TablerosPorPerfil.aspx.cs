﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using perfilamientoUsuarios.ControladoresBD;
using Track_Web.ControladoresBD;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class TablerosPorPerfil : OperacionesTableroGeneral
    {
        ControladorTablerosPorPerfil controladorTablerosPorPerfil = new ControladorTablerosPorPerfil();
        ControladorUsuarios controladorUsuarios = new ControladorUsuarios();
        ControladorPerfiles controladorPerfiles = new ControladorPerfiles();
        ControladorTableros controladorTableros = new ControladorTableros();

        private List<string> listadoImagenesPorTablero;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Utilities.VerifyLoginStatus(Session, Response);
                ObtenerInformacionTodosJSON();
            }
            
            
            listadoImagenesPorTablero=PruebaObtenerImagenesPorTablero();
            ArmarJSDespliegaGaleriaImagenesTablero();
        }
        /*
        public void btnCargarImagenesPorTablero_Click(Object sender, EventArgs e) {
            listadoImagenesPorTablero = controladorTablerosPorPerfil.ObtenerImagenesPorIdTablero(Convert.ToInt32(hidden_idTablero.Value));
            ArmarJSDespliegaGaleriaImagenesTablero();
        }
        */
        public void btnBorrarTableroPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorTablerosPorPerfil.EliminarRegistroTablerosPorPerfilPorId(Convert.ToInt32(hidden_idTemporalTableroPorPerfil.Value));
            Session["recargado"] = "TRUE";
            RecargarTableroActual();
        }

        public void btnActualizarTableroPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorTablerosPorPerfil.ActualizarRegistroTablerosPorPerfilPorId(hidden_JSONTableroPorPerfilTemporal.Value);
            Session["recargado"] = "TRUE";
            RecargarTableroActual();
        }

        public void btnInsertarTableroPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorTablerosPorPerfil.InsertarRegistroTablerosPorPerfilPorId(hidden_JSONTableroPorPerfilTemporal.Value);
            Session["recargado"] = "TRUE";
            RecargarTableroActual();
        }
        
        public List<string> PruebaObtenerImagenesPorTablero() {
            return controladorTablerosPorPerfil.ObtenerImagenesPorIdTablero(1);
        }

        public void ArmarJSDespliegaGaleriaImagenesTablero() {
            StringBuilder cadenaListadoImagenes = new StringBuilder();

            foreach (string imagen in listadoImagenesPorTablero)
                cadenaListadoImagenes.Append("'" + imagen + "',");

            string listadoImagenesCadena = cadenaListadoImagenes.ToString();
            listadoImagenesCadena = listadoImagenesCadena.ToString().Substring(0, listadoImagenesCadena.Length - 1);

            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" >\n");

            scriptJSGenerado.Append("var arregloImagenesRecibido = [");
            scriptJSGenerado.Append(listadoImagenesCadena);
            scriptJSGenerado.Append("];");
            scriptJSGenerado.Append("$(document).ready(function () {");
            scriptJSGenerado.Append("AlimentarCargarImagenesPorTablero(arregloImagenesRecibido);");
            scriptJSGenerado.Append("});");
            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptDespliegaGaleriaImagenesTablero", scriptJSGenerado.ToString());
        }
        

        public override void ObtenerInformacionTodosJSON()
        {
            contenedorJSONTablerosPorPerfil.Value = controladorTablerosPorPerfil.ObtenerTodosRegistrosTablerosPorPerfil();
            contenedorJSONUsuarios.Value = controladorUsuarios.ObtenerTodosRegistrosUsuarios();
            contenedorJSONPerfiles.Value = controladorPerfiles.ObtenerTodosRegistrosPerfil();
            contenedorJSONTableros.Value = controladorTableros.ObtenerTodosRegistrosTableros();
        }
    }
}