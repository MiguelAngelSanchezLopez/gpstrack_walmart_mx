﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProgramacionRotacion.aspx.cs" Inherits="Track_Web.ProgramacionRotacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script type="text/javascript">
    Ext.onReady(function () {
        Ext.QuickTips.init();
        Ext.Ajax.timeout = 3600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

        //Verifica si se debe controlar tiempo de expiración de sesión
        Ext.Ajax.request({
            url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
            success: function (data, success) {
                if (data != null) {
                    data = Ext.decode(data.responseText);

                    if (data > 0) {
                        Ext.ns('App');

                        //Session timeout in secons     
                        App.SESSION_TIMEOUT = data;

                        // Helper that converts minutes to milliseconds.
                        App.toMilliseconds = function (minutes) {
                            return minutes * 60 * 1000;
                        }

                        // Notifies user that her session has timed out.
                        App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                            Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                            Ext.MessageBox.show({
                                title: "Sesión expirada.",
                                msg: "Su sesión ha expirado.",
                                icon: Ext.MessageBox.WARNING,
                                buttons: Ext.MessageBox.OK,
                                fn: function () {
                                    window.location = "Login.aspx";
                                }
                            });

                        });

                        // Starts the session timeout workflow after an AJAX request completes.
                        Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                            // Reset the client-side session timeout timers.
                            App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                        });

                    }
                }
            }
        })


      Ext.define('Rotacion', {
        extend: 'Ext.data.Model',
        fields: [
            'IdEmbarque',
            'IdMaster',
            'Linea_Transporte',
            'Patente_Trailer',
            'Patente_Tracto',
            'Origen',
            'Rut_Conductor',
            'Nombre_Conductor'
        ]
      });

      var data = (function () {
        data = []
        data.push({
          IdEmbarque: '',
          IdMaster: '',
          Linea_Transporte: '',
          Patente_Trailer: '',
          Patente_Tracto: '',
          Origen: '',
          Rut_Conductor: '',
          Nombre_Conductor: ''
        });

        return data;
      })();

      // create the Data Store
      var store = Ext.create('Ext.data.Store', {
        // destroy the store if the grid is destroyed
        autoDestroy: true,
        model: 'Rotacion',
        proxy: {
          type: 'memory'
        },
      //  data: data,
        sorters: [{
          property: 'start',
          direction: 'ASC'
        }]
      });


      var btnGuardar = {
        id: 'btnGuardar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/save_black_20x20.png',
        text: 'Guardar',
        width: 100,
        height: 27,
        handler: function () {
          rowEditing.cancelEdit();
          // Create a model instance
          var r = Ext.create('Rotacion', {
            IdEmbarque: '',
            IdMaster: '',
            Linea_Transporte: '',
            Patente_Trailer: '',
            Patente_Tracto: '',
            Origen: '',
            Rut_Conductor: '',
            Nombre_Conductor: ''
          });

          store.insert(0, r);
          rowEditing.startEdit(0, 0);
        }
      };

      var toolbarPosiciones = Ext.create('Ext.toolbar.Toolbar', {
        id: 'toolbarPosiciones',
        height: 91,
        layout: 'column',
        items: [btnGuardar]
      });

      var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToMoveEditor: 1,
        autoCancel: false
      });


      var storeFiltroPatente = new Ext.data.JsonStore({
        fields: ['Patente'],
        proxy: new Ext.data.HttpProxy({
          //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPatentesRuta&Todos=True',
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });   

      var gridPanelReporte = Ext.create('Ext.grid.Panel', {
        id: 'gridPanelReporte',
        title: 'Reporte Kms Recorridos',
        store: store,
        anchor: '100% 100%',
        columnLines: true,
        scroll: false,
        tbar: toolbarPosiciones,              
        columns: [

                    { text: 'Id Embarque', sortable: true, width: 105, dataIndex: 'IdEmbarque',editor: { allowBlank: true, maxLength: 50, regex: /^[-''&.,\w\sáéíóúüñÑ]+$/i} },
                    { text: 'ID Master', sortable: true, width: 105, dataIndex: 'IdMaster', editor: { allowBlank: true, maxLength: 50, regex: /^[-''&.,\w\sáéíóúüñÑ]+$/i} } ,
                    { text: 'Linea Transporte', sortable: true, width: 200, dataIndex: 'Linea_Transporte', editor: { allowBlank: true, maxLength: 50, regex: /^[-''&.,\w\sáéíóúüñÑ]+$/i} } ,
                    { text: 'Patente Trailer', sortable: true, width: 200, dataIndex: 'Patente_Trailer', editor: { xtype: 'combo', allowBlank: true, store: storeFiltroPatente, valueField: 'Patente', displayField: 'Patente', editable: false, forceSelection: true } },
                    { text: 'Patente Tracto', sortable: true, width: 200, dataIndex: 'Patente_Tracto', editor: { xtype: 'combo', allowBlank: true, store: storeFiltroPatente, valueField: 'Patente', displayField: 'Patente', editable: false, forceSelection: true } },
                    { text: 'Origen', sortable: true, width: 200, dataIndex: 'Origen', editor: { allowBlank: true, maxLength: 50, regex: /^[-''&.,\w\sáéíóúüñÑ]+$/i} } ,
                    { text: 'Rut Conductor', sortable: true, width: 200, dataIndex: 'Rut_Conductor' , editor: { allowBlank: true, maxLength: 50, regex: /^[-''&.,\w\sáéíóúüñÑ]+$/i} } ,
                    { text: 'Nombre Conductor', sortable: true, flex: 1, dataIndex: 'Nombre_Conductor', editor: { allowBlank: true, maxLength: 50, regex: /^[-''&.,\w\sáéíóúüñÑ]+$/i } }
        ],
        plugins: [rowEditing]       
      });




      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        items: [gridPanelReporte]
      });

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, centerPanel]
      });
    });
  </script>
</asp:Content>


