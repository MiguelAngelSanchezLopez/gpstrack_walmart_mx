﻿using Microsoft.CSharp;
using perfilamientoUsuarios.ControladoresBD;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace Track_Web.ControladoresBD
{
    public class ControladorViajesCreadosPorUsuario : ControladorGeneral
    {
        
        public List<string> listaColumnasConsulta = new List<string>();

        public List<Object> listaViajesCreadosPorUsuario;

        public void LlenarListaColumnasConsulta() {
            
            listaColumnasConsulta.Clear();

            listaColumnasConsulta.Add("ID");
            listaColumnasConsulta.Add("NUMTRANSPORTE");
            listaColumnasConsulta.Add("IDEMBARQUE");
            listaColumnasConsulta.Add("PATENTE_TRACTO");
            listaColumnasConsulta.Add("PATENTE_TRAILER");
            listaColumnasConsulta.Add("PLOCALES");
            listaColumnasConsulta.Add("SECUENCIA");
            listaColumnasConsulta.Add("FECHALLEGADA");	
            listaColumnasConsulta.Add("FECHASALIDA");
            listaColumnasConsulta.Add("MERCADERIA");
            listaColumnasConsulta.Add("MINIMO");
            listaColumnasConsulta.Add("MAXIMO");
            listaColumnasConsulta.Add("MINUTOS");
            listaColumnasConsulta.Add("RUTTRANSPORTISTA");
            listaColumnasConsulta.Add("NOMBRETRANSPORTISTA");	
            listaColumnasConsulta.Add("RUTCONDUCTOR");
            listaColumnasConsulta.Add("NOMBRECONDUCTOR");
            listaColumnasConsulta.Add("FECHACREACION");
            listaColumnasConsulta.Add("TIPOVIAJE");
            listaColumnasConsulta.Add("FILENAME");
            listaColumnasConsulta.Add("ACTUALIZADO");	
            listaColumnasConsulta.Add("COMENTARIOS");
            listaColumnasConsulta.Add("USUARIO");
            listaColumnasConsulta.Add("ESTADOTRACTO");
            listaColumnasConsulta.Add("ESTADOTRAILER");
            listaColumnasConsulta.Add("TIPO_TRANSPORTE");
             
        }

        public Object LlenarViajeCreadoPorUsuario()
        {
            
            Dictionary<string, object> viajeCreadoPorUsuarioTemporal = new Dictionary<string, object>();

            foreach (string propiedad in listaColumnasConsulta)
                viajeCreadoPorUsuarioTemporal[propiedad] = lectorDatosSQL[propiedad].ToString();
            
            return viajeCreadoPorUsuarioTemporal;
        }

        public void LlenarListadoViajesCreadosPorUsuario()
        {
            while (lectorDatosSQL.Read())
                listaViajesCreadosPorUsuario.Add(LlenarViajeCreadoPorUsuario());
        }

        public string ObtenerTodosViajesCreadosPorUsuario(string usuario)
        {
            try
            {
                LlenarListaColumnasConsulta();

                InicializarConexion();

                listaViajesCreadosPorUsuario = new List<Object>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetViajesCreadosPorUsuario");

                EstablecerParametroProcedimientoAlmacenado(usuario, "string", "@Usuario");

                LlenarLectorDatosSQL();

                LlenarListadoViajesCreadosPorUsuario();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listaViajesCreadosPorUsuario);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }
    }
}