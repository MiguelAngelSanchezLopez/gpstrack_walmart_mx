﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Track_Web.ModelosBD;

namespace Track_Web.ControladoresBD
{
    public class ControladorTablerosPorPerfil : ControladorGeneral
    {
        private List<Track_TablerosPorPerfil> listadoTablerosPorPerfil;

        private List<string> listadoImagenesPorTablero = new List<string>();

        public String LlenarImagenPorTablero() {
            return lectorDatosSQL["URL"].ToString();
        }

        public Track_TablerosPorPerfil LlenarTableroPorPerfil()
        {
            Track_TablerosPorPerfil TableroPorPerfilTemporal = new Track_TablerosPorPerfil();
            TableroPorPerfilTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            TableroPorPerfilTemporal.IdTablero = Convert.ToInt32(lectorDatosSQL["IdTablero"]);
            TableroPorPerfilTemporal.IdPerfil = Convert.ToInt32(lectorDatosSQL["IdPerfil"]);
            TableroPorPerfilTemporal.FechaCreacion = Convert.ToDateTime(lectorDatosSQL["FechaCreacion"]);
            TableroPorPerfilTemporal.IdUsuarioCreo = Convert.ToInt32(lectorDatosSQL["IdUsuarioCreo"]);
            TableroPorPerfilTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"]);
            return TableroPorPerfilTemporal;
        }

        public void LlenarListadoTablerosPorPerfil()
        {
            while (lectorDatosSQL.Read())
                listadoTablerosPorPerfil.Add(LlenarTableroPorPerfil());
        }

        public void LlenarListadoImagenerPorIdTablero()
        {
            while (lectorDatosSQL.Read())
                listadoImagenesPorTablero.Add(LlenarImagenPorTablero());
        }

        public List<string> ObtenerImagenesPorIdTablero(int idTablero)
        {
            try
            {
                InicializarConexion();
                
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetImagenerPorIdTablero");
                EstablecerParametroProcedimientoAlmacenado(idTablero, "int", "@IdTablero");

                LlenarLectorDatosSQL();

                LlenarListadoImagenerPorIdTablero();

                conexionSQL.Close();

                return listadoImagenesPorTablero;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerTodosRegistrosTablerosPorPerfil()
        {
            try
            {
                InicializarConexion();

                listadoTablerosPorPerfil = new List<Track_TablerosPorPerfil>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetTablerosPorPerfil");

                LlenarLectorDatosSQL();

                LlenarListadoTablerosPorPerfil();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoTablerosPorPerfil);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }
        }

        public void EliminarRegistroTablerosPorPerfilPorId(int id)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_EliminarTablerosPorPerfil");
                EstablecerParametroProcedimientoAlmacenado(id, "int", "@Id");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ActualizarRegistroTablerosPorPerfilPorId(string cadenaJSONTableroPorPerfil)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_ActualizarTablerosPorPerfil");
                Track_TablerosPorPerfil TableroPorPerfilTemporal = JsonConvert.DeserializeObject<Track_TablerosPorPerfil>(cadenaJSONTableroPorPerfil);
                EstablecerParametroProcedimientoAlmacenado(TableroPorPerfilTemporal.Id, "int", "@Id");
                EstablecerParametroProcedimientoAlmacenado(TableroPorPerfilTemporal.IdTablero, "int", "@IdTablero");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroTablerosPorPerfilPorId(string cadenaJSONTableroPorPerfil)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarTablerosPorPerfil");
                Track_TablerosPorPerfil TableroPorPerfilTemporal = JsonConvert.DeserializeObject<Track_TablerosPorPerfil>(cadenaJSONTableroPorPerfil);

                EstablecerParametroProcedimientoAlmacenado(TableroPorPerfilTemporal.IdTablero, "int", "@IdTablero");
                EstablecerParametroProcedimientoAlmacenado(TableroPorPerfilTemporal.IdPerfil, "int", "@IdPerfil");
                EstablecerParametroProcedimientoAlmacenado(TableroPorPerfilTemporal.IdUsuarioCreo, "int", "@IdUsuarioCreo");
                EstablecerParametroProcedimientoAlmacenado(TableroPorPerfilTemporal.FechaCreacion, "datetime", "@FechaCreacion");
                EstablecerParametroProcedimientoAlmacenado(TableroPorPerfilTemporal.Activo, "bool", "@Activo");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}