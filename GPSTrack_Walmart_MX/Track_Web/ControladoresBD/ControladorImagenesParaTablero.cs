﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Track_Web.ModelosBD;

namespace Track_Web.ControladoresBD
{
    public class ControladorImagenesParaTablero : ControladorGeneral
    {
        private List<Track_ImagenesParaTablero> listadoImagenesParaTableros;

        public Track_ImagenesParaTablero LlenarImagenParaTablero()
        {
            Track_ImagenesParaTablero ImagenParaTableroTemporal = new Track_ImagenesParaTablero();
            ImagenParaTableroTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            ImagenParaTableroTemporal.URL = lectorDatosSQL["URL"].ToString();
            ImagenParaTableroTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"].ToString());
            return ImagenParaTableroTemporal;
        }

        public void LlenarListadoImagenesParaTableros()
        {
            while (lectorDatosSQL.Read())
                listadoImagenesParaTableros.Add(LlenarImagenParaTablero());
        }

        public string ObtenerTodosRegistrosImagenesParaTableros()
        {
            try
            {
                InicializarConexion();

                listadoImagenesParaTableros = new List<Track_ImagenesParaTablero>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetImagenesParaTableros");

                LlenarLectorDatosSQL();

                LlenarListadoImagenesParaTableros();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoImagenesParaTableros);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }
        
        public void EliminarRegistroImagenParaTableroPorId(int id)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_EliminarImagenParaTablero");
                EstablecerParametroProcedimientoAlmacenado(id, "int", "@Id");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ActualizarRegistroImagenParaTablero(string cadenaJSONImagenParaTablero)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_ActualizarImagenParaTablero");
                Track_ImagenesParaTablero ImagenParaTableroTemporal = JsonConvert.DeserializeObject<Track_ImagenesParaTablero>(cadenaJSONImagenParaTablero);

                EstablecerParametroProcedimientoAlmacenado(ImagenParaTableroTemporal.Id, "int", "@Id");
                EstablecerParametroProcedimientoAlmacenado(ImagenParaTableroTemporal.URL, "string", "@URL");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroImagenParaTablero(string ImagenParaTableroTemporalURL )//string cadenaJSONImagenParaTablero
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarImagenParaTablero");
                //Track_ImagenesParaTablero ImagenParaTableroTemporal = JsonConvert.DeserializeObject<Track_ImagenesParaTablero>(cadenaJSONImagenParaTablero);
                
                EstablecerParametroProcedimientoAlmacenado(ImagenParaTableroTemporalURL, "string", "@URL");

                EstablecerParametroProcedimientoAlmacenado(true, "bool", "@Activo");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int InsertarRegistroImagenParaTableroObtieneIdImagen(string ImagenParaTableroTemporalURL)//string cadenaJSONImagenParaTablero
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarImagenParaTableroObtenerUltimoIdInsertado");
                EstablecerParametroProcedimientoAlmacenado(ImagenParaTableroTemporalURL, "string", "@URL");
                EstablecerParametroProcedimientoAlmacenado(true, "bool", "@Activo");

                EjecutarProcedimientoAlmacenado();

                int idNuevaImagen = Convert.ToInt32(EjecutarProcedimientoAlmacenadoEscalar());//1;

                conexionSQL.Close();

                
                return idNuevaImagen;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}