﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorPaginasPorPerfil: ControladorGeneral
    {
        private List<Track_PaginasPorPerfil> listadoPaginasPorPerfil;
        
        public Track_PaginasPorPerfil LlenarPaginaPorPerfil() {
            Track_PaginasPorPerfil paginaPorPerfilTemporal = new Track_PaginasPorPerfil();
            paginaPorPerfilTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            paginaPorPerfilTemporal.IdPagina = Convert.ToInt32(lectorDatosSQL["IdPagina"]);
            paginaPorPerfilTemporal.IdPerfil = Convert.ToInt32(lectorDatosSQL["IdPerfil"]);
            paginaPorPerfilTemporal.FechaCreacion = Convert.ToDateTime(lectorDatosSQL["FechaCreacion"]);
            paginaPorPerfilTemporal.IdUsuarioCreo = Convert.ToInt32(lectorDatosSQL["IdUsuarioCreo"]);
            paginaPorPerfilTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"]);
            return paginaPorPerfilTemporal;
        }

        public void LlenarListadoPaginasPorPerfil() {
            while (lectorDatosSQL.Read())
                listadoPaginasPorPerfil.Add(LlenarPaginaPorPerfil());
        }
        
        public string ObtenerTodosRegistrosPaginasPorPerfil()
        {
            try
            {
                InicializarConexion();

                listadoPaginasPorPerfil = new List<Track_PaginasPorPerfil>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPaginasPorPerfil");

                LlenarLectorDatosSQL();

                LlenarListadoPaginasPorPerfil();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoPaginasPorPerfil);
            }
            catch (Exception ex) {
                return ArmarJSONGenericoConError(ex.Message);
            }
        }

        public int ObtenerPaginaDeRegistroABorrar(int idRegistroABorrar)
        {
            try
            {
                ControladorGeneral cGeneral = new ControladorGeneral();
                cGeneral.InicializarConexion();
                cGeneral.conexionSQL.Open();
                cGeneral.EstablecerProcedimientoAlmacenado("OBTENER_IDPAGINA_DEL_REGISTRO_A_BORRAR_PAGINAS_POR_PERFIL");
                cGeneral.EstablecerParametroProcedimientoAlmacenado(idRegistroABorrar, "int", "@Id");
                Int32 idPaginaObtenido = Convert.ToInt32(cGeneral.EjecutarProcedimientoAlmacenadoEscalar());
                cGeneral.conexionSQL.Close();
                return idPaginaObtenido;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int ObtenerPerfilDeRegistroABorrar(int idRegistroABorrar)
        {
            try
            {
                ControladorGeneral cGeneral = new ControladorGeneral();
                cGeneral.InicializarConexion();
                cGeneral.conexionSQL.Open();
                cGeneral.EstablecerProcedimientoAlmacenado("OBTENER_IDPERFIL_DEL_REGISTRO_A_BORRAR_PAGINAS_POR_PERFIL");
                cGeneral.EstablecerParametroProcedimientoAlmacenado(idRegistroABorrar, "int", "@Id");
                Int32 idPerfilObtenido = Convert.ToInt32(cGeneral.EjecutarProcedimientoAlmacenadoEscalar());
                cGeneral.conexionSQL.Close();
                return idPerfilObtenido;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ExtenderEliminacionPaginasAUsuariosConPerfilCoincidente(int IdPagina, List<int> IdsUsuarios)
        {
            try
            {
                foreach (int IdUsuario in IdsUsuarios)
                {
                    ControladorGeneral cGeneral = new ControladorGeneral();
                    cGeneral.InicializarConexion();
                    cGeneral.conexionSQL.Open();
                    
                    cGeneral.EstablecerProcedimientoAlmacenado("Track_DesactivarPaginaSeleccionadaPorUsuario");

                    cGeneral.EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                    cGeneral.EstablecerParametroProcedimientoAlmacenado(IdPagina, "int", "@IdPagina");

                    cGeneral.EjecutarProcedimientoAlmacenado();
                    cGeneral.conexionSQL.Close();

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<int> ObtenerListaUsuariosConPaginaAsignada(Track_PaginasPorPerfil paginaPorPerfilTemporal)
        {
            try
            {
                List<int> IdsUsuarios = new List<int>();
                ControladorGeneral cGeneral = new ControladorGeneral();
                cGeneral.InicializarConexion();
                cGeneral.conexionSQL.Open();

                cGeneral.EstablecerProcedimientoAlmacenado("OBTENER_LISTA_USUARIOS_CON_PAGINA_ASIGNADA");

                cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPagina, "int", "@IdPagina");
                cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPerfil, "int", "@IdPerfil");

                cGeneral.EjecutarProcedimientoAlmacenado();

                cGeneral.LlenarLectorDatosSQL();

                while (cGeneral.lectorDatosSQL.Read())
                    IdsUsuarios.Add(Convert.ToInt32(cGeneral.lectorDatosSQL["IdUsuario"]));

                cGeneral.conexionSQL.Close();

                return IdsUsuarios;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public void EliminacionDePaginasParaUsuariosConMismoPerfil(int id) {
            //obtener pagina y perfil de registro a borrar
            Track_PaginasPorPerfil paginaPorPerfilTemporal = new Track_PaginasPorPerfil();
            paginaPorPerfilTemporal.IdPagina = ObtenerPaginaDeRegistroABorrar(id);
            paginaPorPerfilTemporal.IdPerfil = ObtenerPerfilDeRegistroABorrar(id);

            //obtener lista de usuarios
            List<int> IdsUsuarios = ObtenerListaUsuariosConPaginaAsignada(paginaPorPerfilTemporal);

            //borrar pagina de cada usuario de la lista
            ExtenderEliminacionPaginasAUsuariosConPerfilCoincidente(paginaPorPerfilTemporal.IdPagina, IdsUsuarios);
        }

        public void EliminarRegistroPaginasPorPerfilPorId(int id) {
            try
            {

                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_EliminarPaginasPorPerfil");
                EstablecerParametroProcedimientoAlmacenado(id,"int", "@Id");
                
                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();

                EliminacionDePaginasParaUsuariosConMismoPerfil(id);
            }
            catch (Exception ex) {
                throw;
            }
        }

        public void ActualizarRegistroPaginasPorPerfilPorId(string cadenaJSONPaginaPorPerfil)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_ActualizarPaginasPorPerfil");
                Track_PaginasPorPerfil paginaPorPerfilTemporal = JsonConvert.DeserializeObject<Track_PaginasPorPerfil>(cadenaJSONPaginaPorPerfil);
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.Id, "int", "@Id");
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPagina, "int", "@IdPagina");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int ObtenerNumeroDuplicadosPaginasPerfil(Track_PaginasPorPerfil paginaPorPerfilTemporal) {
            try {
                ControladorGeneral cGeneral = new ControladorGeneral();
                cGeneral.InicializarConexion();
                cGeneral.conexionSQL.Open();
                cGeneral.EstablecerProcedimientoAlmacenado("OBTENER_REGISTROS_DUPLICADOS");
                cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPagina, "int", "@IdPagina");
                cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPerfil, "int", "@IdPerfil");
                Int32 numeroDuplicados = Convert.ToInt32(cGeneral.EjecutarProcedimientoAlmacenadoEscalar());
                cGeneral.conexionSQL.Close();
                return numeroDuplicados;
                
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<int> ObtenerListaUsuariosSinPaginaAsignada(Track_PaginasPorPerfil paginaPorPerfilTemporal)
        {
            try
            {
                List<int> IdsUsuarios = new List<int>();
                ControladorGeneral cGeneral = new ControladorGeneral();
                cGeneral.InicializarConexion();
                cGeneral.conexionSQL.Open();

                cGeneral.EstablecerProcedimientoAlmacenado("OBTENER_LISTA_USUARIOS_SIN_PAGINA_ASIGNADA");

                cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPagina, "int", "@IdPagina");
                cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPerfil, "int", "@IdPerfil");
                
                cGeneral.EjecutarProcedimientoAlmacenado();

                cGeneral.LlenarLectorDatosSQL();

                while (cGeneral.lectorDatosSQL.Read())
                    IdsUsuarios.Add(Convert.ToInt32(cGeneral.lectorDatosSQL["IdUsuario"]));

                cGeneral.conexionSQL.Close();
                
                return IdsUsuarios;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ExtenderPaginasAUsuariosConPerfilCoincidente(int IdPagina, List<int> IdsUsuarios) {
            try
            {
                foreach (int IdUsuario in IdsUsuarios) {
                    ControladorGeneral cGeneral = new ControladorGeneral();
                    cGeneral.InicializarConexion();
                    cGeneral.conexionSQL.Open();

                    cGeneral.EstablecerProcedimientoAlmacenado("Track_InsertarPaginasPorUsuario");

                    cGeneral.EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                    cGeneral.EstablecerParametroProcedimientoAlmacenado(IdPagina, "int", "@IdPagina");

                    cGeneral.EjecutarProcedimientoAlmacenado();
                    cGeneral.conexionSQL.Close();
                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Boolean InsertarRegistroPaginasPorPerfilPorId(string cadenaJSONPaginaPorPerfil)
        {
            
            try
            {
                Track_PaginasPorPerfil paginaPorPerfilTemporal = JsonConvert.DeserializeObject<Track_PaginasPorPerfil>(cadenaJSONPaginaPorPerfil);

                int numeroDuplicados = ObtenerNumeroDuplicadosPaginasPerfil(paginaPorPerfilTemporal);

                if (numeroDuplicados == 0)
                {
                    ControladorGeneral cGeneral = new ControladorGeneral();

                    cGeneral.InicializarConexion();
                    cGeneral.conexionSQL.Open();

                    cGeneral.EstablecerProcedimientoAlmacenado("Track_InsertarPaginasPorPerfil");

                    cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPagina, "int", "@IdPagina");
                    cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPerfil, "int", "@IdPerfil");
                    cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdUsuarioCreo, "int", "@IdUsuarioCreo");
                    cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.FechaCreacion, "datetime", "@FechaCreacion");
                    cGeneral.EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.Activo, "bool", "@Activo");

                    cGeneral.EjecutarProcedimientoAlmacenado();

                    //extender paginas a usuarios con perfil coindicente
                    List<int> IdsUsuarios = ObtenerListaUsuariosSinPaginaAsignada(paginaPorPerfilTemporal);

                    ExtenderPaginasAUsuariosConPerfilCoincidente(paginaPorPerfilTemporal.IdPagina, IdsUsuarios);

                    cGeneral.conexionSQL.Close();
                    
                    return true;
                }
                else {
                    return false; 
                }

                
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}