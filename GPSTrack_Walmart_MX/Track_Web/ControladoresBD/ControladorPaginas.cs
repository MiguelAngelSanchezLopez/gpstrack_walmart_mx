﻿using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorPaginas:ControladorGeneral
    {
        private List<Track_Paginas> listadoPaginas;

        public Track_Paginas LlenarPagina()
        {
            Track_Paginas paginaTemporal = new Track_Paginas();
            paginaTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            paginaTemporal.Nombre = lectorDatosSQL["Nombre"].ToString();
            paginaTemporal.NombreArchivo = lectorDatosSQL["NombreArchivo"].ToString();
            paginaTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"].ToString());
            return paginaTemporal;
        }

        public void LlenarListadoPaginas()
        {
            while (lectorDatosSQL.Read())
                listadoPaginas.Add(LlenarPagina());
        }

        public string ObtenerTodosRegistrosPaginas()
        {
            try
            {
                InicializarConexion();

                listadoPaginas = new List<Track_Paginas>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPaginas");

                LlenarLectorDatosSQL();

                LlenarListadoPaginas();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoPaginas);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }
    }
}