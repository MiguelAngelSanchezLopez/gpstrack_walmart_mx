﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Track_Web.ModelosBD;

namespace Track_Web.ControladoresBD
{
    public class ControladorImagenesPorTablero : ControladorGeneral
    {
        private List<RelacionImagenTablero> listadorelacionImagenTableros;
        private List<Track_ImagenesPorTablero> listadoImagenesPorTableros;

        public Track_ImagenesPorTablero LlenarImagenPorTablero()
        {
            Track_ImagenesPorTablero ImagenPorTableroTemporal = new Track_ImagenesPorTablero();
            ImagenPorTableroTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            ImagenPorTableroTemporal.IdImagen = Convert.ToInt32(lectorDatosSQL["IdImagen"].ToString());
            ImagenPorTableroTemporal.IdTablero = Convert.ToInt32(lectorDatosSQL["IdTablero"].ToString());
            ImagenPorTableroTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"].ToString());
            return ImagenPorTableroTemporal;
        }

        public RelacionImagenTablero LlenarRelacionImagenTablero()
        {
            RelacionImagenTablero relacionImagenTemporal = new RelacionImagenTablero();
            relacionImagenTemporal.Id= Convert.ToInt32(lectorDatosSQL["Id"]);
            relacionImagenTemporal.URL = lectorDatosSQL["URL"].ToString();
            return relacionImagenTemporal;
        }

        public void LlenarListadoImagenesPorTableros()
        {
            while (lectorDatosSQL.Read())
                listadoImagenesPorTableros.Add(LlenarImagenPorTablero());
        }

        public void LlenarListadoImagenesYRelacionesPorTableros()
        {
            while (lectorDatosSQL.Read())
                listadorelacionImagenTableros.Add(LlenarRelacionImagenTablero());
        }

        public string ObtenerTodasImagenesYRelacionesPorTableros(int idTablero)
        {
            try
            {
                InicializarConexion();

                listadorelacionImagenTableros = new List<RelacionImagenTablero>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("SP_GET_IMAGENES_Y_RELACIONES_PARA_TABLERO");
                EstablecerParametroProcedimientoAlmacenado(idTablero, "int", "@IdTablero");

                LlenarLectorDatosSQL();

                LlenarListadoImagenesYRelacionesPorTableros();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadorelacionImagenTableros);
                
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }


        public string ObtenerTodosRegistrosImagenesPorTableros()
        {
            try
            {
                InicializarConexion();

                listadoImagenesPorTableros = new List<Track_ImagenesPorTablero>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetImagenesPorTableros");

                LlenarLectorDatosSQL();

                LlenarListadoImagenesPorTableros();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoImagenesPorTableros);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }

        public void EliminarRegistroImagenPorTableroPorId(int id)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_EliminarImagenPorTablero");
                EstablecerParametroProcedimientoAlmacenado(id, "int", "@Id");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ActualizarRegistroImagenPorTablero(string cadenaJSONImagenPorTablero)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_ActualizarImagenPorTablero");
                Track_ImagenesPorTablero ImagenPorTableroTemporal = JsonConvert.DeserializeObject<Track_ImagenesPorTablero>(cadenaJSONImagenPorTablero);

                EstablecerParametroProcedimientoAlmacenado(ImagenPorTableroTemporal.Id, "int", "@Id");
                EstablecerParametroProcedimientoAlmacenado(ImagenPorTableroTemporal.IdImagen, "int", "@IdImagen");
                EstablecerParametroProcedimientoAlmacenado(ImagenPorTableroTemporal.IdTablero, "int", "@IdTablero");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroImagenPorTablero(int IdImagen, int IdTablero)//string cadenaJSONImagenParaTablero
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarImagenPorTablero");
                //Track_ImagenesParaTablero ImagenParaTableroTemporal = JsonConvert.DeserializeObject<Track_ImagenesParaTablero>(cadenaJSONImagenParaTablero);

                //EstablecerParametroProcedimientoAlmacenado(ImagenPorTableroTemporalURL, "string", "@URL");
                EstablecerParametroProcedimientoAlmacenado(IdImagen, "int", "@IdImagen");
                EstablecerParametroProcedimientoAlmacenado(IdTablero, "int", "@IdTablero");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}