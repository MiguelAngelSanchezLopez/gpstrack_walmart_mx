﻿using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Track_Web
{
    public class LectorPaginasAutorizadasAlUsuario:ControladorGeneral
    {
        
        public List<string> LlenarListadoPaginas(List<string> listadoPaginasPermitidasAlUsuario)
        {
            while (lectorDatosSQL.Read())
                listadoPaginasPermitidasAlUsuario.Add(lectorDatosSQL["NombreArchivo"].ToString());

            return listadoPaginasPermitidasAlUsuario;
        }

        public List<string> ObtenerTodosRegistrosPaginas(int IdUsuarioLogueado)
        {
            List<string> listadoPaginasPermitidasAlUsuario = new List<string>();
            try
            {
                InicializarConexion();
                
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPaginasPermitidasAlUsuario");
                EstablecerParametroProcedimientoAlmacenado(IdUsuarioLogueado, "int", "@IdUsuario");

                LlenarLectorDatosSQL();

                listadoPaginasPermitidasAlUsuario=LlenarListadoPaginas(listadoPaginasPermitidasAlUsuario);

                conexionSQL.Close();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listadoPaginasPermitidasAlUsuario;
        }
    }
}