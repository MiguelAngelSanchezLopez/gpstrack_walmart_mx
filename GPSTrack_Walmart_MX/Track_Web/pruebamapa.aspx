﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pruebamapa.aspx.cs" Inherits="Track_Web.pruebamapa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>


    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #street-view {
            height: 100%;
        }
    </style>

    <div id="street-view"></div>
    <script>
        var panorama;

        function initialize() {
            var latitud = getParameterByName('latitud');
            var longitud = getParameterByName('longitud');
            
            panorama = new google.maps.StreetViewPanorama(
                document.getElementById('street-view'),
                {
                    position: {
                        lat: parseFloat(latitud),
                        lng: parseFloat(longitud)
                    },
                    pov: { heading: 165, pitch: 0 },
                    zoom: 1
                });
        }
        
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }

    </script>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8&callback=initialize">
    </script>


</body>
</html>
