﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Track_Web.ModelosBD
{
    public class RelacionImagenTablero
    {
        public int Id { get; set; }
        public string URL { get; set; }
    }
}