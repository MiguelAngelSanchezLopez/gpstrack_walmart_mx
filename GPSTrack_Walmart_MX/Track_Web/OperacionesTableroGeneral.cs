﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Track_Web
{
    public class OperacionesTableroGeneral : System.Web.UI.Page
    {
        public void RecargarPaginaActual()
        {
            ObtenerInformacionTodosJSON();

            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" id=\"MostrarMensajeProcesoExitosoDesdeServer\">\n");

            scriptJSGenerado.Append("swal({");
            scriptJSGenerado.Append("title: 'Proceso Exitoso',");
            scriptJSGenerado.Append("text: 'Los Datos han sido actualizados',");
            scriptJSGenerado.Append("allowOutsideClick: false,");

            scriptJSGenerado.Append("}).then((result) => {");
            scriptJSGenerado.Append("   TodosProcesosPorPantalla();");

            scriptJSGenerado.Append("})");

            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "JSScriptBlockRecargarPaginaActual", scriptJSGenerado.ToString());

        }

        public void RecargarTableroActual()
        {
            ObtenerInformacionTodosJSON();

            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" id=\"MostrarMensajeProcesoExitosoDesdeServer\">\n");

            scriptJSGenerado.Append("swal({");
            scriptJSGenerado.Append("title: 'Proceso Exitoso',");
            scriptJSGenerado.Append("text: 'Los Datos han sido actualizados',");
            scriptJSGenerado.Append("allowOutsideClick: false,");

            scriptJSGenerado.Append("}).then((result) => {");
            scriptJSGenerado.Append("   TodosProcesosPorPantalla();");

            scriptJSGenerado.Append("})");

            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "JSScriptBlock", scriptJSGenerado.ToString());

        }

        public virtual void ObtenerInformacionTodosJSON()
        {

        }
    }

}