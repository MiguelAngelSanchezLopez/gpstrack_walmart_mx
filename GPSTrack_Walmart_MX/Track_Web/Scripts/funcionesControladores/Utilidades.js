﻿function convertirFechaAFormatoMMDDYYYY(fechaOrigen) {
    if (fechaOrigen.indexOf("T") > -1)
        return fechaOrigen;

    var elementosFecha = fechaOrigen.split("/");
    var fechaConFormato = elementosFecha[1] + "/" + elementosFecha[0] + "/" + elementosFecha[2];
    return fechaConFormato;
}

function ObtenerBooleanoDeCadena(cadena) {
    return (cadena == "True");
}

function CrearFechaFormatoDDMMYYYY(fechaOrigen) {
    fechaOrigen = fechaOrigen.substr(0, 10);
    var elementosFecha = fechaOrigen.split("-");
    var fechaConFormatoDDMMYYYY = elementosFecha[2] + "/" + elementosFecha[1] + "/" + elementosFecha[0];
    return fechaConFormatoDDMMYYYY;
}

function AcompletarCifras(cifra) {
    if (cifra.length<2)
        return (cifra < 10) ? "0" + cifra : cifra;
    return cifra;
}

function CrearFechaFormatoDDMMYYYYBD(fechaOrigen) {
    fechaOrigen = fechaOrigen.substr(0, 10);

    var elementosFechaPrevios = fechaOrigen.split(" ");

    var elementosFecha = elementosFechaPrevios[0].split("/");
    var fechaConFormatoDDMMYYYY = AcompletarCifras(elementosFecha[2]) + "/" + AcompletarCifras(elementosFecha[1]) + "/" + elementosFecha[0];
    return fechaConFormatoDDMMYYYY;
}

function MostrarAlertaGeneral(titulo, descripcion, tipo) {
    swal(
        titulo, descripcion, tipo
    );
}

function MostrarAlertaFallida(texto) {
    MostrarAlertaGeneral('Proceso Fallido', texto, 'error');
}

function InicializarSelectoresFecha() {
    $('.selectorDatePicker').datepicker({ dateFormat: 'dd/mm/yy' });

    $('.selectorDatePicker').on('change', function () {
        $("#" + $(this).attr("campoTexto")).val($(this).val());
        InvocarCambioCampoTexto($(this).attr("campoTexto"));
    });
}

function InvocarCambioCampoTexto(nombreCampoTexto) {
    const event = $.Event('keyup');
    $("#" + nombreCampoTexto).focus();
    $("#" + nombreCampoTexto).trigger(event);
}

function LimpiarCampoTextoValorNulo(data, nombreCampoTexto) {
    if (data[0].text == "SELECCIONE UN VALOR")
        $("#" + nombreCampoTexto).val("");
}

function ObtenerArgumentoDisplayDeBoolean(valorBooleano) {
    return (valorBooleano) ? "block" : "none";
}

function EliminarUltimoCaracterCadena(cadena) {
    return cadena.substr(0, cadena.length - 1);
}