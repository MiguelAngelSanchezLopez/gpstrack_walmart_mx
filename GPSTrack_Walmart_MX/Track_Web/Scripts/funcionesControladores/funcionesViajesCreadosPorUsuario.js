﻿
var objetoJSONViajesCreadosPorUsuario = new Object();

function TratamientoJSONGeneral(cadenaJSON, objetoJSONDestino) {
    var objetoJSONTemporal = JSON.parse(cadenaJSON);

    (objetoJSONTemporal.Status) ? AsignarContenidoJSON(objetoJSONTemporal, objetoJSONDestino) : MostrarAlertaFallida(objetoJSONTemporal.Error);
}

function EstablecerObjetoJSONViajesCreadosPorUsuario() {

    try {
        //MainContent_contenedorJSONViajesCreadosPorUsuario
        //TratamientoJSONGeneral($("#" + "contenedorJSONViajesCreadosPorUsuario").val(), "objetoJSONViajesCreadosPorUsuario");
        TratamientoJSONGeneral($("#MainContent_contenedorJSONViajesCreadosPorUsuario").val(), "objetoJSONViajesCreadosPorUsuario");
    } catch (err) {
        //MostrarAlertaFallida(err.message);
    }

    ObtenerJSONViajesCreados();
}

var viajesCreadosJSONNuevo = null;

function ObtenerJSONViajesCreados() {
    viajesCreadosJSONNuevo = objetoJSONViajesCreadosPorUsuario;
}

var arregloEncabezadosGeneralViajesAceptados = [
    'PATENTE_TRACTO',
    'PATENTE_TRAILER',
    'NOMBRETRANSPORTISTA',
    'NOMBRECONDUCTOR',
    'FECHACREACION'
];

var arregloEncabezadosGeneralViajes = [
    'Tracto',
    'Trailer',
    'Transportista',
    'Conductor',
    'Creación',
    'Ver más detalles'
];

var arregloEncabezadosGeneralViajesMasDetallesAceptados = [
    'NUMTRANSPORTE',
    'IDEMBARQUE',
    'PATENTE_TRACTO',
    'PATENTE_TRAILER',
    'RUTTRANSPORTISTA',
    'NOMBRETRANSPORTISTA',
    'NOMBRECONDUCTOR',
    'FECHACREACION',
    'TIPOVIAJE',
    'ESTADOTRACTO',
    'ESTADOTRAILER'
];

var arregloEncabezadosGeneralViajesMasDetalles = [
    'Numero de Transporte',
    'Id Embarque',
    'Tracto',
    'Trailer',
    'Ruta',
    'Transportista',
    'Conductor',
    'Fecha Creación',
    'Tipo Viaje',
    'Estado Tracto',
    'Estado Trailer'
];

function ObtenerEncabezadoTablaHTMLGeneralViajes(encabezado) {
    var contenidoHTML = [];
    contenidoHTML.push('<td>');
    contenidoHTML.push('<b>');
    contenidoHTML.push(encabezado);
    contenidoHTML.push('</b>');
    contenidoHTML.push('</td>');
    return contenidoHTML.join("");
}

function ObtenerEncabezadosTablaGeneral(arregloEncabezados) {
    var contenidoHTML = [];

    contenidoHTML.push('<tr>');

    $.each(arregloEncabezados, function (key, val) {
        contenidoHTML.push(ObtenerEncabezadoTablaHTMLGeneralViajes(val));
    });

    contenidoHTML.push('</tr>');

    return contenidoHTML.join("");
}

function ObtenerEncabezadosTablaHTMLGeneralViajes() {
    return ObtenerEncabezadosTablaGeneral(arregloEncabezadosGeneralViajes);
}

function ObtenerEncabezadosTablaHTMLGeneralViajesMasDetalles() {
    var contenidoHTML = [];

    contenidoHTML.push('<tr>');

    $.each(arregloEncabezadosGeneralViajesMasDetalles, function (key, val) {

        if (val == "Transportista")
            contenidoHTML.push('</tr><tr id="nuevorenglontr" >');

        contenidoHTML.push(ObtenerEncabezadoTablaHTMLGeneralViajes(val));
    });

    contenidoHTML.push('</tr>');

    return contenidoHTML.join("");
}

function ObtenerFechaFormateadaSiEsFecha(key, val) {
    if (key == "FECHACREACION")
        val = CrearFechaFormatoDDMMYYYYBD(val);

    return val;
}

function CrearTablaHTMLGeneralViajes() {
    var contadorViajes = 0;
    var contenidoHTML = [];
    contenidoHTML.push('<div style="height: 400px!important; overflow-y : auto!important;">');
    contenidoHTML.push('<table style="width: 900px;" class="table table-striped">');

    contenidoHTML.push(ObtenerEncabezadosTablaHTMLGeneralViajes());

    var elemento = viajesCreadosJSONNuevo;

    $.each(elemento, function (key1, val1) {

        contenidoHTML.push('<tr>');
        var elemento1 = val1;
        $.each(elemento1, function (key2, val2) {

            var coincideColumnaAceptada = false;
            $.each(arregloEncabezadosGeneralViajesAceptados, function (key, val) {
                if (val == key2)
                    coincideColumnaAceptada = true;
            });

            if (coincideColumnaAceptada) {
                contenidoHTML.push('<td>');

                val2 = ObtenerFechaFormateadaSiEsFecha(key2, val2);

                contenidoHTML.push(val2);
                contenidoHTML.push('</td>');
            }

        });
        contenidoHTML.push('<td>');
        contenidoHTML.push('<a style="color:#fff;" class="enlaceVerMasInformacion btn btn-primary" indiceviaje="' + contadorViajes + '" href="#">');
        contenidoHTML.push("<i class='fa fa-eye'></i>&nbsp;&nbsp;ver más información");
        contenidoHTML.push('</a>');
        contenidoHTML.push('</td>');

        contenidoHTML.push('</tr>');

        contadorViajes++;
    });

    contenidoHTML.push('</table>');
    contenidoHTML.push('</div>');
    $("#contenedorViajes").html(contenidoHTML.join(""));
}

function ObtenerUsuario() {
    return $("#MainContent_contenedorNombreUsuario").val();//"Miguel Angel Sánchez López";
}

function MostrarMensajeSWAL_HTML(titulo, contenidoHTML) {
    swal({
        title: titulo,
        html: contenidoHTML,
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: 'Aceptar',
        width: "1000px"
    });
}

function MostrarVentanaEjemplo(contenidoHTML) {
    MostrarMensajeSWAL_HTML('Viajes creados por el usuario ' + ObtenerUsuario(), contenidoHTML);
    AsignarComportamientoBotonVerMasInfoViaje();
}

function AsignarComportamientoBotonVerViajesCreados() {
    $("#VerViajesCreados").on("click", function () {
        var contenidoHTML = $("#contenedorViajes").html();
        MostrarVentanaEjemplo(contenidoHTML);
    });
}

function ArmarContenidoHTMLMasDetallesViaje(indiceviaje) {
    var contenidoHTML = [];
    contenidoHTML.push('<table>');
    contenidoHTML.push(ObtenerEncabezadosTablaHTMLGeneralViajesMasDetalles());

    var contadorViajes = 0;

    var elemento = viajesCreadosJSONNuevo;

    $.each(elemento, function (key1, val1) {

        if (contadorViajes == indiceviaje) {
            contenidoHTML.push('<tr>');
            var elemento1 = val1;
            $.each(elemento1, function (key2, val2) {

                var coincideColumnaAceptada = false;
                $.each(arregloEncabezadosGeneralViajesMasDetallesAceptados, function (key, val) {
                    if (val == key2) {

                        if (val == "NOMBRETRANSPORTISTA")
                            contenidoHTML.push('</tr><tr id="nuevorenglontrdatos" ></tr><tr>');

                        coincideColumnaAceptada = true;
                    }
                });

                if (coincideColumnaAceptada) {
                    contenidoHTML.push('<td>');

                    val2 = ObtenerFechaFormateadaSiEsFecha(key2, val2);

                    contenidoHTML.push(val2);
                    contenidoHTML.push('</td>');
                }

            });
            contenidoHTML.push('</tr>');
        }
        contadorViajes++;

    });
    contenidoHTML.push('</table>');

    $("#contenedorViajesInfoViaje").html(contenidoHTML.join(""));

    $("#nuevorenglontrdatos").html($("#nuevorenglontr").html());
    $("#nuevorenglontr").html("");
}

function AsignarComportamientoBotonVerMasInfoViaje() {
    $(".enlaceVerMasInformacion").on("click", function () {
        ArmarContenidoHTMLMasDetallesViaje($(this).attr("indiceviaje"));

        var contenidoHTML = $("#contenedorViajesInfoViaje").html();
        MostrarMensajeSWAL_HTML('Mas detalles del viaje', contenidoHTML);
    });
}

function RepocisionarBotonVerViajes() {
    var alturaVentanaActual = $(window).height();
    $("#VerViajesCreados").parent().css({ position: 'relative' });
    $("#VerViajesCreados").css({ top: (alturaVentanaActual-40), left: 10, position: 'absolute' });
    $("#VerViajesCreados").parent().css('z-index', 3000);
}

$(document).ready(function () {
    EstablecerObjetoJSONViajesCreadosPorUsuario();

    AsignarComportamientoBotonVerViajesCreados();
    CrearTablaHTMLGeneralViajes();
    RepocisionarBotonVerViajes();
    //$(".enlaceViajesCreados").css("pointer-events", "auto !important;");
});