﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PosicionesGPS.aspx.cs" Inherits="Track_Web.PosicionesGPS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    AltoTrack Platform 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
    <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
    <script src="Scripts/TopMenu.js" type="text/javascript"></script>
    <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
    <script src="Scripts/markerwithlabel.js" type="text/javascript"></script>

    <script type="text/javascript">
        var infowindow = new google.maps.InfoWindow();
        var geoLayer = new Array();
        var arrayPositions = new Array();
        var trafficLayer = new google.maps.TrafficLayer();

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.Ajax.timeout = 3600000;
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                               /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

            //Verifica si se debe controlar tiempo de expiración de sesión
            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);

                        if (data > 0) {
                            Ext.ns('App');

                            //Session timeout in secons     
                            App.SESSION_TIMEOUT = data;

                            // Helper that converts minutes to milliseconds.
                            App.toMilliseconds = function (minutes) {
                                return minutes * 60 * 1000;
                            }

                            // Notifies user that her session has timed out.
                            App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                                Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                                Ext.MessageBox.show({
                                    title: "Sesión expirada.",
                                    msg: "Su sesión ha expirado.",
                                    icon: Ext.MessageBox.WARNING,
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        window.location = "Login.aspx";
                                    }
                                });

                            });

                            // Starts the session timeout workflow after an AJAX request completes.
                            Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                                // Reset the client-side session timeout timers.
                                App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                            });

                        }
                    }
                }
            })

            var storeZonas = new Ext.data.JsonStore({
                id: 'storeZonas',
                autoLoad: true,
                fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboZonas = new Ext.form.field.ComboBox({
                id: 'comboZonas',
                store: storeZonas
            });

            var dateDesde = new Ext.form.DateField({
                id: 'dateDesde',
                fieldLabel: 'Desde',
                labelWidth: 100,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                maxValue: new Date(),
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
            });

            dateDesde.on('change', function () {
                var _desde = Ext.getCmp('dateDesde');
                var _hasta = Ext.getCmp('dateHasta');

                _hasta.setValue(_desde.getValue());
                _hasta.setMinValue(_desde.getValue());
                _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 14));
                _hasta.validate();
            });

            var hourDesde = {
                xtype: 'timefield',
                id: 'hourDesde',
                allowBlank: false,
                format: 'H:i',
                minValue: '00:00',
                maxValue: '23:59',
                increment: 10,
                anchor: '99%',
                editable: true,
                value: '00:00',
                style: {
                    marginTop: '5px'
                }
            };

            var dateHasta = new Ext.form.DateField({
                id: 'dateHasta',
                fieldLabel: 'Hasta',
                labelWidth: 100,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                minValue: Ext.getCmp('dateDesde').getValue(),
                maxValue: new Date(),
                style: {
                    marginLeft: '5px'
                },
            });

            var hourHasta = {
                xtype: 'timefield',
                id: 'hourHasta',
                allowBlank: false,
                format: 'H:i',
                minValue: '00:00',
                maxValue: '23:59',
                increment: 10,
                anchor: '99%',
                editable: true,
                value: new Date(),
                style: {
                    marginTop: '5px'
                },
            };

            var storeFiltroTransportista = new Ext.data.JsonStore({
                autoLoad: true,
                fields: ['Transportista'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroTransportista = new Ext.form.field.ComboBox({
                id: 'comboFiltroTransportista',
                fieldLabel: 'Línea Transporte',
                labelWidth: 100,
                forceSelection: true,
                store: storeFiltroTransportista,
                valueField: 'Transportista',
                displayField: 'Transportista',
                queryMode: 'local',
                anchor: '99%',
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: false,
                forceSelection: true,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (field, newVal) {
                        if (newVal != null) {
                            FiltrarPatentes();
                        }
                        Ext.getCmp('comboFiltroPatente').reset();
                    }
                }
            });

            var storeFiltroPatente = new Ext.data.JsonStore({
                autoLoad: true,
                fields: ['Patente'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=False',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroPatente = new Ext.form.field.ComboBox({
                id: 'comboFiltroPatente',
                fieldLabel: 'Tracto',
                labelWidth: 100,
                store: storeFiltroPatente,
                valueField: 'Patente',
                displayField: 'Patente',
                queryMode: 'local',
                anchor: '99%',
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                forceSelection: true,
                allowBlank: false,
                style: {
                    marginLeft: '5px'
                }
            });

            var chkMostrarZonasEstudio = new Ext.form.Checkbox({
                id: 'chkMostrarZonasEstudio',
                fieldLabel: 'Mostrar zonas estudio',
                labelWidth:150,
                checked: false,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            MostrarZonas();
                        }
                        else {
                            eraseAllZones();
                        }
                    }
                }
            });


            var toolbarPosiciones = Ext.create('Ext.toolbar.Toolbar', {
                id: 'toolbarPosiciones',
                height: 150,
                layout: 'column',
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.75,
                    items: [dateDesde, dateHasta]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.24,
                    items: [hourDesde, hourHasta]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroTransportista]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroPatente]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [chkMostrarZonasEstudio]
                }]
            });

            var chkMostrarTrafico = new Ext.form.Checkbox({
                id: 'chkMostrarTrafico',
                fieldLabel: 'Mostrar tráfico',
                labelWidth: 90,
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            trafficLayer.setMap(map);
                        }
                        else {
                            trafficLayer.setMap(null);
                        }
                    }
                }
            });

            var btnExportar = {
                id: 'btnExportar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/export_black_20x20.png',
                text: 'Exportar',
                width: 80,
                height: 26,
                style: {
                    marginLeft: '20px'
                },
                listeners: {
                    click: {
                        element: 'el',
                        fn: function () {

                            if (Ext.getCmp("gridPosiciones").getStore().count() == 0) {
                                return;
                            }

                            var desde = Ext.getCmp('dateDesde').getRawValue();
                            var hasta = Ext.getCmp('dateHasta').getRawValue();
                            var horaDesde = Ext.getCmp('hourDesde').getRawValue();
                            var horaHasta = Ext.getCmp('hourHasta').getRawValue();

                            var transportista = Ext.getCmp('comboFiltroTransportista').getRawValue();
                            var patente = Ext.getCmp('comboFiltroPatente').getValue();

                            var mapForm = document.createElement("form");
                            mapForm.target = "ToExcel";
                            mapForm.method = "POST"; // or "post" if appropriate
                            mapForm.action = 'PosicionesGPS.aspx?Metodo=ExportExcel';

                            //
                            var _desde = document.createElement("input");
                            _desde.type = "text";
                            _desde.name = "desde";
                            _desde.value = desde;
                            mapForm.appendChild(_desde);

                            var _horaDesde = document.createElement("input");
                            _horaDesde.type = "text";
                            _horaDesde.name = "horaDesde";
                            _horaDesde.value = horaDesde;
                            mapForm.appendChild(_horaDesde);

                            var _hasta = document.createElement("input");
                            _hasta.type = "text";
                            _hasta.name = "hasta";
                            _hasta.value = hasta;
                            mapForm.appendChild(_hasta);

                            var _horaHasta = document.createElement("input");
                            _horaHasta.type = "text";
                            _horaHasta.name = "horaHasta";
                            _horaHasta.value = horaHasta;
                            mapForm.appendChild(_horaHasta);

                            var _patente = document.createElement("input");
                            _patente.type = "text";
                            _patente.name = "patente";
                            _patente.value = patente;
                            mapForm.appendChild(_patente);

                            document.body.appendChild(mapForm);
                            mapForm.submit();

                        }
                    }
                }
            };

            var btnBuscar = {
                id: 'btnBuscar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/searchreport_black_20x20.png',
                text: 'Buscar',
                width: 90,
                height: 26,
                style: {
                    marginLeft: '20px'
                },
                handler: function () {
                    Buscar();
                }
            };

            var btnCancelar = {
                id: 'btnCancelar',
                xtype: 'button',
                width: 90,
                height: 26,
                iconAlign: 'left',
                icon: 'Images/back_black_20x20.png',
                text: 'Cancelar',

                handler: function () {
                    Cancelar();
                }
            };

            var storeZonasToDraw = new Ext.data.JsonStore({
                id: 'storeZonasToDraw',
                autoLoad: false,
                fields: ['IdZona'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonasToDraw',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridZonasToDraw = Ext.create('Ext.grid.Panel', {
                id: 'gridZonasToDraw',
                store: storeZonasToDraw,
                columns: [
                          { text: 'IdZona', flex: 1, dataIndex: 'IdZona' }
                ]

            });

            var storePosiciones = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['Patente',
                          'IdTipoMovil',
                          'NombreTipoMovil',
                          'Transportista',
                          { name: 'Fecha', type: 'date', dateFormat: 'c' },
                          'Latitud',
                          'Longitud',
                          'Velocidad',
                          'Direccion',
                          'Ignicion',
                ],
                proxy: new Ext.data.HttpProxy({
                    //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPosicionesGPS_Ruta',
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPosicionesGPS',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPosiciones = Ext.create('Ext.grid.Panel', {
                id: 'gridPosiciones',
                store: storePosiciones,
                tbar: toolbarPosiciones,
                columnLines: true,
                anchor: '100% 100%',
                scroll: false,
                buttons: [chkMostrarTrafico, btnExportar, btnBuscar, btnCancelar],
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                              { text: 'Fecha', width: 110, dataIndex: 'Fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                              { text: 'Placa', width: 60, dataIndex: 'Patente' },
                              { text: 'IdTipoMovil', dataIndex: 'IdTipoMovil', hidden: true },
                              { text: 'NombreTipoMovil', dataIndex: 'NombreTipoMovil', hidden: true },
                              { text: 'Línea Transporte', flex: 1, dataIndex: 'Transportista' },
                              { text: 'Latitud', dataIndex: 'Latitud', hidden: true },
                              { text: 'Longitud', dataIndex: 'Longitud', hidden: true },
                              { text: 'Vel.', width: 35, dataIndex: 'Velocidad' },
                              { text: 'Direccion', dataIndex: 'Direccion', hidden: true },
                              { text: 'Ignición', width: 55, dataIndex: 'Ignicion' }
                ],
                listeners: {
                    select: function (sm, row, rec) {

                        var date = Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.Fecha.toString();

                        for (var i = 0; i < markers.length; i++) {
                            if (markers[i].labelText == date) {
                                markers[i].setAnimation(google.maps.Animation.BOUNCE);
                                setTimeout('markers[' + i + '].setAnimation(null);', 800);
                            }
                        }

                        Ext.getCmp('textFecha').setValue(date.replace("T", " "));
                        Ext.getCmp('textVelocidad').setValue(row.data.Velocidad);
                        Ext.getCmp('textLatitud').setValue(row.data.Latitud);
                        Ext.getCmp('textLongitud').setValue(row.data.Longitud);

                        map.setCenter(new google.maps.LatLng(row.data.Latitud, row.data.Longitud));
                        //map.setZoom(16);

                        Ext.getCmp("gridPosiciones").getSelectionModel().deselectAll();

                    }
                }
            });

            var textFecha = new Ext.form.TextField({
                id: 'textFecha',
                fieldLabel: 'Fecha',
                labelWidth: 60,
                anchor: '99%',
                readOnly: true
            });

            var textVelocidad = new Ext.form.TextField({
                id: 'textVelocidad',
                fieldLabel: 'Velocidad',
                labelWidth: 60,
                anchor: '99%',
                readOnly: true
            });

            var textLatitud = new Ext.form.TextField({
                id: 'textLatitud',
                fieldLabel: 'Latitud',
                labelWidth: 60,
                anchor: '99%',
                readOnly: true
            });

            var textLongitud = new Ext.form.TextField({
                id: 'textLongitud',
                fieldLabel: 'Longitud',
                labelWidth: 60,
                anchor: '99%',
                readOnly: true
            });

            var viewWidth = Ext.getBody().getViewSize().width;
            var viewHeight = Ext.getBody().getViewSize().height;

            var winDetallesPunto = new Ext.Window({
                id: 'winDetallesPunto',
                title: 'Detalles',
                width: 210,
                height: 150,
                closable: false,
                collapsible: true,
                modal: false,
                initCenter: false,
                x: viewWidth - 220,
                y: 50,
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    style: 'padding-top:3px;padding-left:5px;',
                    items: [textFecha]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    style: 'padding-left:5px;',
                    items: [textVelocidad]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    style: 'padding-left:5px;',
                    items: [textLatitud]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    style: 'padding-left:5px;',
                    items: [textLongitud]
                }],
                resizable: false,
                border: true,
                draggable: false
            }).show();

            var leftPanel = new Ext.FormPanel({
                id: 'leftPanel',
                region: 'west',
                margins: '0 0 3 3',
                border: true,
                width: 480,
                minWidth: 300,
                maxWidth: viewWidth / 2,
                layout: 'anchor',
                split: true,
                collapsible: true,
                items: [gridPosiciones]
            });

            leftPanel.on('collapse', function () {
                google.maps.event.trigger(map, "resize");
            });

            leftPanel.on('expand', function () {
                google.maps.event.trigger(map, "resize");
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                region: 'center',
                border: true,
                margins: '0 3 3 0',
                anchor: '100% 100%',
                contentEl: 'dvMap'
            });

            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, leftPanel, centerPanel]
            });

            viewport.on('resize', function () {
                google.maps.event.trigger(map, "resize");
                Ext.getCmp('winDetallesPunto').setPosition(Ext.getBody().getViewSize().width - 220, 50, true)
            });

        });

    </script>

    <script type="text/javascript">

        var zoneLabels = new Array();

        Ext.onReady(function () {
            GeneraMapa("dvMap", true);
        });

        function FiltrarPatentes() {
            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

            var store = Ext.getCmp('comboFiltroPatente').store;
            store.load({
                params: {
                    transportista: transportista
                }
            });
        }

        function Buscar() {

            if (!Ext.getCmp('leftPanel').getForm().isValid()) {
                return;
            }

            Ext.Msg.wait('Espere por favor...', 'Generando posiciones');

            Ext.getCmp('gridPosiciones').store.removeAll();
            ClearMap();
            Ext.getCmp('textFecha').reset();
            Ext.getCmp('textVelocidad').reset();
            Ext.getCmp('textLatitud').reset();
            Ext.getCmp('textLongitud').reset();

            var desde = Ext.getCmp('dateDesde').getValue();
            var hasta = Ext.getCmp('dateHasta').getValue();
            var horaDesde = Ext.getCmp('hourDesde').getRawValue();
            var horaHasta = Ext.getCmp('hourHasta').getRawValue();
            var patente = Ext.getCmp('comboFiltroPatente').getValue();

            var storePos = Ext.getCmp('gridPosiciones').store;
            var storeZone = Ext.getCmp('gridZonasToDraw').store;

            storePos.load({
                params: {
                    fechaDesde: desde,
                    fechaHasta: hasta,
                    HoraDesde: horaDesde,
                    HoraHasta: horaHasta,
                    patente: patente
                },
                callback: function (r, options, success) {
                    if (success) {

                        storeZone.load({
                            params: {
                                fechaDesde: desde,
                                fechaHasta: hasta,
                                patente1: patente,
                                patente2: ''
                            },
                            callback: function (r, options, success) {
                                if (success) {
                                    Ext.Msg.hide();
                                    MuestraRutaViaje();

                                    var store = Ext.getCmp('gridZonasToDraw').getStore();
                                    for (var i = 0; i < store.count() ; i++) {
                                        DrawZone(store.getAt(i).data.IdZona);
                                    }

                                }
                            }

                        });

                    }

                }
            });

        }

        function Cancelar() {

            arrayPositions.splice(0, arrayPositions.length);

            Ext.getCmp('dateDesde').reset();
            Ext.getCmp('hourDesde').reset();
            Ext.getCmp('dateHasta').reset();
            Ext.getCmp('hourHasta').reset();
            Ext.getCmp('comboFiltroTransportista').reset();
            Ext.getCmp('comboFiltroPatente').reset();
            Ext.getCmp('comboFiltroPatente').store.load();

            Ext.getCmp('gridPosiciones').store.removeAll();
            ClearMap();

            for (var i = 0; i < geoLayer.length; i++) {
                geoLayer[i].layer.setMap(null);
                geoLayer[i].label.setMap(null);
            }
            geoLayer.splice(0, geoLayer.length);

            Ext.getCmp('textFecha').reset();
            Ext.getCmp('textVelocidad').reset();
            Ext.getCmp('textLatitud').reset();
            Ext.getCmp('textLongitud').reset();
        }

        function MuestraRutaViaje() {

            var store = Ext.getCmp('gridPosiciones').getStore();
            var rowCount = store.count();
            var iterRow = 0;

            while (iterRow < rowCount) {

                var dir = parseInt(store.data.items[iterRow].raw.Direccion);

                var lat = store.data.items[iterRow].raw.Latitud;
                var lon = store.data.items[iterRow].raw.Longitud;

                var Latlng = new google.maps.LatLng(lat, lon);

                arrayPositions.push({
                    Fecha: store.data.items[iterRow].raw.Fecha.toString(),
                    Velocidad: store.data.items[iterRow].raw.Velocidad,
                    Latitud: lat,
                    Longitud: lon,
                    LatLng: Latlng
                });
 
                if (store.data.items[iterRow].raw.Velocidad > 0) {
                    switch (true) {
                        case ( store.data.items[iterRow].raw.Velocidad < 80):
                            var marker = new MarkerWithLabel({
                                position: Latlng,
                                map: map,
                                labelContent: store.data.items[iterRow].raw.Velocidad,
                                labelAnchor: new google.maps.Point(15, 19),
                                labelClass: "labelsBlack", // the CSS class for the label
                                labelInBackground: false,
                                labelText: store.data.items[iterRow].raw.Fecha.toString(),
                                icon: 'Images/rectangle_green_25x25.png'
                            });
                            break;
                        case ((store.data.items[iterRow].raw.Velocidad >= 80) && (store.data.items[iterRow].raw.Velocidad < 95) ):
                            var marker = new MarkerWithLabel({
                                position: Latlng,
                                map: map,
                                labelContent: store.data.items[iterRow].raw.Velocidad,
                                labelAnchor: new google.maps.Point(15, 19),
                                labelClass: "labelsBlack", // the CSS class for the label
                                labelInBackground: false,
                                labelText: store.data.items[iterRow].raw.Fecha.toString(),
                                icon: 'Images/rectangle_yellow_25x25.png'
                            });
                            break;
                        case ( store.data.items[iterRow].raw.Velocidad >= 95):
                            var marker = new MarkerWithLabel({
                                position: Latlng,
                                map: map,
                                labelContent: store.data.items[iterRow].raw.Velocidad,
                                labelAnchor: new google.maps.Point(15, 19),
                                labelClass: "labelsWhite", // the CSS class for the label
                                labelInBackground: false,
                                labelText: store.data.items[iterRow].raw.Fecha.toString(),
                                icon: 'Images/rectangle_red_25x25.png'
                            });
                            break;
                    }
                }
                else {
                    marker = new google.maps.Marker({
                        position: Latlng,
                        icon: 'Images/dot_red_16x16.png',
                        map: map,
                        labelText: store.data.items[iterRow].raw.Fecha.toString()
                    });
                }

                var label = new Label({
                    map: null,
                });

                label.bindTo('position', marker, 'position');
                label.bindTo('text', marker, 'labelText');

                google.maps.event.addListener(marker, 'click', function () {
                    var latLng = this.position;
                    var fec = this.labelText;
                    var latitudObtenida = "";
                    var longitudObtenida = "";

                    for (i = 0; i < arrayPositions.length; i++) {
                        if (arrayPositions[i].Fecha.toString() == fec.toString() & arrayPositions[i].LatLng.toString() == latLng.toString()) {
                            latitudObtenida = arrayPositions[i].Latitud;
                            longitudObtenida = arrayPositions[i].Longitud;

                            Ext.getCmp('textFecha').setValue((arrayPositions[i].Fecha.toString()).replace("T", " "));
                            Ext.getCmp('textVelocidad').setValue(arrayPositions[i].Velocidad);
                            Ext.getCmp('textLatitud').setValue(arrayPositions[i].Latitud);
                            Ext.getCmp('textLongitud').setValue(arrayPositions[i].Longitud);
                            break;
                        }
                    }

                    var contentString =
                    '<br>' +
                    '<table>' +
                    obtenerIframeVistaCalles(latitudObtenida, longitudObtenida) +
                    '</table>' +
                    '<br>';

                    infowindow.setContent(contentString);
                    infowindow.open(map, this);

                });

                markers.push(marker);
                labels.push(label);


                iterRow++;
            }

            if (rowCount > 0) {
                map.setCenter(markers[markers.length - 1].position);
            }

        }



        function DrawZone(idZona) {

            for (var i = 0; i < geoLayer.length; i++) {
                geoLayer[i].layer.setMap(null);
                geoLayer[i].label.setMap(null);
                geoLayer.splice(i, 1);
            }

            //var colorZone = "#7f7fff";

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
                params: {
                    IdZona: idZona
                },
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        if (data.Vertices.length > 1) { //Polygon
                            var polygonGrid = new Object();
                            polygonGrid.IdZona = data.IdZona;

                            var arr = new Array();
                            for (var i = 0; i < data.Vertices.length; i++) {
                                arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
                            }

                            if (data.idTipoZona == 3) {
                                var colorZone = "#FF0000";
                            }
                            else {
                                var colorZone = "#7f7fff";
                            }

                            polygonGrid.layer = new google.maps.Polygon({
                                paths: arr,
                                strokeColor: "#000000",
                                strokeWeight: 1,
                                strokeOpacity: 0.9,
                                fillColor: colorZone,
                                fillOpacity: 0.3,
                                labelText: data.NombreZona
                            });
                            polygonGrid.label = new Label({
                                position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                map: map
                            });
                            polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
                            polygonGrid.layer.setMap(map);
                            geoLayer.push(polygonGrid);
                        }
                        else
                            if (data.Vertices.length = 1) { //Point
                                var Point = new Object();
                                Point.IdZona = data.IdZona;

                                var image = new google.maps.MarkerImage("Images/greymarker_32x32.png");

                                Point.layer = new google.maps.Marker({
                                    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                    icon: image,
                                    labelText: data.NombreZona,
                                    map: map
                                });

                                Point.label = new Label({
                                    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                    map: map
                                });

                                Point.label.bindTo('text', Point.layer, 'labelText');
                                Point.layer.setMap(map);
                                geoLayer.push(Point);
                            }

                    }
                },
                failure: function (msg) {
                    alert('Se ha producido un error.');
                }
            });
        }

        function MostrarZonas() {

            Ext.Msg.wait('Espere por favor...', 'Generando zonas');

            var countZonas = Ext.getCmp('comboZonas').store.count()

            for (i = 0; i < countZonas; i++) {
                var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
                var idTipoZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdTipoZona;

                if (idTipoZona == 16) {
                    DrawZone(idZona, idTipoZona);
                }
            }

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
                success: function (response, opts) {

                    var task = new Ext.util.DelayedTask(function () {
                        Ext.Msg.hide();
                    });

                    task.delay(1500);

                },
                failure: function (response, opts) {
                    Ext.Msg.hide();
                }
            });
        }

        function eraseAllZones() {
            var countZonas = Ext.getCmp('comboZonas').store.count()

            for (i = 0; i < countZonas; i++) {
                var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
                EraseZone(idZona);
            }

            zoneLabels = [];
        }

        function EraseZone(idZona) {
            for (var i = 0; i < geoLayer.length; i++) {
                if (idZona == geoLayer[i].IdZona) {
                    geoLayer[i].layer.setMap(null);
                    geoLayer[i].label.setMap(null);
                    geoLayer.splice(i, 1);
                }
            }

            for (var i = 0; i < zoneLabels.length; i++) {
                if (zoneLabels[i].text == idZona) {
                    zoneLabels[i].setMap(null);
                    zoneLabels.splice(i, 1);

                }
            }

        }

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
    <div id="dvMap"></div>
</asp:Content>
