﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViajesRuta.aspx.cs" Inherits="Track_Web.ViajesRuta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
    <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
    <script src="Scripts/TopMenu.js" type="text/javascript"></script>
    <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
    <script src="Scripts/RowExpander.js" type="text/javascript"></script>
    <script src="Scripts/markerwithlabel.js" type="text/javascript"></script>

    <script type="text/javascript">

        var geoLayer = new Array();
        var arrayPositions = new Array();
        var arrayAlerts = new Array();
        var trafficLayer = new google.maps.TrafficLayer();
        var infowindow = new google.maps.InfoWindow();
        var arrayHouses = new Array();

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.Ajax.timeout = 3600000;
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                                /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                 toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

            //Verifica si se debe controlar tiempo de expiración de sesión
            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);

                        if (data > 0) {
                            Ext.ns('App');

                            //Session timeout in secons     
                            App.SESSION_TIMEOUT = data;

                            // Helper that converts minutes to milliseconds.
                            App.toMilliseconds = function (minutes) {
                                return minutes * 60 * 1000;
                            }

                            // Notifies user that her session has timed out.
                            App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                                Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                                Ext.MessageBox.show({
                                    title: "Sesión expirada.",
                                    msg: "Su sesión ha expirado.",
                                    icon: Ext.MessageBox.WARNING,
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        window.location = "Login.aspx";
                                    }
                                });

                            });

                            // Starts the session timeout workflow after an AJAX request completes.
                            Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                                // Reset the client-side session timeout timers.
                                App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                            });

                        }
                    }
                }
            })

            var chkNroTransporte = new Ext.form.Checkbox({
                id: 'chkNroTransporte',
                labelSeparator: '',
                hideLabel: true,
                checked: false,
                style: {
                    marginTop: '7px',
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            Ext.getCmp("textFiltroNroTransporte").setDisabled(false);
                            Ext.getCmp("dateDesde").setDisabled(true);
                            Ext.getCmp("dateHasta").setDisabled(true);
                            Ext.getCmp("comboFiltroTransportista").setDisabled(true);
                            Ext.getCmp("comboFiltroPatente").setDisabled(true);
                            Ext.getCmp("comboFiltroEstadoViaje").setDisabled(true);
                            Ext.getCmp("comboFiltroLocales").setDisabled(true);

                        }
                        else {
                            Ext.getCmp("textFiltroNroTransporte").setDisabled(true);
                            Ext.getCmp("dateDesde").setDisabled(false);
                            Ext.getCmp("dateHasta").setDisabled(false);
                            Ext.getCmp("comboFiltroTransportista").setDisabled(false);
                            Ext.getCmp("comboFiltroPatente").setDisabled(false);
                            Ext.getCmp("comboFiltroEstadoViaje").setDisabled(false);
                            Ext.getCmp('textFiltroNroTransporte').reset();
                            Ext.getCmp("comboFiltroLocales").setDisabled(false);
                        }
                    }
                }
            });

            var textFiltroNroTransporte = new Ext.form.TextField({
                id: 'textFiltroNroTransporte',
                fieldLabel: 'ID Master',
                anchor: '99%',
                labelWidth: 80,
                maxLength: 20,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                disabled: true,
            });

            var dateDesde = new Ext.form.DateField({
                id: 'dateDesde',
                fieldLabel: 'Desde',
                labelWidth: 110,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                maxValue: new Date(),
                style: {
                    marginLeft: '5px'
                },
                disabled: false
            });

            var dateHasta = new Ext.form.DateField({
                id: 'dateHasta',
                fieldLabel: 'Hasta',
                labelWidth: 80,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                minValue: Ext.getCmp('dateDesde').getValue(),
                maxValue: new Date(),
                style: {
                    marginLeft: '5px'
                },
                disabled: false
            });

            dateDesde.on('change', function () {
                var _desde = Ext.getCmp('dateDesde');
                var _hasta = Ext.getCmp('dateHasta');

                _hasta.setMinValue(_desde.getValue());
                _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 60));
                _hasta.validate();
                //FiltrarViajes();
            });

            dateHasta.on('change', function () {
                var _desde = Ext.getCmp('dateDesde');
                var _hasta = Ext.getCmp('dateHasta');

                _desde.setMinValue(Ext.Date.add(_hasta.getValue(), Ext.Date.DAY, -60));
                //_desde.setMaxValue(_hasta.getValue());
                _desde.validate();
                //FiltrarViajes();
            });

            Ext.getCmp('dateDesde').setMinValue(Ext.Date.add(Ext.getCmp('dateHasta').getValue(), Ext.Date.DAY, -60));
            Ext.getCmp('dateHasta').setMaxValue(Ext.Date.add(Ext.getCmp('dateDesde').getValue(), Ext.Date.DAY, 60));

            var storeFiltroPatente = new Ext.data.JsonStore({
                fields: ['Patente'],
                proxy: new Ext.data.HttpProxy({
                    //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPatentesRuta&Todos=True',
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroPatente = new Ext.form.field.ComboBox({
                id: 'comboFiltroPatente',
                fieldLabel: 'Placa',
                forceSelection: true,
                store: storeFiltroPatente,
                valueField: 'Patente',
                displayField: 'Patente',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 80,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                forceSelection: true,
                disabled: false
            });

            var storeFiltroTransportista = new Ext.data.JsonStore({
                fields: ['Transportista'],
                proxy: new Ext.data.HttpProxy({
                    //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetTransportistasRuta&Todos=True',
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroTransportista = new Ext.form.field.ComboBox({
                id: 'comboFiltroTransportista',
                fieldLabel: 'Línea Transporte',
                forceSelection: true,
                store: storeFiltroTransportista,
                valueField: 'Transportista',
                displayField: 'Transportista',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 110,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: false,
                forceSelection: true,
                listeners: {
                    select: function () {
                        FiltrarPatentes();
                    }
                },
                disabled: false
            });

            var storeFiltroEstadoViaje = new Ext.data.JsonStore({
                fields: ['EstadoViaje'],
                data: [{ "EstadoViaje": "Todos" },
                { "EstadoViaje": "Asignado" },
                { "EstadoViaje": "En Ruta" },
                { "EstadoViaje": "En Local" },
                { "EstadoViaje": "Finalizado" }
                    //{ "EstadoViaje": "Cerrado por Sistema" }
                ]
            });

            var comboFiltroEstadoViaje = new Ext.form.field.ComboBox({
                id: 'comboFiltroEstadoViaje',
                fieldLabel: 'Estado',
                store: storeFiltroEstadoViaje,
                valueField: 'EstadoViaje',
                displayField: 'EstadoViaje',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 80,
                editable: false,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            Ext.getCmp('comboFiltroEstadoViaje').setValue('Todos');

            var storeFiltroProveedorGPS = new Ext.data.JsonStore({
                fields: ['ProveedorGPS'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetProveedoresGPS&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroProveedorGPS = new Ext.form.field.ComboBox({
                id: 'comboFiltroProveedorGPS',
                fieldLabel: 'Proveedor',
                forceSelection: true,
                store: storeFiltroProveedorGPS,
                valueField: 'ProveedorGPS',
                displayField: 'ProveedorGPS',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 80,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                forceSelection: true
            });

            storeFiltroProveedorGPS.load({
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp("comboFiltroProveedorGPS").setValue("Todos");
                    }
                }
            })

            var storeFiltroLocales = new Ext.data.JsonStore({
                autoLoad: true,
                fields: ['CodigoInterno', 'IdFormato', 'NumeroLocal'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetLocales',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroLocales = new Ext.form.field.ComboBox({
                id: 'comboFiltroLocales',
                fieldLabel: 'Tienda',
                store: storeFiltroLocales,
                valueField: 'CodigoInterno',
                displayField: 'NumeroLocal',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 110,
                allowBlank: false,
                editable: true,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            storeFiltroLocales.load({
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp("comboFiltroLocales").setValue(0);
                    }
                }
            });

            var storeFiltroTipoViaje = new Ext.data.JsonStore({
                fields: ['TipoViaje'],
                data: [{ "TipoViaje": "Todos" },
                { "TipoViaje": "Tradicional" },
                { "TipoViaje": "LogisticaInv" },
                ]
            });

            var comboFiltroTipoViaje = new Ext.form.field.ComboBox({
                id: 'comboFiltroTipoViaje',
                fieldLabel: 'Tipo Viaje',
                store: storeFiltroTipoViaje,
                valueField: 'TipoViaje',
                displayField: 'TipoViaje',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 80,
                editable: false,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            Ext.getCmp('comboFiltroTipoViaje').setValue('Todos');

            var chkMostrarZonas = new Ext.form.Checkbox({
                id: 'chkMostrarZonas',
                fieldLabel: 'Mostrar Zonas',
                labelWidth: 80,
                minWidth: 105,
                checked: false,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            MostrarZonas();
                        }
                        else {
                            eraseAllZones();
                        }
                    }
                }
            });

            var chkMostrarLabels = new Ext.form.Checkbox({
                id: 'chkMostrarLabels',
                fieldLabel: 'Mostrar Etiquetas',
                minWidth: 130,
                checked: false,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            for (var i = 0; i < zoneLabels.length; i++) {
                                zoneLabels[i].setMap(map);
                            }
                        }
                        else {
                            for (var i = 0; i < zoneLabels.length; i++) {
                                zoneLabels[i].setMap(null);
                            }
                        }
                    }
                }
            });

            var chkMostrarTrafico = new Ext.form.Checkbox({
                id: 'chkMostrarTrafico',
                fieldLabel: 'Mostrar tráfico',
                minWidth: 115,
                checked: false,
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            trafficLayer.setMap(map);
                        }
                        else {
                            trafficLayer.setMap(null);
                        }
                    }
                }
            });

            var btnActualizar = {
                id: 'btnActualizar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/refresh_gray_20x20.png',
                text: 'Actualizar',
                width: 80,
                height: 26,
                handler: function () {
                    FiltrarViajes();
                }
            };

            var btnExportar = {
                id: 'btnExportar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/export_black_20x20.png',
                text: 'Exportar',
                width: 80,
                height: 26,
                style: {
                    marginLeft: '20px'
                },
                listeners: {
                    click: {
                        element: 'el',
                        fn: function () {

                            if (Ext.getCmp("gridPanelViajesRuta").getStore().count() == 0) {
                                return;
                            }

                            var desde = Ext.getCmp('dateDesde').getRawValue();
                            var hasta = Ext.getCmp('dateHasta').getRawValue();
                            var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getRawValue();
                            var patente = Ext.getCmp('comboFiltroPatente').getValue();
                            var estadoViaje = Ext.getCmp('comboFiltroEstadoViaje').getValue();
                            var local = Ext.getCmp('comboFiltroLocales').getValue();
                            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();
                            var tipoViaje = Ext.getCmp('comboFiltroTipoViaje').getValue();

                            var mapForm = document.createElement("form");
                            mapForm.target = "ToExcel";
                            mapForm.method = "POST"; // or "post" if appropriate
                            mapForm.action = 'ViajesRuta.aspx?Metodo=ExportExcel';

                            //
                            var _desde = document.createElement("input");
                            _desde.type = "text";
                            _desde.name = "desde";
                            _desde.value = desde;
                            mapForm.appendChild(_desde);

                            var _hasta = document.createElement("input");
                            _hasta.type = "text";
                            _hasta.name = "hasta";
                            _hasta.value = hasta;
                            mapForm.appendChild(_hasta);

                            var _nroTransporte = document.createElement("input");
                            _nroTransporte.type = "text";
                            _nroTransporte.name = "nroTransporte";
                            _nroTransporte.value = nroTransporte;
                            mapForm.appendChild(_nroTransporte);

                            var _patente = document.createElement("input");
                            _patente.type = "text";
                            _patente.name = "patente";
                            _patente.value = patente;
                            mapForm.appendChild(_patente);

                            var _estadoViaje = document.createElement("input");
                            _estadoViaje.type = "text";
                            _estadoViaje.name = "estadoViaje";
                            _estadoViaje.value = estadoViaje;
                            mapForm.appendChild(_estadoViaje);

                            var _transportista = document.createElement("input");
                            _transportista.type = "text";
                            _transportista.name = "transportista";
                            _transportista.value = transportista;
                            mapForm.appendChild(_transportista);

                            var _tipoViaje = document.createElement("input");
                            _tipoViaje.type = "text";
                            _tipoViaje.name = "tipoViaje";
                            _tipoViaje.value = tipoViaje;
                            mapForm.appendChild(_tipoViaje);

                            var _codLocal = document.createElement("input");
                            _codLocal.type = "text";
                            _codLocal.name = "codLocal";
                            _codLocal.value = local;
                            mapForm.appendChild(_codLocal);

                            document.body.appendChild(mapForm);
                            mapForm.submit();

                        }
                    }
                }
            };

            var toolbarViajesRuta = Ext.create('Ext.toolbar.Toolbar', {
                id: 'toolbarViajesRuta',
                height: 120,
                layout: 'column',
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.05,
                    items: [chkNroTransporte]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.45,
                    items: [textFiltroNroTransporte]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    items: [comboFiltroEstadoViaje]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    items: [dateDesde, comboFiltroTransportista, comboFiltroLocales]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    items: [dateHasta, comboFiltroPatente, comboFiltroTipoViaje]
                }]
            });

            var storeViajesRuta = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['NroTransporte',
                    'IdEmbarque',
                    'Fecha',
                    'SecuenciaDestino',
                    'PatenteTracto',
                    'PatenteTrailer',
                    'Transportista',
                    { name: 'FechaHoraCreacion', type: 'date', dateFormat: 'c' },
                    'CodigoOrigen',
                    'NombreOrigen',
                    'FHAsignacion',
                    'FHSalidaOrigen',
                    'CodigoDestino',
                    'NombreDestino',
                    'FHLlegadaDestino',
                    { name: 'FHCierreSistema', type: 'date', dateFormat: 'c' },
                    'TiempoViaje',
                    'FHSalidaDestino',
                    'EstadoViaje',
                    'EstadoLat',
                    'EstadoLon',
                    'DestinoLat',
                    'DestinoLon',
                    'CantidadAlertas',
                    'DescOrigen',
                    'DescDestino',
                    'EstadoReporte',
                    'Agrupacion',
                    'TipoViaje'
                ],
                proxy: new Ext.data.HttpProxy({
                    //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetViajesRuta',
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetViajesHistoricos',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            storeFiltroTransportista.load({
                callback: function (r, options, success) {
                    if (success) {
                        //Ext.getCmp("comboFiltroTransportista").store.insert(0, { Transportista: "Todos" });
                        //Ext.getCmp("comboFiltroTransportista").setValue("Todos");

                        var firstTransportista = Ext.getCmp("comboFiltroTransportista").store.getAt(0).get("Transportista");
                        Ext.getCmp("comboFiltroTransportista").setValue(firstTransportista);

                        storeFiltroPatente.load({
                            callback: function (r, options, success) {
                                if (success) {
                                    //Ext.getCmp("comboFiltroPatente").store.insert(0, { Patente: "Todas" });
                                    Ext.getCmp("comboFiltroPatente").setValue("Todas");
                                    FiltrarPatentes();

                                }
                            }
                        })

                    }
                }
            })

            var gridPanelViajesRuta = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelViajesRuta',
                title: 'Viajes',
                store: storeViajesRuta,
                anchor: '100% 70%',
                columnLines: true,
                tbar: toolbarViajesRuta,
                buttons: [chkMostrarZonas, chkMostrarLabels, chkMostrarTrafico, btnExportar, btnActualizar],
                scroll: false,
                //buttons: [btnExportar],
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                    { text: 'Id Master', sortable: true, width: 80, dataIndex: 'NroTransporte' },
                    { text: 'Id Embarque', sortable: true, width: 80, dataIndex: 'IdEmbarque' },
                    { text: 'Fecha', sortable: true, width: 70, dataIndex: 'Fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y') },
                    { text: 'Tracto', sortable: true, width: 56, dataIndex: 'PatenteTracto' },
                    { text: 'Remolque', sortable: true, width: 60, dataIndex: 'PatenteTrailer' },
                    { text: 'Línea', sortable: true, flex: 1, dataIndex: 'Transportista' },
                    { text: 'Destino', sortable: true, width: 50, dataIndex: 'CodigoDestino' },
                    { text: 'Tipo', sortable: true, width: 50, dataIndex: 'TipoViaje' },
                    { text: 'Alertas', sortable: true, width: 50, dataIndex: 'CantidadAlertas', renderer: renderCantidadAlertas },
                    { text: 'Estado', sortable: true, flex: 1, dataIndex: 'EstadoViaje', renderer: renderEstadoViaje }
                ],
                listeners: {
                    select: function (sm, row, rec) {

                        Ext.Msg.wait('Espere por favor...', 'Generando ruta');

                        var nroTransporte = row.data.NroTransporte;
                        var idEmbarque = row.data.IdEmbarque;
                        var origen = row.data.CodigoOrigen;
                        var destino = row.data.CodigoDestino;
                        var estadoViaje = row.data.EstadoViaje;
                        var patenteTracto = row.data.PatenteTracto;
                        var patenteTrailer = row.data.PatenteTrailer;
                        var FechaHoraCreacion = row.data.FechaHoraCreacion;
                        var FHSalidaOrigen = row.data.FHSalidaOrigen;
                        var FHLlegadaDestino = row.data.FHLlegadaDestino;
                        var FHCierreSistema = row.data.FHCierreSistema;

                        var estadoLat = row.data.EstadoLat;
                        var estadoLon = row.data.EstadoLon;
                        var destinoLat = row.data.DestinoLat;
                        var destinoLon = row.data.DestinoLon;

                        var lineaTransporte = row.data.Transportista;
                        var descOrigen = row.data.DescOrigen;
                        var descDestino = row.data.DescDestino;
                        var estadoReporte = row.data.EstadoReporte;
                        var agrupacion = row.data.Agrupacion;


                        ClearMap();
                        arrayPositions.splice(0, arrayPositions.length);
                        arrayAlerts.splice(0, arrayAlerts.length);
                        eraseHouses();

                        GetPosiciones(origen, destino, patenteTracto, patenteTrailer, FechaHoraCreacion, FHSalidaOrigen, FHLlegadaDestino, FHCierreSistema, nroTransporte, idEmbarque, destino, estadoViaje, lineaTransporte, descOrigen, descDestino, estadoReporte, agrupacion)
                        GetAlertasRuta(nroTransporte, idEmbarque, destino, estadoViaje);

                        if (estadoViaje == 'En Ruta' || estadoViaje == 'RUTA') {
                            CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon);
                        }
                        else {
                            Ext.getCmp('winDistanciaTiempo').hide();
                        }

                        Ext.Ajax.request({
                            url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
                            success: function (response, opts) {

                                var task = new Ext.util.DelayedTask(function () {
                                    Ext.Msg.hide();
                                });

                                task.delay(100);

                            },
                            failure: function (response, opts) {
                                Ext.Msg.hide();
                            }
                        });

                    }
                }

            });

            var storeZonas = new Ext.data.JsonStore({
                id: 'storeZonas',
                autoLoad: true,
                fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboZonas = new Ext.form.field.ComboBox({
                id: 'comboZonas',
                store: storeZonas
            });

            var storeZonasToDraw = new Ext.data.JsonStore({
                id: 'storeZonasToDraw',
                autoLoad: false,
                fields: ['IdZona'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonasToDraw',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridZonasToDraw = Ext.create('Ext.grid.Panel', {
                id: 'gridZonasToDraw',
                store: storeZonasToDraw,
                columns: [
                    { text: 'IdZona', flex: 1, dataIndex: 'IdZona' }
                ]

            });

            var storePosicionesRuta = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['Patente',
                    'IdTipoMovil',
                    'NombreTipoMovil',
                    { name: 'Fecha', type: 'date', dateFormat: 'c' },
                    'Latitud',
                    'Longitud',
                    'Velocidad',
                    'Direccion',
                    'Ignicion',
                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPosicionesRuta',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPosicionesRuta = Ext.create('Ext.grid.Panel', {
                id: 'gridPosicionesRuta',
                store: storePosicionesRuta,
                columns: [
                    { text: 'Patente', dataIndex: 'Patente', hidden: true },
                    { text: 'IdTipoMovil', dataIndex: 'IdTipoMovil', hidden: true },
                    { text: 'NombreTipoMovil', dataIndex: 'NombreTipoMovil', hidden: true },
                    { text: 'Fecha', dataIndex: 'Fecha', hidden: true, renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    { text: 'Latitud', dataIndex: 'Latitud', hidden: true },
                    { text: 'Longitud', dataIndex: 'Longitud', hidden: true },
                    { text: 'Velocidad', dataIndex: 'Velocidad', hidden: true },
                    { text: 'Direccion', dataIndex: 'Direccion', hidden: true },
                    { text: 'Ignicion', dataIndex: 'Ignicion', hidden: true }
                ]
            });

            var storeAlertasRuta = new Ext.data.JsonStore({
                autoLoad: false,
                fields: [{ name: 'FechaInicioAlerta', type: 'date', dateFormat: 'c' },
                { name: 'FechaHoraCreacion', type: 'date', dateFormat: 'c' },
                    'PatenteTracto',
                    'TextFechaCreacion',
                    'PatenteTrailer',
                    'Velocidad',
                    'Latitud',
                    'Longitud',
                    'DescripcionAlerta',
                    'Ocurrencia'
                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxAlertas.aspx?Metodo=GetAlertasRuta',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPanelAlertasRuta = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelAlertasRuta',
                title: 'Alertas',
                //hideCollapseTool: true,
                store: storeAlertasRuta,
                anchor: '100% 30%',
                columnLines: true,
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                    { text: 'Fecha Inicio', sortable: true, width: 110, dataIndex: 'FechaInicioAlerta', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    { text: 'Fecha Envío', sortable: true, width: 110, dataIndex: 'FechaHoraCreacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    { text: 'Descripción', sortable: true, flex: 1, dataIndex: 'DescripcionAlerta' }
                ],
                listeners: {
                    select: function (sm, row, rec) {

                        var date = Ext.getCmp('gridPanelAlertasRuta').getStore().data.items[rec].raw.FechaHoraCreacion.toString();

                        for (var i = 0; i < markers.length; i++) {
                            if (markers[i].labelText == date) {
                                markers[i].setAnimation(google.maps.Animation.BOUNCE);
                                setTimeout('markers[' + i + '].setAnimation(null);', 800);

                                var contentString =

                                    '<br>' +
                                    '<table>' +
                                    '<tr>' +
                                    '       <td><b>Fecha</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '       <td>' + row.data.TextFechaCreacion + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '        <td><b>Velocidad:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + row.data.Velocidad + ' Km/h </td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '        <td><b>Latitud:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + row.data.Latitud + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '        <td><b>Longitud:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + row.data.Longitud + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '        <td><b>Descripción:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + row.data.DescripcionAlerta + '</td>' +
                                    '</tr>' +

                                    obtenerIframeVistaCalles(row.data.Latitud,row.data.Longitud)
                                    +
                                    
                                    '</table>' +
                                    '<br>';
                                infowindow.setContent(contentString);
                                infowindow.open(map, markers[i]);

                                Ext.getCmp("gridPanelAlertasRuta").getSelectionModel().deselectAll();

                                break;
                            }
                        }

                        map.setCenter(new google.maps.LatLng(row.data.Latitud, row.data.Longitud));
                        //map.setZoom(16);

                    }
                }
            });

            var viewWidth = Ext.getBody().getViewSize().width;
            var viewHeight = Ext.getBody().getViewSize().height;

            var textDistancia = new Ext.form.TextField({
                id: 'textDistancia',
                fieldLabel: 'Distancia',
                labelWidth: 60,
                anchor: '99%',
                readOnly: true
            });

            var textTiempo = new Ext.form.TextField({
                id: 'textTiempo',
                fieldLabel: 'Tiempo',
                labelWidth: 60,
                anchor: '99%',
                readOnly: true
            });

            var textETA = new Ext.form.TextField({
                id: 'textETA',
                fieldLabel: 'ETA',
                labelWidth: 60,
                anchor: '99%',
                readOnly: true
            });

            var storeGraphTemperatura = new Ext.data.JsonStore({
                //autoLoad: false,
                fields: ['Fecha',
                    'FechaText',
                    'Temperatura1',
                    'Temperatura2',
                    'LineaCentral',
                    'ZonaAlta',
                    'ZonaBaja'

                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetRpt_Temperatura',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                }),
                data: [{ FechaText: '00:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '01:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '02:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '03:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '04:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '05:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '06:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '07:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '08:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '09:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '10:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '11:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                { FechaText: '12:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 }]
            });

            var graphTemperatura = new Ext.chart.Chart({
                id: 'graphTemperatura',
                anchor: '100% 100%',
                animate: true,
                shadow: true,
                store: storeGraphTemperatura,
                legend: {
                    position: 'right'
                },
                /*style: {
                    marginLeft: '30px'
                },*/
                axes: [{
                    type: 'Numeric',
                    minimum: -25,
                    maximum: 25,
                    position: 'left',
                    fields: ['Temperatura1'],
                    title: true,
                    grid: true,
                    label: {
                        renderer: Ext.util.Format.numberRenderer('0,0'),
                        font: '9px Arial',

                    },
                    roundToDecimal: false
                }, {
                    type: 'Category',
                    position: 'bottom',
                    fields: ['FechaText'],
                    //title: ['Detención', 'Pérdida señal'],
                    grid: true
                    /*label: {
                        font: '9px Arial',
                        renderer: function (name) {
                            return name.substr(0, 3);
                        }
                    }*/
                }],
                series: [{
                    type: 'line',
                    axis: 'left',
                    //smooth: true,
                    highlight: false,
                    xField: 'FechaText',
                    yField: ['Temperatura1'],
                    markerConfig: {
                        type: 'circle',
                        size: 3,
                        radius: 3,
                        'stroke-width': 0
                    },
                    tips: {
                        trackMouse: true,
                        width: 140,
                        height: 45,
                        renderer: function (storeItem, item) {
                            this.setTitle('Fecha: ' + storeItem.get('Fecha') + ' Temperatura: ' + storeItem.get('Temperatura1'));
                        }
                    },
                }, /*{
            type: 'line',
            axis: 'left',
            //smooth: true,
            highlight: false,
            xField: 'FechaText',
            yField: ['Temperatura2'],
            markerConfig: {
                type: 'circle',
                size: 3,
                radius: 3,
                'stroke-width': 0
            },
            tips: {
                trackMouse: true,
                width: 140,
                height: 45,
                renderer: function (storeItem, item) {
                    this.setTitle('Fecha: ' + storeItem.get('Fecha') + ' Temp. ' + storeItem.get('Temperatura2'));
                }
            },
        },*/ {
                    type: 'line',
                    axis: 'left',
                    highlight: false,
                    xField: 'FechaText',
                    yField: ['LineaCentral'],
                    markerConfig: {
                        type: 'circle',
                        size: 0,
                        radius: 0,
                        color: 'red',
                        'stroke-width': 0
                    },
                    style: {
                        fill: "#ef4038",
                        stroke: "#ef4038"
                    },
                }, {
                    type: 'line',
                    axis: 'left',
                    highlight: false,
                    xField: 'FechaText',
                    yField: ['ZonaAlta'],
                    markerConfig: {
                        type: 'circle',
                        size: 0,
                        radius: 0,
                        'stroke-width': 0
                    },
                    style: {
                        fill: "#f3cede",
                        stroke: "#f3cede",
                        'stroke-width': 3
                    },
                }, {
                    type: 'line',
                    axis: 'left',
                    highlight: false,
                    xField: 'FechaText',
                    yField: ['ZonaBaja'],
                    markerConfig: {
                        type: 'circle',
                        size: 0,
                        radius: 0,
                        'stroke-width': 0
                    },
                    style: {
                        fill: "#38e7ef",
                        stroke: "#38e7ef",
                        'stroke-width': 3
                    },
                }
                ]
            });

            var panGraphTemperatura = new Ext.FormPanel({
                id: 'panGraphTemperatura',
                //anchor: '100% 60%',
                height: 435,
                //width: 800,
                //title: 'Gráfico de temperatura',
                //renderTo: Ext.getBody(),
                layout: 'fit',
                items: [graphTemperatura]
            });

            var btnSalirGraphTempreatura = {
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/back_black_16x16.png',
                text: 'Salir',
                width: 70,
                height: 24,
                handler: function (a, b, c, d, e) {
                    winGraphTemperatura.close();
                    var store = Ext.getCmp('graphTemperatura').store;
                    store.removeAll();
                }
            };

            var winGraphTemperatura = new Ext.Window({
                id: 'winGraphTemperatura',
                title: 'Reporte de Temperatura',
                //height: viewHeight / 1.2,
                //width: viewWidth / 1.25,
                height: 500,
                width: 1000,
                modal: true,
                resizable: false,
                border: true,
                draggable: true,
                closeAction: 'hide',
                maximizable: false,
                items: [panGraphTemperatura],
                buttons: [btnSalirGraphTempreatura]
            });

            var winDistanciaTiempo = new Ext.Window({
                id: 'winDistanciaTiempo',
                title: 'Distancia / Tiempo hasta Local',
                width: 210,
                height: 125,
                closable: true,
                closeAction: 'hide',
                modal: false,
                initCenter: false,
                x: viewWidth - 220,
                y: 335,
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    style: 'padding-top:3px;padding-left:5px;',
                    items: [textDistancia]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    style: 'padding-left:5px;',
                    items: [textTiempo]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    style: 'padding-left:5px;',
                    items: [textETA]
                }
                ],
                resizable: false,
                border: true,
                draggable: false
            });

            var leftPanel = new Ext.FormPanel({
                id: 'leftPanel',
                region: 'west',
                margins: '0 0 3 3',
                border: true,
                width: 620,
                minWidth: 400,
                maxWidth: viewWidth / 1.5,
                layout: 'anchor',
                split: true,
                collapsible: true,
                hideCollapseTool: true,
                items: [gridPanelViajesRuta, gridPanelAlertasRuta]
            });

            leftPanel.on('collapse', function () {
                google.maps.event.trigger(map, "resize");
            });

            leftPanel.on('expand', function () {
                google.maps.event.trigger(map, "resize");
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                region: 'center',
                border: true,
                margins: '0 3 3 0',
                anchor: '100% 100%',
                contentEl: 'dvMap'
            });



            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, leftPanel, centerPanel]
            });

            viewport.on('resize', function () {
                google.maps.event.trigger(map, "resize");
                Ext.getCmp('winDistanciaTiempo').setPosition(Ext.getBody().getViewSize().width - 220, 50, true)

            });

        });

    </script>

    <script type="text/javascript">

        var zoneLabels = new Array();

        Ext.onReady(function () {
            GeneraMapa("dvMap", true);
        });

        function FiltrarViajes() {

            var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getValue();

            console.log("nroTransporte: " + nroTransporte);

            if (Ext.getCmp("chkNroTransporte").getValue() == true && nroTransporte == "") {
                return;
            }

            ClearMap();
            arrayPositions.splice(0, arrayPositions.length);
            arrayAlerts.splice(0, arrayAlerts.length);
            eraseHouses();

            for (var i = 0; i < geoLayer.length; i++) {
                geoLayer[i].layer.setMap(null);
                geoLayer[i].label.setMap(null);
                geoLayer.splice(i, 1);
            }

            Ext.getCmp('winDistanciaTiempo').hide();

            Ext.getCmp("gridPanelViajesRuta").getStore().removeAll();
            Ext.getCmp("gridPanelAlertasRuta").getStore().removeAll()

            var desde = Ext.getCmp('dateDesde').getValue();
            var hasta = Ext.getCmp('dateHasta').getValue();

            var patente = Ext.getCmp('comboFiltroPatente').getValue();
            var estadoViaje = Ext.getCmp('comboFiltroEstadoViaje').getValue();
            var local = Ext.getCmp('comboFiltroLocales').getValue();

            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();
            var tipoViaje = Ext.getCmp('comboFiltroTipoViaje').getValue();

            switch (estadoViaje) {
                case "Finalizado":
                    estadoViaje = "EnLocal-P";
                    break;
                case "En Local":
                    estadoViaje = "EnLocal-R";
                    break;
                case "En Ruta":
                    estadoViaje = "RUTA";
                    break;
                case "Asignado":
                    estadoViaje = "ASIGNADO";
                    break;
                case "Cerrado por Sistema":
                    estadoViaje = "Cerrado por Sistema";
                    break;
                case "Todos":
                    estadoViaje = "Todos";
                    break;
                default:
                    estadoViaje = "Todos";
            }

            var store = Ext.getCmp('gridPanelViajesRuta').store;
            store.load({
                params: {
                    desde: desde,
                    hasta: hasta,
                    nroTransporte: nroTransporte,
                    patente: patente,
                    estadoViaje: estadoViaje,
                    transportista: transportista,
                    codLocal: local,
                    tipoViaje: tipoViaje
                }
            });
        }

        function GetAlertasRuta(nroTransporte, idEmbarque, destino, estadoViaje) {

            var store = Ext.getCmp('gridPanelAlertasRuta').store;
            store.load({
                params: {
                    nroTransporte: nroTransporte,
                    idEmbarque: idEmbarque,
                    destino: destino,
                    estadoViaje: estadoViaje
                },
                callback: function (r, options, success) {
                    if (success) {
                        MuestraAlertasViaje();
                    }
                }
            });
        }

        function GetPosiciones(origen, destino, patenteTracto, patenteTrailer, fechaHoraCreacion, fechaHoraSalidaOrigen, fechaHoraLlegadaDestino, fechaHoraCierreSistema, nroTransporte, idEmbarque, destino, estadoViaje, lineaTransporte, descOrigen, descDestino, estadoReporte, agrupacion) {

            Ext.getCmp('gridPosicionesRuta').store.removeAll();

            var store = Ext.getCmp('gridPosicionesRuta').store;
            var storeZone = Ext.getCmp('gridZonasToDraw').store;

            var fec;

            if (estadoViaje == 'Finalizado') {
                fec = fechaHoraLlegadaDestino;
            }
            if (estadoViaje == 'Cerrado por Sistema') {
                fec = fechaHoraCierreSistema;
            }
            if (fec == null) {
                fec = new Date();
            }

            store.load({
                params: {
                    patenteTracto: patenteTracto,
                    patenteTrailer: patenteTrailer,
                    fechaHoraCreacion: fechaHoraCreacion,
                    fechaHoraSalidaOrigen: fechaHoraSalidaOrigen,
                    fechaHoraLlegadaDestino: fec,
                    nroTransporte: nroTransporte,
                    idEmbarque: idEmbarque,
                    destino: destino,
                    estadoViaje: estadoViaje
                },
                callback: function (r, options, success) {
                    if (success) {

                        storeZone.load({
                            params: {
                                fechaDesde: fechaHoraCreacion,
                                fechaHasta: fec,
                                patente1: patenteTrailer,
                                patente2: patenteTracto
                            },
                            callback: function (r, options, success) {
                                if (success) {

                                    MuestraRutaViaje(patenteTracto, lineaTransporte, descOrigen, descDestino, estadoReporte, agrupacion, nroTransporte);

                                    var store = Ext.getCmp('gridZonasToDraw').getStore();
                                    for (var i = 0; i < store.count(); i++) {
                                        DrawZone(store.getAt(i).data.IdZona);
                                    }

                                    DrawZone(origen);
                                    var storeViajes = Ext.getCmp('gridPanelViajesRuta').store;

                                    for (var i = 0; i < storeViajes.count(); i++) {
                                        if (storeViajes.getAt(i).data.NroTransporte == nroTransporte) {
                                            DrawZone(storeViajes.getAt(i).data.CodigoDestino);
                                            DrawHouse(storeViajes.getAt(i).data.CodigoDestino, storeViajes.getAt(i).data.DestinoLat, storeViajes.getAt(i).data.DestinoLon, storeViajes.getAt(i).data.EstadoViaje);
                                        }
                                    }

                                }
                            }

                        });

                    }
                }
            });

        }

        function MuestraRutaViaje(placa, lineaTransporte, descOrigen, descDestino, estadoReporte, agrupacion, nroTransporte) {

            var store = Ext.getCmp('gridPosicionesRuta').getStore();
            var rowCount = store.count();
            var iterRow = 0;

            while (iterRow < rowCount) {

                var dir = parseInt(store.data.items[iterRow].raw.Direccion);

                var lat = store.data.items[iterRow].raw.Latitud;
                var lon = store.data.items[iterRow].raw.Longitud;

                var Latlng = new google.maps.LatLng(lat, lon);

                arrayPositions.push({
                    Fecha: store.data.items[iterRow].raw.Fecha.toString(),
                    Velocidad: store.data.items[iterRow].raw.Velocidad,
                    Latitud: lat,
                    Longitud: lon,
                    LatLng: Latlng
                });

                if (store.data.items[iterRow].raw.Velocidad > 0) {
                    switch (true) {
                        case (store.data.items[iterRow].raw.Velocidad < 80):
                            var marker = new MarkerWithLabel({
                                position: Latlng,
                                map: map,
                                labelContent: store.data.items[iterRow].raw.Velocidad,
                                labelAnchor: new google.maps.Point(15, 19),
                                labelClass: "labelsBlack", // the CSS class for the label
                                labelInBackground: false,
                                labelText: store.data.items[iterRow].raw.Fecha.toString(),
                                icon: 'Images/rectangle_green_25x25.png'
                            });
                            break;
                        case ((store.data.items[iterRow].raw.Velocidad >= 80) && (store.data.items[iterRow].raw.Velocidad < 95)):
                            var marker = new MarkerWithLabel({
                                position: Latlng,
                                map: map,
                                labelContent: store.data.items[iterRow].raw.Velocidad,
                                labelAnchor: new google.maps.Point(15, 19),
                                labelClass: "labelsBlack", // the CSS class for the label
                                labelInBackground: false,
                                labelText: store.data.items[iterRow].raw.Fecha.toString(),
                                icon: 'Images/rectangle_yellow_25x25.png'
                            });
                            break;
                        case (store.data.items[iterRow].raw.Velocidad >= 95):
                            var marker = new MarkerWithLabel({
                                position: Latlng,
                                map: map,
                                labelContent: store.data.items[iterRow].raw.Velocidad,
                                labelAnchor: new google.maps.Point(15, 19),
                                labelClass: "labelsWhite", // the CSS class for the label
                                labelInBackground: false,
                                labelText: store.data.items[iterRow].raw.Fecha.toString(),
                                icon: 'Images/rectangle_red_25x25.png'
                            });
                            break;
                    }
                }
                else {
                    marker = new google.maps.Marker({
                        position: Latlng,
                        icon: 'Images/dot_red_16x16.png',
                        map: map,
                        labelText: store.data.items[iterRow].raw.Fecha.toString()
                    });
                }

                var label = new Label({
                    map: null
                });
                label.bindTo('position', marker, 'position');
                label.bindTo('text', marker, 'labelText');

                google.maps.event.addListener(marker, 'click', function () {
                    var latLng = this.position;
                    var fec = this.labelText;

                    for (i = 0; i < arrayPositions.length; i++) {
                        if (arrayPositions[i].Fecha.toString() == fec.toString() & arrayPositions[i].LatLng.toString() == latLng.toString()) {

                            var Lat = arrayPositions[i].Latitud;
                            var Lon = arrayPositions[i].Longitud;

                            //Estado reporte
                            if (estadoReporte === 0)
                                estadoReporte = "<p style='color:red;'>Sin Reporte</p>";
                            else if (arrayPositions[i].EstadoReporte === 1)
                                estadoReporte = "<p style='color:green;'>En Tiempo</p>";
                            else if (arrayPositions[i].EstadoReporte === 2)
                                estadoReporte = "<p style='color:yellow;'>Atraso</p>";

                            //Estado motor
                            var estadoMotor;
                            if (arrayPositions[i].Velocidad > 0)
                                estadoMotor = "En Movimiento";
                            else
                                estadoMotor = "Detenido";

                            var contentString =

                                '<br>' +
                                '<table>' +
                                '<tr>' +
                                '       <td><b>Fecha</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '       <td>' + (arrayPositions[i].Fecha.toString()).replace("T", " ") + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>ID Master:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + nroTransporte + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Línea Transporte:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + lineaTransporte + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Placa:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + placa + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Velocidad:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + arrayPositions[i].Velocidad + ' Km/h </td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Latitud:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + arrayPositions[i].Latitud + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Longitud:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + arrayPositions[i].Longitud + '</td>' +
                                '</tr>' +


                                '<tr>' +
                                '        <td><b>Origen:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + descOrigen + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Destino:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + descDestino + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Estado Reporte:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + estadoReporte + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Estado Motor:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + estadoMotor + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Agrupación:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + agrupacion + '</td>' +
                                '</tr>' +

                                obtenerIframeVistaCalles(arrayPositions[i].Latitud,arrayPositions[i].Longitud)
                                    +
                                
                                '</table>' +
                                '<br>';

                            infowindow.setContent(contentString);
                            infowindow.open(map, this);

                            break;
                        }
                    }

                });

                markers.push(marker);
                labels.push(label);

                iterRow++;
            }

            if (rowCount > 0) {
                var len = markers.length - 1
                map.setCenter(markers[len].position);
                markers[len].setAnimation(google.maps.Animation.BOUNCE);
                setTimeout('markers[' + len + '].setAnimation(null);', 800);
            }

        }

        function MuestraAlertasViaje() {

            var store = Ext.getCmp('gridPanelAlertasRuta').getStore();
            var rowCount = store.count();
            var iterRow = 0;

            while (iterRow < rowCount) {
                var descrip = store.data.items[iterRow].raw.DescripcionAlerta;

                var lat = store.data.items[iterRow].raw.Latitud;
                var lon = store.data.items[iterRow].raw.Longitud;

                var Latlng = new google.maps.LatLng(lat, lon);

                arrayAlerts.push({
                    Fecha: store.data.items[iterRow].raw.FechaHoraCreacion.toString(),
                    TextFechaCreacion: store.data.items[iterRow].raw.TextFechaCreacion,
                    Velocidad: store.data.items[iterRow].raw.Velocidad,
                    Latitud: lat,
                    Longitud: lon,
                    LatLng: Latlng,
                    Descripcion: store.data.items[iterRow].raw.DescripcionAlerta
                });

                switch (true) {
                    case (descrip == 'CRUCE GEOCERCA PARA INGRESAR A TIENDA'):
                        marker = new google.maps.Marker({
                            //idBusqueda: busquedaAud,
                            position: Latlng,
                            icon: 'Images/finishflag_24x24.png',
                            map: map,
                            labelText: store.data.items[iterRow].raw.FechaHoraCreacion.toString()
                            //labelText: htmlString2,
                            //infoWinMark: htmlNew
                        });
                        break;
                    default:
                        marker = new google.maps.Marker({
                            //idBusqueda: busquedaAud,
                            position: Latlng,
                            icon: 'Images/alert_orange_22x22.png',
                            map: map,
                            labelText: store.data.items[iterRow].raw.FechaHoraCreacion.toString()
                            //labelText: htmlString2,
                            //infoWinMark: htmlNew
                        });
                        break;
                }

                var label = new Label({
                    //idBusqueda: busquedaAud,
                    map: null
                });
                label.bindTo('position', marker, 'position');
                label.bindTo('text', marker, 'labelText');

                google.maps.event.addListener(marker, 'click', function () {

                    var latLng = this.position;
                    var fec = this.labelText;

                    for (i = 0; i < arrayAlerts.length; i++) {
                        if (arrayAlerts[i].Fecha.toString() == fec.toString() & arrayAlerts[i].LatLng.toString() == latLng.toString()) {

                            var contentString =

                                '<br>' +
                                '<table>' +
                                '<tr>' +
                                '       <td><b>Fecha</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '       <td>' + arrayAlerts[i].TextFechaCreacion + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Velocidad:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + arrayAlerts[i].Velocidad + ' Km/h </td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Latitud:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + arrayAlerts[i].Latitud + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Longitud:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + arrayAlerts[i].Longitud + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '        <td><b>Descripción:</b></td>' +
                                '       <td><pre>     </pre></td>' +
                                '        <td>' + arrayAlerts[i].Descripcion + '</td>' +
                                '</tr>' +
                                obtenerIframeVistaCalles(arrayAlerts[i].Latitud,arrayAlerts[i].Longitud)
                                    +
                                
                                '</table>' +
                                '<br>';

                            infowindow.setContent(contentString);
                            infowindow.open(map, this);
                            //GetDivInnerHtml();
                            break;

                        }
                    }

                });


                markers.push(marker);
                labels.push(label);

                iterRow++;
            }

        }


        function GetDivInnerHtml() {
            $.get("https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=19.309238039454,-99.3427772001981&heading=-65&pitch=5&fov=80", function (data) {   //OR www.google.com
                $(".divMain").html(data);
            });
        }


        function DrawZone(idZona) {

            for (var i = 0; i < geoLayer.length; i++) {
                geoLayer[i].layer.setMap(null);
                geoLayer[i].label.setMap(null);
                geoLayer.splice(i, 1);
            }

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
                params: {
                    IdZona: idZona
                },
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        if (data.Vertices.length > 1) { //Polygon
                            var polygonGrid = new Object();
                            polygonGrid.IdZona = data.IdZona;

                            var arr = new Array();
                            for (var i = 0; i < data.Vertices.length; i++) {
                                arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
                            }

                            if (data.idTipoZona == 3) {
                                var colorZone = "#FF0000";
                            }
                            else {
                                var colorZone = "#7f7fff";
                            }

                            polygonGrid.layer = new google.maps.Polygon({
                                paths: arr,
                                strokeColor: "#000000",
                                strokeWeight: 1,
                                strokeOpacity: 0.9,
                                fillColor: colorZone,
                                fillOpacity: 0.3,
                                labelText: data.NombreZona
                            });

                            var viewLabel = Ext.getCmp('chkMostrarLabels').getValue();
                            polygonGrid.label = new Label({
                                text: idZona,
                                position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                map: viewLabel ? map : null
                            });
                            polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
                            polygonGrid.layer.setMap(map);
                            geoLayer.push(polygonGrid);

                            if (containsLabel(zoneLabels, idZona) == false) {
                                zoneLabels.push(polygonGrid.label);
                            }
                        }
                        else
                            if (data.Vertices.length = 1) { //Point
                                var Point = new Object();
                                Point.IdZona = data.IdZona;

                                var image = new google.maps.MarkerImage("Images/greymarker_32x32.png");

                                Point.layer = new google.maps.Marker({
                                    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                    icon: image,
                                    labelText: data.NombreZona,
                                    map: map
                                });

                                Point.label = new Label({
                                    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                    map: map
                                });

                                Point.label.bindTo('text', Point.layer, 'labelText');
                                Point.layer.setMap(map);
                                geoLayer.push(Point);
                            }

                    }
                },
                failure: function (msg) {
                    alert('Se ha producido un error. 3');
                }
            });
        }

        function containsZone(a, obj) {
            var i = a.length;
            while (i--) {
                if (a[i].IdZona === obj) {
                    return true;
                }
            }
            return false;
        }

        function containsLabel(a, obj) {
            var i = a.length;
            while (i--) {
                //GetDetalleAreaSeleccionada
                if (a[i].text === obj) {
                    return true;
                }
            }
            return false;
        }

        function eraseAllZones() {
            var countZonas = Ext.getCmp('comboZonas').store.count()

            for (i = 0; i < countZonas; i++) {
                var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
                EraseZone(idZona);
            }

            zoneLabels = [];
        }

        function EraseZone(idZona) {
            for (var i = 0; i < geoLayer.length; i++) {
                if (idZona == geoLayer[i].IdZona) {
                    geoLayer[i].layer.setMap(null);
                    geoLayer[i].label.setMap(null);
                    geoLayer.splice(i, 1);
                }
            }

            for (var i = 0; i < zoneLabels.length; i++) {
                if (zoneLabels[i].text == idZona) {
                    zoneLabels[i].setMap(null);
                    zoneLabels.splice(i, 1);

                }
            }

        }

        function CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon) {

            var service = new google.maps.DistanceMatrixService();
            var origen = new google.maps.LatLng(estadoLat, estadoLon);
            var destino = new google.maps.LatLng(destinoLat, destinoLon);

            service.getDistanceMatrix(
                {
                    origins: [origen],
                    destinations: [destino],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                }, callback);
        }

        function callback(response, status) {
            if (status == google.maps.DistanceMatrixStatus.OK) {

                var distance = response.rows[0].elements[0].distance.text;
                var time = response.rows[0].elements[0].duration.text;
                var duration = response.rows[0].elements[0].duration.value;

                var eta = new Date();
                eta.setSeconds(eta.getSeconds() + duration);

                var textETA = + eta.getFullYear() + "-" + (eta.getMonth() + 1) + "-" + eta.getDate() + " " + eta.getHours() + ":" + eta.getMinutes();

                Ext.getCmp('winDistanciaTiempo').show();

                Ext.getCmp('textDistancia').setValue(distance);
                Ext.getCmp('textTiempo').setValue(time);
                Ext.getCmp('textETA').setValue(textETA);

                Ext.getCmp('winDistanciaTiempo').setPosition(Ext.getBody().getViewSize().width - 220, 50, true)
            }
        }

        function FiltrarPatentes() {
            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

            var store = Ext.getCmp('comboFiltroPatente').store;
            store.load({
                params: {
                    transportista: transportista
                }
            });
        }

        var renderEstadoViaje = function (value, meta) {
            {
                if (value == 'Cerrado por Sistema') {
                    meta.tdCls = 'red-cell';
                    return value;
                }
                if (value == 'Finalizado') {
                    meta.tdCls = 'blue-cell';
                    return value;
                }
                else {
                    meta.tdCls = 'black-cell';
                    return value;
                }
            }
        };

        var renderCantidadAlertas = function (value, meta) {
            {
                if (value >= 1) {
                    meta.tdCls = 'red-cell';
                    return value;
                }
                else {
                    return value;
                }
            }
        };

        function MostrarZonas() {

            Ext.Msg.wait('Espere por favor...', 'Generando zonas');

            var countZonas = Ext.getCmp('comboZonas').store.count()

            for (i = 0; i < countZonas; i++) {
                var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
                var idTipoZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdTipoZona;

                DrawZone(idZona, idTipoZona);
            }

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
                success: function (response, opts) {

                    var task = new Ext.util.DelayedTask(function () {
                        Ext.Msg.hide();
                    });

                    task.delay(1500);

                },
                failure: function (response, opts) {
                    Ext.Msg.hide();
                }
            });
        }

        function DrawHouse(idZona, latitud, longitud, estadoViaje) {

            var image = new google.maps.MarkerImage("Images/house_blue_24x24.png");

            if (estadoViaje == "Finalizado") {
                image = new google.maps.MarkerImage("Images/house_green_24x24.png");
            }
            if (estadoViaje == "Cerrado por Sistema") {
                image = new google.maps.MarkerImage("Images/house_red_24x24.png");
            }

            var point = new google.maps.LatLng(latitud, longitud);

            var markerHouse = new google.maps.Marker({
                position: point,
                icon: image,

                map: map
            });

            arrayHouses.push(markerHouse);
        }

        function eraseHouses() {

            for (i = 0; i < arrayHouses.length; i++) {
                arrayHouses[i].setMap(null);
            }
            arrayHouses = [];
        }

        function ShowReporteTemperatura(row) {

            var myMask = new Ext.LoadMask(Ext.getBody(), { msg: "Cargando..." });
            myMask.show();

            var nroTransporte = row.data.NroTransporte;

            var store = Ext.getCmp('graphTemperatura').store;
            store.removeAll();

            store.load({
                params: {
                    nroTransporte: nroTransporte,
                    desde: '',
                    hasta: '',
                    transportista: '',
                    patente: ''
                },
                callback: function (r, options, success) {

                    if (Ext.getCmp('graphTemperatura').store.count() == 0) {
                        Ext.getCmp('graphTemperatura').surface.removeAll();

                        store.add({ FechaText: '00:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '01:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '02:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '03:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '04:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '05:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '06:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '07:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '08:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '09:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '10:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '11:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                            { FechaText: '12:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 });
                    }

                    myMask.hide();
                }

            });


        }
        /*
        function obtenerIframeVistaCalles(latitud, longitud) {
            var vistaCalles =
                '<tr>' +

                '        <td colspan="3" > <iframe frameborder="0" style="border:0" src="http://localhost:54841/pruebamapa.aspx?latitud=' + latitud + '&longitud=' + longitud + '"  height="200" width="300"></iframe> </td>' +

                '</tr>';
            return vistaCalles;
        }
        */
    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">




    <div id="dvMap"></div>
</asp:Content>

