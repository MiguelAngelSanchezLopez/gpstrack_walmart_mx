﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PruebaCargadorArchivos.aspx.cs" Inherits="Track_Web.PruebaCargadorArchivos" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Force latest IE rendering engine or ChromeFrame if installed -->
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script src="Scripts/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="Scripts/Plugins/js/jquery-ui.js"></script>
        <script src="Scripts/Plugins/subidorArchivos/js/jquery.iframe-transport.js"></script>
        <script src="Scripts/Plugins/subidorArchivos/js/jquery.fileupload.js"></script>
        <script type="text/javascript" src="Scripts/Plugins/jquery.cookie.js"></script>
        <script type="text/javascript" src="Scripts/Plugins/js/sweetalert2.all.js"></script>

        <script src="Scripts/Plugins/subidorArchivos/upload.js" type="text/javascript"></script>
        <!-- jQuery UI styles -->
        <!--
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/dark-hive/jquery-ui.css" id="theme">
        -->

        <link href="Styles/Site.css" rel="stylesheet" type="text/css" />
        <!--
        <link href="Scripts/ext-4.0.1/resources/css/ext-all-gray.css" rel="stylesheet" type="text/css" />
        <link href="Scripts/Plugins/css/jquery.dataTables.css" rel="stylesheet" />
        <link rel="stylesheet" href="Scripts/Plugins/css/font-awesome.min.css">
        <link rel="stylesheet" href="Scripts/Plugins/css/font-awesome.min.css">

        <link href="Scripts/Plugins/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="Scripts/Plugins/css/jquery-ui.css" />
        <link rel="stylesheet" href="Scripts/Plugins/subidorArchivos/css/jquery.fileupload.css">
        <link rel="stylesheet" href="Scripts/Plugins/subidorArchivos/css/jquery.fileupload-ui.css">
        -->
        <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/jquery-ui-demo-ie8.css">
        <![endif]-->
        <!-- blueimp Gallery styles -->
        <link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->

    </head>

    <body>


        <table align="center" >
            <tr>
                <td>
                    <input type="file" name="file" id="btnFileUpload" />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="progressbar" style="width: 100px; display: none;">
                        <div id="mensajeTexto">
                            Cargando Archivo ...
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="contenedorListaImagenes" style="max-height: 320px; overflow-y: scroll; width:500px; "></div>
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <a id='AsignarImgsTableroActual' href='#'>Agregar imagenes al tablero</a>
                    </center>
                </td>
            </tr>
        </table>
    </body>
</html>
