﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UltimaPosicion.aspx.cs" Inherits="Track_Web.UltimaPosicion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
  <script src="Scripts/RowExpander.js" type="text/javascript"></script>
  
<script type="text/javascript">

  var geoLayer = new Array();
  var arrayPositions = new Array();
  var arrayAlerts = new Array();
  var trafficLayer = new google.maps.TrafficLayer();
  var infowindow = new google.maps.InfoWindow();
  var idAlerta;

  Ext.onReady(function () {

      Ext.QuickTips.init();
      Ext.Ajax.timeout = 3600000;
      Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
      Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
      Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

      //Verifica si se debe controlar tiempo de expiración de sesión
      Ext.Ajax.request({
          url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
          success: function (data, success) {
              if (data != null) {
                  data = Ext.decode(data.responseText);

                  if (data > 0) {
                      Ext.ns('App');

                      //Session timeout in secons     
                      App.SESSION_TIMEOUT = data;

                      // Helper that converts minutes to milliseconds.
                      App.toMilliseconds = function (minutes) {
                          return minutes * 60 * 1000;
                      }

                      // Notifies user that her session has timed out.
                      App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                          Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                          Ext.MessageBox.show({
                              title: "Sesión expirada.",
                              msg: "Su sesión ha expirado.",
                              icon: Ext.MessageBox.WARNING,
                              buttons: Ext.MessageBox.OK,
                              fn: function () {
                                  window.location = "Login.aspx";
                              }
                          });

                      });

                      // Starts the session timeout workflow after an AJAX request completes.
                      Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                          // Reset the client-side session timeout timers.
                          App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                      });

                  }
              }
          }
      })

    var storePosicionesRuta = new Ext.data.JsonStore({
      autoLoad: false,
      fields: [ { name: 'Fecha', type: 'date', dateFormat: 'c' },
                'Patente',
                'Transportista',
                'Proveedor',
                'Latitud',
                'Longitud',
                'Direccion',
                'Velocidad',
                'Ignicion'],
      proxy: new Ext.data.HttpProxy({
        url: 'AjaxPages/AjaxAlertas.aspx?Metodo=GetUltimaPosicionModuloMapa',
        reader: { type: 'json', root: 'Zonas' },
        headers: {
          'Content-type': 'application/json'
        }
      })
    });

    var gridPosicionesRuta = Ext.create('Ext.grid.Panel', {
      id: 'gridPosicionesRuta',
      store: storePosicionesRuta,
      columns: [
                    { text: 'Fecha', dataIndex: 'Fecha', hidden: true, renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    { text: 'Patente', dataIndex: 'Patente', hidden: true },
                    { text: 'Transportista', dataIndex: 'Transportista', hidden: true },
                    { text: 'Proveedor', dataIndex: 'Proveedor', hidden: true },
                    { text: 'Latitud', dataIndex: 'Latitud', hidden: true },
                    { text: 'Longitud', dataIndex: 'Longitud', hidden: true },
                    { text: 'Direccion', dataIndex: 'Direccion', hidden: true },
                    { text: 'Velocidad', dataIndex: 'Velocidad', hidden: true },
                    { text: 'Ignicion', dataIndex: 'Ignicion', hidden: true }
             ]
    });

    idAlerta = location.search.split('ID=')[1]
    if (idAlerta == undefined) {
        idAlerta = 0;
    }

    GetPosiciones(idAlerta)

    var centerPanel = new Ext.FormPanel({
      id: 'centerPanel',
      region: 'center',
      border: true,
      margins: '0 3 3 0',
      anchor: '100% 100%',
      contentEl: 'dvMap'
    });

    var viewport = Ext.create('Ext.container.Viewport', {
      layout: 'border',
      items: [centerPanel]
    });

  }); 

</script>

<script type="text/javascript">

  Ext.onReady(function () {
    GeneraMapa("dvMap", true);
  });

  function GetPosiciones(idAlerta) {

    var store = Ext.getCmp('gridPosicionesRuta').store;

    store.load({
      params: {
        idAlerta: idAlerta
      },
      callback: function (r, options, success) {
        if (success) {

            MuestraRutaViaje();

        }
      }
    });

  }

  function MuestraRutaViaje() {

    var store = Ext.getCmp('gridPosicionesRuta').getStore();
    var rowCount = store.count();
    var iterRow = 0;

    while (iterRow < rowCount) {

      var dir = parseInt(store.data.items[iterRow].raw.Direccion);

      var lat = store.data.items[iterRow].raw.Latitud;
      var lon = store.data.items[iterRow].raw.Longitud;

      var Latlng = new google.maps.LatLng(lat, lon);

      arrayPositions.push(
        {
        Fecha: store.data.items[iterRow].raw.Fecha.toString(),
        Patente: store.data.items[iterRow].raw.Patente,
        Transportista: store.data.items[iterRow].raw.Transportista,
        Proveedor: store.data.items[iterRow].raw.Proveedor,
        Velocidad: store.data.items[iterRow].raw.Velocidad,
        Latitud: lat,
        Longitud: lon,
        LatLng: Latlng
      });

      if (store.data.items[iterRow].raw.Velocidad > 0) {

        switch (true) {
          case ((dir >= 338) || (dir < 22)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/1_arrowcircle_blue_N_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
          case ((dir >= 22) && (dir < 67)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/2_arrowcircle_blue_NE_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
          case ((dir >= 67) && (dir < 112)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/3_arrowcircle_blue_E_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
          case ((dir >= 112) && (dir < 157)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/4_arrowcircle_blue_SE_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
          case ((dir >= 157) && (dir < 202)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/5_arrowcircle_blue_S_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
          case ((dir >= 202) && (dir < 247)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/6_arrowcircle_blue_SW_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
          case ((dir >= 247) && (dir < 292)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/7_arrowcircle_blue_W_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
          case ((dir >= 292) && (dir < 338)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/8_arrowcircle_blue_NW_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
              //labelText: htmlString2,
              //infoWinMark: htmlNew
            });
            break;
        }
      }
      else {
        marker = new google.maps.Marker({
          position: Latlng,
          icon: 'Images/dot_red_16x16.png',
          map: map,
          labelText: store.data.items[iterRow].raw.Fecha.toString()

          //labelText: htmlString2,
          //infoWinMark: htmlNew
        });
      }

      var label = new Label({
        map: null
      });
      label.bindTo('position', marker, 'position');
      label.bindTo('text', marker, 'labelText');

      google.maps.event.addListener(marker, 'click', function () {
        var latLng = this.position;
        var fec = this.labelText;

        for (i = 0; i < arrayPositions.length; i++) {
          if (arrayPositions[i].Fecha.toString() == fec.toString() & arrayPositions[i].LatLng.toString() == latLng.toString()) {

            var Lat = arrayPositions[i].Latitud;
            var Lon = arrayPositions[i].Longitud;

            var contentString =

                  '<br>' +
                      '<table>' +
                        '<tr>' +
                            '       <td><b>Fecha</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '       <td>' + (arrayPositions[i].Fecha.toString()).replace("T", " ") + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Patente:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Patente + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Transportista:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Transportista + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Proveedor GPS:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Proveedor + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Velocidad:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Velocidad + ' Km/h </td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Latitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Latitud + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Longitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Longitud + '</td>' +
                        '</tr>' +

                      '</table>' +
                    '<br>';

            infowindow.setContent(contentString);
            infowindow.open(map, this);

            break;
          }
        }

      });

      markers.push(marker);
      labels.push(label);

      iterRow++;
    }

    if (rowCount > 0) {
      map.setCenter(markers[0].position);
    }

  }

</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
