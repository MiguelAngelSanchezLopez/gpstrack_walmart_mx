﻿<%@  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MonitoreoOnline.aspx.cs" Inherits="Track_Web.MonitoreoOnline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

  <script type="text/javascript">

    var geoLayer = new Array();
    var arrayPositions = new Array();
    var trafficLayer = new google.maps.TrafficLayer();
    var infowindow = new google.maps.InfoWindow();
    //var directionsService = new google.maps.DirectionsService;
    //var directionsDisplay = new google.maps.DirectionsRenderer;

    Ext.onReady(function () {

        Ext.QuickTips.init();
        Ext.Ajax.timeout = 3600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

        Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                               /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

        //Verifica si se debe controlar tiempo de expiración de sesión
        Ext.Ajax.request({
            url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
            success: function (data, success) {
                if (data != null) {
                    data = Ext.decode(data.responseText);

                    if (data > 0) {
                        Ext.ns('App');

                        //Session timeout in secons     
                        App.SESSION_TIMEOUT = data;

                        // Helper that converts minutes to milliseconds.
                        App.toMilliseconds = function (minutes) {
                            return minutes * 60 * 1000;
                        }

                        // Notifies user that her session has timed out.
                        App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                            Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                            Ext.MessageBox.show({
                                title: "Sesión expirada.",
                                msg: "Su sesión ha expirado.",
                                icon: Ext.MessageBox.WARNING,
                                buttons: Ext.MessageBox.OK,
                                fn: function () {
                                    window.location = "Login.aspx";
                                }
                            });

                        });

                        // Starts the session timeout workflow after an AJAX request completes.
                        Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                            // Reset the client-side session timeout timers.
                            App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                        });

                    }
                }
            }
        })

      var storeZonas = new Ext.data.JsonStore({
        id: 'storeZonas',
        autoLoad: true,
        fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboZonas = new Ext.form.field.ComboBox({
        id: 'comboZonas',
        store: storeZonas
      });

      var storeFiltroComunaMapa = new Ext.data.JsonStore({
        fields: ['IdComuna', 'ComunaMapa'],
        data: [{ IdComuna: '0', ComunaMapa: 'Distrito Federal', Latitud: '19.419444', Longitud: '-99.145556' },
                 { IdComuna: '1', ComunaMapa: 'Aguascalientes', Latitud: '22.021667', Longitud: '-102.356389' },
                 { IdComuna: '2', ComunaMapa: 'Baja California', Latitud: '29.95', Longitud: '-115.116667' },
                 { IdComuna: '3', ComunaMapa: 'Baja California Sur', Latitud: '25.846111', Longitud: '-111.972778' },
                 { IdComuna: '4', ComunaMapa: 'Campeche', Latitud: '18.836389', Longitud: '-90.403333' },
                 { IdComuna: '5', ComunaMapa: 'Chiapas', Latitud: '16.41', Longitud: '-92.408611' },
                 { IdComuna: '6', ComunaMapa: 'Chihuahua', Latitud: '28.814167', Longitud: '-106.439444' },
                 { IdComuna: '7', ComunaMapa: 'Coahuila de Zaragoza', Latitud: '27.302222', Longitud: '-102.044722' },
                 { IdComuna: '8', ComunaMapa: 'Colima', Latitud: '19.096667', Longitud: '-103.960833' },
                 { IdComuna: '9', ComunaMapa: 'Durango', Latitud: '24.934722', Longitud: '-104.911944' },
                 { IdComuna: '10', ComunaMapa: 'Guanajuato', Latitud: '21.018889', Longitud: '-101.262778' },
                 { IdComuna: '11', ComunaMapa: 'Guerrero', Latitud: '17.613056', Longitud: '-99.95' },
                 { IdComuna: '12', ComunaMapa: 'Hidalgo', Latitud: '20.478333', Longitud: '-98.863611' },
                 { IdComuna: '13', ComunaMapa: 'Jalisco', Latitud: '20.566667', Longitud: '-103.676389' },
                 { IdComuna: '14', ComunaMapa: 'México', Latitud: '19.354167', Longitud: '-99.630833' },
                 { IdComuna: '15', ComunaMapa: 'Michoacán', Latitud: '19.168611', Longitud: '-101.899722' },
                 { IdComuna: '16', ComunaMapa: 'Morelos', Latitud: '18.7475', Longitud: '-99.070278' },
                 { IdComuna: '17', ComunaMapa: 'Nayarit', Latitud: '21.743889', Longitud: '-105.228333' },
                 { IdComuna: '18', ComunaMapa: 'Nuevo León', Latitud: '25.566667', Longitud: '-99.970556' },
                 { IdComuna: '19', ComunaMapa: 'Oaxaca', Latitud: '16.898056', Longitud: '-96.414167' },
                 { IdComuna: '20', ComunaMapa: 'Puebla', Latitud: '19.003611', Longitud: '-97.888333' },
                 { IdComuna: '21', ComunaMapa: 'Querétaro', Latitud: '20.591', Longitud: '-100.391' },
                 { IdComuna: '22', ComunaMapa: 'Quintana Roo', Latitud: '19.6', Longitud: '-87.916667' },
                 { IdComuna: '23', ComunaMapa: 'San Luis Potosí', Latitud: '22.603333', Longitud: '-100.429722' },
                 { IdComuna: '24', ComunaMapa: 'Sinaloa', Latitud: '25.002778', Longitud: '-107.502778' },
                 { IdComuna: '25', ComunaMapa: 'Sonora', Latitud: '29.646111', Longitud: '-110.868889' },
                 { IdComuna: '26', ComunaMapa: 'Tabasco', Latitud: '17.972222', Longitud: '-92.588889' },
                 { IdComuna: '27', ComunaMapa: 'Tamaulipas', Latitud: '24.287222', Longitud: '-98.563333' },
                 { IdComuna: '28', ComunaMapa: 'Tlaxcala', Latitud: '19.428889', Longitud: '-98.160833' },
                 { IdComuna: '29', ComunaMapa: 'Veracruz', Latitud: '19.434722', Longitud: '-96.383056' },
                 { IdComuna: '30', ComunaMapa: 'Yucatán', Latitud: '20.833333', Longitud: '-89' },
                 { IdComuna: '31', ComunaMapa: 'Sacatecas', Latitud: '23.292778', Longitud: '-102.700556' }
        ]
      });

      var comboFiltroComunaMapa = new Ext.form.field.ComboBox({
        id: 'comboFiltroComunaMapa',
        fieldLabel: 'Estado',
        store: storeFiltroComunaMapa,
        valueField: 'IdComuna',
        displayField: 'ComunaMapa',
        emptyText: 'Seleccione...',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 110,
        editable: false,
        style: {
          marginTop: '5px',
          marginLeft: '5px'
        },
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        forceSelection: true,
        listeners: {
          select: function () {
            FiltrarComunaMapa();
          }
        }
      });

      //Ext.getCmp('comboFiltroComunaMapa').setValue('0');

      var chkNroTransporte = new Ext.form.Checkbox({
        id: 'chkNroTransporte',
        labelSeparator: '',
        hideLabel: true,
        checked: false,
        style: {
          marginTop: '7px',
          marginLeft: '5px'
        },
        listeners: {
          change: function (cb, checked) {
            if (checked == true) {
              Ext.getCmp("textFiltroNroTransporte").setDisabled(false);
              Ext.getCmp("comboFiltroEstadoViaje").setDisabled(true);
              Ext.getCmp("comboFiltroEstadoGPS").setDisabled(true);
              Ext.getCmp("comboFiltroTransportista").setDisabled(true);
              Ext.getCmp("comboFiltroProveedorGPS").setDisabled(true);
              Ext.getCmp("comboFiltroPatente").setDisabled(true);

            }
            else {
              Ext.getCmp("textFiltroNroTransporte").setDisabled(true);
              Ext.getCmp("comboFiltroEstadoViaje").setDisabled(false);
              Ext.getCmp("comboFiltroEstadoGPS").setDisabled(false);
              Ext.getCmp("comboFiltroTransportista").setDisabled(false);
              Ext.getCmp("comboFiltroProveedorGPS").setDisabled(false);
              Ext.getCmp("comboFiltroPatente").setDisabled(false);

              Ext.getCmp('textFiltroNroTransporte').reset();

            }
          }
        }
      });

      var textFiltroNroTransporte = new Ext.form.TextField({
        id: 'textFiltroNroTransporte',
        fieldLabel: 'ID Embarque',
        labelWidth: 80,
        allowBlank: true,
        anchor: '99%',
        maxLength: 20,
        style: {
          marginTop: '5px',
          marginLeft: '5px'
        },
        disabled: true

      });

      var storeFiltroEstadoViaje = new Ext.data.JsonStore({
        fields: ['EstadoViaje'],
        data: [{ "EstadoViaje": "Todos" },
                { "EstadoViaje": "En Viaje" },
                { "EstadoViaje": "Liberado" }
        ]
      });

      var comboFiltroEstadoViaje = new Ext.form.field.ComboBox({
        id: 'comboFiltroEstadoViaje',
        fieldLabel: 'Estado Viaje',
        store: storeFiltroEstadoViaje,
        valueField: 'EstadoViaje',
        displayField: 'EstadoViaje',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 110,
        editable: false,
        style: {
          marginTop: '5px',
          marginLeft: '5px'
        },
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        forceSelection: true,
        listeners: {
          select: function () {
            //FiltrarFlota();
          }
        }
      });

      Ext.getCmp('comboFiltroEstadoViaje').setValue('En Viaje');

      var storeFiltroTransportista = new Ext.data.JsonStore({
        fields: ['Transportista'],
        proxy: new Ext.data.HttpProxy({
          //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetTransportistasRuta&Todos=True',
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroTransportista = new Ext.form.field.ComboBox({
        id: 'comboFiltroTransportista',
        fieldLabel: 'Línea Transporte',
        forceSelection: true,
        store: storeFiltroTransportista,
        valueField: 'Transportista',
        displayField: 'Transportista',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 110,
        style: {
          marginLeft: '5px'
        },
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        forceSelection: true,
        listeners: {
          select: function () {
            FiltrarPatentes();
            FiltrarProveedoresGPS();
            //FiltrarFlota();
          }
        }
      });

      var storeFiltroPatente = new Ext.data.JsonStore({
        fields: ['Patente'],
        proxy: new Ext.data.HttpProxy({

          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroPatente = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatente',
        fieldLabel: 'Tracto',
        labelWidth: 110,
        store: storeFiltroPatente,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        forceSelection: true,
        allowBlank: false,
        style: {
          marginLeft: '5px'
        },
        listeners: {
          select: function () {
            //FiltrarFlota();
          }
        }
      });

      var storeFiltroEstadoGPS = new Ext.data.JsonStore({
        fields: ['EstadoGPS'],
        data: [{ "EstadoGPS": "Todos" },
                { "EstadoGPS": "Online" },
                { "EstadoGPS": "Offline" }
        ]
      });

      var comboFiltroEstadoGPS = new Ext.form.field.ComboBox({
        id: 'comboFiltroEstadoGPS',
        fieldLabel: 'Estado GPS',
        store: storeFiltroEstadoGPS,
        valueField: 'EstadoGPS',
        displayField: 'EstadoGPS',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 110,
        editable: false,
        style: {
          marginTop: '5px',
          marginLeft: '5px'
        },
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        forceSelection: true,
        listeners: {
          select: function () {
            //FiltrarFlota();
          }
        }
      });

      Ext.getCmp('comboFiltroEstadoGPS').setValue('Todos');

      var storeFiltroProveedorGPS = new Ext.data.JsonStore({
        fields: ['ProveedorGPS'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetProveedoresGPS&Todos=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroProveedorGPS = new Ext.form.field.ComboBox({
        id: 'comboFiltroProveedorGPS',
        fieldLabel: 'Proveedor GPS',
        forceSelection: true,
        store: storeFiltroProveedorGPS,
        valueField: 'ProveedorGPS',
        displayField: 'ProveedorGPS',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 110,
        style: {
          marginLeft: '5px'
        },
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        forceSelection: true,
        listeners: {
          select: function () {
            //FiltrarFlota();
          }
        }
      });

      storeFiltroProveedorGPS.load({
        callback: function (r, options, success) {
          if (success) {
            FiltrarProveedoresGPS();
            Ext.getCmp("comboFiltroProveedorGPS").setValue("Todos");
          }
        }
      })

      storeFiltroTransportista.load({
        callback: function (r, options, success) {
          if (success) {
            var firstTransportista = Ext.getCmp("comboFiltroTransportista").store.getAt(0).get("Transportista");
            Ext.getCmp("comboFiltroTransportista").setValue(firstTransportista);

            storeFiltroProveedorGPS.load({
              params: {
                Transportista: firstTransportista
              },
              callback: function (r, options, success) {
                if (success) {
                  Ext.getCmp("comboFiltroProveedorGPS").setValue("Todos");
                }
              }
            })

            storeFiltroPatente.load({
              callback: function (r, options, success) {
                if (success) {
                  Ext.getCmp("comboFiltroPatente").setValue("Todas");
                  FiltrarPatentes();
                  FiltrarFlota();
                }
              }
            })

          }
        }
      })

      var chkMostrarZonas = new Ext.form.Checkbox({
        id: 'chkMostrarZonas',
        fieldLabel: 'Mostrar locales',
        labelWidth: 100,
        width: 130,
        checked: false,
        style: {
          marginLeft: '5px'
        },
        listeners: {
          change: function (cb, checked) {
            if (checked == true) {
              MostrarZonas();
            }
            else {
              eraseAllZones();
            }
          }
        }
      });

      var chkMostrarLabels = new Ext.form.Checkbox({
        id: 'chkMostrarLabels',
        fieldLabel: 'Mostrar etiquetas',
        width: 130,
        checked: false,
        style: {
          marginLeft: '5px'
        },
        listeners: {
          change: function (cb, checked) {
            if (checked == true) {
              for (var i = 0; i < zoneLabels.length; i++) {
                zoneLabels[i].setMap(map);
              }
            }
            else {
              for (var i = 0; i < zoneLabels.length; i++) {
                zoneLabels[i].setMap(null);
              }
            }
          }
        }
      });

      var chkMostrarTrafico = new Ext.form.Checkbox({
        id: 'chkMostrarTrafico',
        fieldLabel: 'Mostrar tráfico',
        labelWidth: 90,
        width: 130,
        checked: false,
        listeners: {
          change: function (cb, checked) {
            if (checked == true) {
              trafficLayer.setMap(map);
            }
            else {
              trafficLayer.setMap(null);
            }
          }
        }
      });

      var btnActualizar = {
        id: 'btnActualizar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/refresh_gray_20x20.png',
        text: 'Actualizar',
        width: 80,
        height: 26,
        handler: function () {
          FiltrarFlota();
        }
      };

      var btnExportar = {
        id: 'btnExportar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/export_black_20x20.png',
        text: 'Exportar',
        width: 80,
        height: 26,
        style: {
          marginLeft: '20px'
        },
        listeners: {
          click: {
            element: 'el',
            fn: function () {

              var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getValue();
              var estadoViaje = Ext.getCmp('comboFiltroEstadoViaje').getValue();
              var estadoGPS = Ext.getCmp('comboFiltroEstadoGPS').getValue();
              var transportista = Ext.getCmp('comboFiltroTransportista').getValue();
              var proveedorGPS = Ext.getCmp('comboFiltroProveedorGPS').getValue();
              var patente = Ext.getCmp('comboFiltroPatente').getValue();

              var mapForm = document.createElement("form");
              mapForm.target = "ToExcel";
              mapForm.method = "POST"; // or "post" if appropriate
              mapForm.action = 'MonitoreoOnline.aspx?Metodo=ExportExcel';

              var _nroTransporte = document.createElement("input");
              _nroTransporte.type = "text";
              _nroTransporte.name = "nroTransporte";
              _nroTransporte.value = nroTransporte;
              mapForm.appendChild(_nroTransporte);

              var _estadoViaje = document.createElement("input");
              _estadoViaje.type = "text";
              _estadoViaje.name = "estadoViaje";
              _estadoViaje.value = estadoViaje;
              mapForm.appendChild(_estadoViaje);

              var _estadoGPS = document.createElement("input");
              _estadoGPS.type = "text";
              _estadoGPS.name = "estadoGPS";
              _estadoGPS.value = estadoGPS;
              mapForm.appendChild(_estadoGPS);

              var _transportista = document.createElement("input");
              _transportista.type = "text";
              _transportista.name = "transportista";
              _transportista.value = transportista;
              mapForm.appendChild(_transportista);

              var _proveedorGPS = document.createElement("input");
              _proveedorGPS.type = "text";
              _proveedorGPS.name = "proveedorGPS";
              _proveedorGPS.value = proveedorGPS;
              mapForm.appendChild(_proveedorGPS);

              var _patente = document.createElement("input");
              _patente.type = "text";
              _patente.name = "patente";
              _patente.value = patente;
              mapForm.appendChild(_patente);

              document.body.appendChild(mapForm);
              mapForm.submit();

            }
          }
        }
      };

      var toolbarPosiciones = Ext.create('Ext.toolbar.Toolbar', {
        id: 'toolbarPosiciones',
        height: 91,
        layout: 'column',
        items: [{
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.5,
          items: [comboFiltroEstadoViaje, comboFiltroTransportista, comboFiltroPatente]
        }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.5,
          items: [comboFiltroEstadoGPS, comboFiltroProveedorGPS, comboFiltroComunaMapa]
        }]
      });



      var storePosiciones = new Ext.data.JsonStore({
        autoLoad: false,
        fields: [{ name: 'UltReporte', type: 'date', dateFormat: 'c' },
                  'TextUltReporte',
                  'Patente',
                  'Transportista',
                  'Latitud',
                  'Longitud',
                  'Ignicion',
                  'Velocidad',
                  'Direccion',
                  'EstadoGPS',
                  'Puerta1',
                  'Temperatura1',
                  'EstadoViaje',
                  'NroTransporte',
                  'IdEmbarque',
                  'CodigoOrigen',
                  'LatitudOrigen',
                  'LongitudOrigen',
                  'CodigoDestino',
                  'LatitudDestino',
                  'LongitudDestino',
                  'ProveedorGPS',
                  'RutConductor',
                  'NombreConductor',
        ],
        groupField: 'EstadoViaje',
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetMonitoreoOnline',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
        groupHeaderTpl: '{name} ({rows.length})'
      });

      var gridPosiciones = Ext.create('Ext.grid.Panel', {
        id: 'gridPosiciones',
        store: storePosiciones,
        tbar: toolbarPosiciones,
        columnLines: true,
        anchor: '100% 100%',
        scroll: false,
        features: [groupingFeature],
        buttons: [chkMostrarZonas, chkMostrarLabels, chkMostrarTrafico, btnExportar, btnActualizar],
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columns: [
                      { text: 'Fecha', width: 105, dataIndex: 'UltReporte', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                      { text: 'ID Master', width: 75, dataIndex: 'NroTransporte' },
                      { text: 'Estado', flex: 1, dataIndex: 'EstadoViaje' },
                      { text: 'Línea Transp.', flex: 1, dataIndex: 'Transportista' },
                      { text: 'Placa', width: 57, dataIndex: 'Patente' },
                      { text: 'Proveedor', width: 70, dataIndex: 'ProveedorGPS' },
                      { text: 'Ignición', width: 55, dataIndex: 'Ignicion' },
                      { text: 'Vel.', width: 35, dataIndex: 'Velocidad' },
                      { text: 'GPS', width: 50, dataIndex: 'EstadoGPS', renderer: renderEstadoGPS },
                      /*{
                          xtype: 'actioncolumn',
                          width: 22,
                          items: [{
                              icon: 'Images/close_red_16x16.png',
                              tooltip: 'Detener motor.',
                              handler: function (grid, rowIndex, colIndex) {
                                  var row = grid.getStore().getAt(rowIndex);
  
                                  DetenerMotor(row);
  
                              }
                          }]
                      }*/

        ],
        listeners: {
          select: function (sm, row, rec) {

            var latOrigen = Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.LatitudOrigen;
            var lonOrigen = Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.LongitudOrigen;
            var latDestino = Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.LatitudDestino;
            var lonDestino = Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.LongitudDestino;

            var latlonOrigen = new google.maps.LatLng(latOrigen, lonOrigen);
            var latlonDestino = new google.maps.LatLng(latDestino, lonDestino);
            /*
                    if (directionsDisplay) {
                        directionsDisplay.setMap(null);
                    }
            
                    directionsDisplay.setMap(map);
            
                    var request = {
                        origin: latlonOrigen,
                        destination: latlonDestino,
                        travelMode: google.maps.TravelMode.DRIVING,
                        unitSystem: google.maps.UnitSystem.METRIC,
                        provideRouteAlternatives: false,
                        region: 'CL'
                    };
            
                    directionsService.route(request,function(response, status) {
                        if (status === google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setOptions({ preserveViewport: true });
                            directionsDisplay.setDirections(response);
                        } 
                    });
            */
            var patFec = Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.Patente + Ext.getCmp('gridPosiciones').getStore().data.items[rec].raw.UltReporte.toString();

            for (var i = 0; i < markers.length; i++) {
              if (markers[i].labelText == patFec) {
                markers[i].setAnimation(google.maps.Animation.BOUNCE);
                setTimeout('markers[' + i + '].setAnimation(null);', 800);

                var estadoMotor = '';
                var estadoReporte = '';
                var Origen = '';
                var destino = '';
                var agrupacion = '';

                Ext.Ajax.request({
                  async: true,
                  url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetinfoWindow',
                  params: {
                    nrotransporte: row.data.NroTransporte
                  },
                  success: function (data, success) {
                    if (data != null) {
                      data = Ext.decode(data.responseText);

                      if (data.length > 0) {
                        estadoMotor = data[0].Ignicion;

                        if (data[0].EstadoReporte === 0)
                          estadoReporte = "<p style='color:red;'>Sin Reporte</p>";
                        else if (data[0].EstadoReporte === 1)
                          estadoReporte = "<p style='color:green;'>En Tiempo</p>";
                        else if (data[0].EstadoReporte === 2)
                          estadoReporte = "<p style='color:yellow;'>Atraso</p>";

                        Origen = data[0].DescOrigen;
                        destino = data[0].DescDestino;

                        if (data[0].Agrupacion.trim() == '')
                          agrupacion = 'Sin registro';
                        else
                          agrupacion = data[0].Agrupacion;

                      }
                      else {
                        estadoMotor = 'Sin registro';
                        estadoReporte = "<p style='color:red;'>Sin Reporte</p>";
                        Origen = 'Sin registro';
                        destino = 'Sin registro';
                        agrupacion = 'Sin registro';
                      }

                    }

                    var contentString =

                        '<br>' +
                            '<table>' +
                              '<tr>' +
                                  '       <td><b>Fecha</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '       <td>' + row.data.TextUltReporte + '</td>' +
                              '</tr>' +
                              '<tr>' +
                                  '        <td><b>ID Master:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + row.data.NroTransporte + '</td>' +
                              '</tr>' +
                              //'<tr>' +
                              //    '        <td><b>ID Embarque:</b></td>' +
                              //    '       <td><pre>     </pre></td>' +
                              //    '        <td>' + row.data.IdEmbarque + '</td>' +
                              //'</tr>' +
                              '<tr>' +
                                  '        <td><b>Línea Transporte:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + row.data.Transportista + '</td>' +
                              '</tr>' +
                              '<tr>' +
                                  '        <td><b>Placa:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + row.data.Patente + '</td>' +
                              '</tr>' +
                              '<tr>' +
                                  '        <td><b>Velocidad:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + row.data.Velocidad + ' Km/h </td>' +
                              '</tr>' +
                              '<tr>' +
                                  '        <td><b>Latitud:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + row.data.Latitud + '</td>' +
                              '</tr>' +
                                '<tr>' +
                                  '        <td><b>Longitud:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + row.data.Longitud + '</td>' +
                              '</tr>' +
                              //'<tr>' +
                              //    '        <td><b>Nombre Conductor:</b></td>' +
                              //    '       <td><pre>     </pre></td>' +
                              //    '        <td>' + row.data.NombreConductor + '</td>' +
                              //'</tr>' +
                               '<tr>' +
                                    '        <td><b>Origen:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + Origen + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '        <td><b>Destino:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + destino + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '        <td><b>Estado Reporte:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + estadoReporte + ' </td>' +
                                '</tr>' +
                                '<tr>' +
                                    '        <td><b>Estado Motor:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + estadoMotor + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '        <td><b>Agrupación:</b></td>' +
                                    '       <td><pre>     </pre></td>' +
                                    '        <td>' + agrupacion + '</td>' +
                                '</tr>' +
                            '</table>' +
                          '<br>';
                    infowindow.setContent(contentString);
                  }
                });
                infowindow.open(map, markers[i]);
                break;
              }
            }
            map.setCenter(new google.maps.LatLng(row.data.Latitud, row.data.Longitud));
            Ext.getCmp("gridPosiciones").getSelectionModel().deselectAll();
          }
        }
      });

      var viewWidth = Ext.getBody().getViewSize().width;
      var viewHeight = Ext.getBody().getViewSize().height;

      var leftPanel = new Ext.FormPanel({
        id: 'leftPanel',
        region: 'west',
        margins: '0 0 3 3',
        border: true,
        width: 600,
        minWidth: 300,
        maxWidth: viewWidth / 2,
        layout: 'anchor',
        split: true,
        collapsible: true,
        items: [gridPosiciones]
      });

      leftPanel.on('collapse', function () {
        google.maps.event.trigger(map, "resize");
      });

      leftPanel.on('expand', function () {
        google.maps.event.trigger(map, "resize");
      });

      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        contentEl: 'dvMap'
      });

            

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, leftPanel, centerPanel]
      });

      viewport.on('resize', function () {
        google.maps.event.trigger(map, "resize");
      });

      //var refreshPanel = function () {
      //    FiltrarFlota();
      //};
      //setInterval(refreshPanel, 120000); //120 seg


    });
  </script>

  <script type="text/javascript">

    var zoneLabels = new Array();

    Ext.onReady(function () {
      GeneraMapa("dvMap", true);
    });

    function FiltrarPatentes() {
      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

      var store = Ext.getCmp('comboFiltroPatente').store;
      store.load({
        params: {
          transportista: transportista
        }
      });
    }

    function FiltrarComunaMapa() {
      var Id = Ext.getCmp('comboFiltroComunaMapa').getValue();

      var lat = Ext.getCmp('comboFiltroComunaMapa').store.getAt(Id).data.Latitud;
      var lon = Ext.getCmp('comboFiltroComunaMapa').store.getAt(Id).data.Longitud;

      map.setCenter(new google.maps.LatLng(lat, lon));
      map.setZoom(15);

    }

    function FiltrarProveedoresGPS() {
      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

      var store = Ext.getCmp('comboFiltroProveedorGPS').store;
      store.load({
        params: {
          Transportista: transportista
        }
      });
    }

    function FiltrarFlota() {
      //ClearMap();

      var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getValue();
      var estadoViaje = Ext.getCmp('comboFiltroEstadoViaje').getValue();
      var estadoGPS = Ext.getCmp('comboFiltroEstadoGPS').getValue();
      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();
      var proveedorGPS = Ext.getCmp('comboFiltroProveedorGPS').getValue();
      var patente = Ext.getCmp('comboFiltroPatente').getValue();

      var store = Ext.getCmp('gridPosiciones').store;
      store.load({
        params: {
          nroTransporte: nroTransporte,
          estadoViaje: estadoViaje,
          estadoGPS: estadoGPS,
          transportista: transportista,
          proveedorGPS: proveedorGPS,
          patente: patente
        },
        callback: function (r, options, success) {
          if (!success) {

          }
          else {
            MostrarFlota();

          }
        }
      });
    }

    var renderEstadoGPS = function (value, meta) {
      {
        if (value == 'Online') {
          meta.tdCls = 'blue-cell';
          return value;
        }
        if (value == 'Offline') {
          meta.tdCls = 'red-cell';
          return value;
        }
        else {
          meta.tdCls = 'black-cell';
          return value;
        }
      }
    };

    function MostrarFlota() {

      ClearMap();

      arrayPositions.splice(0, arrayPositions.length);

      var store = Ext.getCmp('gridPosiciones').getStore();
      var rowCount = store.count();
      var iterRow = 0;

      while (iterRow < rowCount) {

        var dir = parseInt(store.data.items[iterRow].raw.Direccion);

        var lat = store.data.items[iterRow].raw.Latitud;
        var lon = store.data.items[iterRow].raw.Longitud;

        var Latlng = new google.maps.LatLng(lat, lon);

        arrayPositions.push({
          Patente: store.data.items[iterRow].raw.Patente,
          Fecha: store.data.items[iterRow].raw.UltReporte.toString(),
          Velocidad: store.data.items[iterRow].raw.Velocidad,
          Latitud: lat,
          Longitud: lon,
          LatLng: Latlng,
          Puerta: store.data.items[iterRow].raw.Puerta1,
          Temperatura: store.data.items[iterRow].raw.Temperatura1,
          NroTransporte: store.data.items[iterRow].raw.NroTransporte,
          IdEmbarque: store.data.items[iterRow].raw.IdEmbarque,
          Transportista: store.data.items[iterRow].raw.Transportista,
          RutConductor: store.data.items[iterRow].raw.RutConductor,
          NombreConductor: store.data.items[iterRow].raw.NombreConductor,
        });

        var iconRoute;

        if (store.data.items[iterRow].raw.EstadoViaje == 'Liberado') {
          iconRoute = 'Images/Truck_Empty/';
        }
        else {
          iconRoute = 'Images/Truck_Loaded/'
        }

        switch (true) {
          case ((dir >= 338) || (dir < 22)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '1_N_21x29.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
          case ((dir >= 22) && (dir < 67)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '2_NE_32x30.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
          case ((dir >= 67) && (dir < 112)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '3_E_30x22.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
          case ((dir >= 112) && (dir < 157)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '4_SE_30x32.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
          case ((dir >= 157) && (dir < 202)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '5_S_21x29.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
          case ((dir >= 202) && (dir < 247)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '6_SW_30x32.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
          case ((dir >= 247) && (dir < 292)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '7_W_30x22.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
          case ((dir >= 292) && (dir < 338)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: iconRoute + '8_NW_32x30.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
            });
            break;
        }

        var label = new Label({
          map: null
        });
        label.bindTo('position', marker, 'position');
        label.bindTo('text', marker, 'labelText');

        google.maps.event.addListener(marker, 'click', function () {
          var latLng = this.position;
          var patFec = this.labelText;

          for (i = 0; i < arrayPositions.length; i++) {
            if ((arrayPositions[i].Patente + arrayPositions[i].Fecha.toString()) == patFec.toString() & arrayPositions[i].LatLng.toString() == latLng.toString()) {

              var estadoMotor = '';
              var estadoReporte = '';
              var Origen = '';
              var destino = '';
              var agrupacion = '';

              Ext.Ajax.request({
                async: true,
                url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetinfoWindow',
                params: {
                  nrotransporte: arrayPositions[i].NroTransporte
                },
                success: function (data, success) {
                  if (data != null) {
                    data = Ext.decode(data.responseText);

                    if (data.length > 0) {
                      estadoMotor = data[0].Ignicion;

                      if (data[0].EstadoReporte === 0)
                        estadoReporte = "<p style='color:red;'>Sin Reporte</p>";
                      else if (data[0].EstadoReporte === 1)
                        estadoReporte = "<p style='color:green;'>En Tiempo</p>";
                      else if (data[0].EstadoReporte === 2)
                        estadoReporte = "<p style='color:yellow;'>Atraso</p>";

                      Origen = data[0].DescOrigen;
                      destino = data[0].DescDestino;

                      if (data[0].Agrupacion.trim() == '')
                        agrupacion = 'Sin registro';
                      else
                        agrupacion = data[0].Agrupacion;

                    }
                    else {
                      estadoMotor = 'Sin registro';
                      estadoReporte = "<p style='color:red;'>Sin Reporte</p>";
                      Origen = 'Sin registro';
                      destino = 'Sin registro';
                      agrupacion = 'Sin registro';
                    }

                  }

                  var contentString =

                                     '<br>' +
                                         '<table>' +
                                           '<tr>' +
                                               '       <td><b>Fecha</b></td>' +
                                               '       <td><pre>     </pre></td>' +
                                               '       <td>' + (arrayPositions[i].Fecha.toString()).replace("T", " ") + '</td>' +
                                           '</tr>' +
                                           '<tr>' +
                                               '        <td><b>ID Master:</b></td>' +
                                               '       <td><pre>     </pre></td>' +
                                               '        <td>' + arrayPositions[i].NroTransporte + '</td>' +
                                           '</tr>' +
                                           //'<tr>' +
                                           //    '        <td><b>ID Embarque:</b></td>' +
                                           //    '       <td><pre>     </pre></td>' +
                                           //    '        <td>' + arrayPositions[i].IdEmbarque + '</td>' +
                                           //'</tr>' +
                                           '<tr>' +
                                               '        <td><b>Línea Transporte:</b></td>' +
                                               '       <td><pre>     </pre></td>' +
                                               '        <td>' + arrayPositions[i].Transportista + '</td>' +
                                           '</tr>' +
                                           '<tr>' +
                                               '        <td><b>Tracto:</b></td>' +
                                               '       <td><pre>     </pre></td>' +
                                               '        <td>' + arrayPositions[i].Patente + '</td>' +
                                           '</tr>' +
                                           '<tr>' +
                                               '        <td><b>Velocidad:</b></td>' +
                                               '       <td><pre>     </pre></td>' +
                                               '        <td>' + arrayPositions[i].Velocidad + ' Km/h </td>' +
                                           '</tr>' +
                                           '<tr>' +
                                            '        <td><b>Latitud:</b></td>' +
                                            '       <td><pre>     </pre></td>' +
                                            '        <td>' + lat + '</td>' +
                                        '</tr>' +
                                         '<tr>' +
                                            '        <td><b>Longitud:</b></td>' +
                                            '       <td><pre>     </pre></td>' +
                                            '        <td>' + lon + '</td>' +
                                        '</tr>' +
                                           //'<tr>' +
                                           //    '        <td><b>Nombre Conductor:</b></td>' +
                                           //    '       <td><pre>     </pre></td>' +
                                           //    '        <td>' + arrayPositions[i].NombreConductor + '</td>' +
                                           //'</tr>' +
                                           '<tr>' +
                                                             '        <td><b>Origen:</b></td>' +
                                                             '       <td><pre>     </pre></td>' +
                                                             '        <td>' + Origen + '</td>' +
                                                         '</tr>' +
                                                         '<tr>' +
                                                             '        <td><b>Destino:</b></td>' +
                                                             '       <td><pre>     </pre></td>' +
                                                             '        <td>' + destino + '</td>' +
                                                         '</tr>' +
                                                         '<tr>' +
                                                             '        <td><b>Estado Reporte:</b></td>' +
                                                             '       <td><pre>     </pre></td>' +
                                                             '        <td>' + estadoReporte + ' </td>' +
                                                         '</tr>' +
                                                          '<tr>' +
                                                             '        <td><b>Estado Motor:</b></td>' +
                                                             '       <td><pre>     </pre></td>' +
                                                             '        <td>' + estadoMotor + '</td>' +
                                                         '</tr>' +
                                                          '<tr>' +
                                                             '        <td><b>Agrupación:</b></td>' +
                                                             '       <td><pre>     </pre></td>' +
                                                             '        <td>' + agrupacion + '</td>' +
                                                         '</tr>' +
                                         '</table>' +
                                       '<br>';

                  infowindow.setContent(contentString);


                }
              });

              infowindow.open(map, this);


              break;
            }
          }

        });

        markers.push(marker);
        labels.push(label);

        iterRow++;
      }

    }

    function MostrarZonas() {

      Ext.Msg.wait('Espere por favor...', 'Generando zonas');

      var countZonas = Ext.getCmp('comboZonas').store.count()

      for (i = 0; i < countZonas; i++) {
        var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
        var idTipoZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdTipoZona;

        if (idTipoZona == 1 || idTipoZona == 2) {
          DrawZone(idZona, idTipoZona);
        }
      }

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
        success: function (response, opts) {

          var task = new Ext.util.DelayedTask(function () {
            Ext.Msg.hide();
          });

          task.delay(1500);

        },
        failure: function (response, opts) {
          Ext.Msg.hide();
        }
      });
    }

    function DrawZone(idZona) {

      for (var i = 0; i < geoLayer.length; i++) {
        geoLayer[i].layer.setMap(null);
        geoLayer[i].label.setMap(null);
        geoLayer.splice(i, 1);
      }

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
        params: {
          IdZona: idZona
        },
        success: function (data, success) {
          if (data != null) {
            data = Ext.decode(data.responseText);
            if (data.Vertices.length > 1) { //Polygon
              var polygonGrid = new Object();
              polygonGrid.IdZona = data.IdZona;

              var arr = new Array();
              for (var i = 0; i < data.Vertices.length; i++) {
                arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
              }

              if (data.idTipoZona == 3) {
                var colorZone = "#FF0000";
              }
              else {
                var colorZone = "#7f7fff";
              }

              polygonGrid.layer = new google.maps.Polygon({
                paths: arr,
                strokeColor: "#000000",
                strokeWeight: 1,
                strokeOpacity: 0.9,
                fillColor: colorZone,
                fillOpacity: 0.3,
                labelText: data.NombreZona
              });

              var viewLabel = Ext.getCmp('chkMostrarLabels').getValue();
              polygonGrid.label = new Label({
                text: idZona,
                position: new google.maps.LatLng(data.Latitud, data.Longitud),
                map: viewLabel ? map : null
              });
              polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
              polygonGrid.layer.setMap(map);
              geoLayer.push(polygonGrid);

              if (containsLabel(zoneLabels, idZona) == false) {
                zoneLabels.push(polygonGrid.label);
              }
            }
            else
              if (data.Vertices.length = 1) { //Point
                var Point = new Object();
                Point.IdZona = data.IdZona;

                var image = new google.maps.MarkerImage("Images/greymarker_32x32.png");

                Point.layer = new google.maps.Marker({
                  position: new google.maps.LatLng(data.Latitud, data.Longitud),
                  icon: image,
                  labelText: data.NombreZona,
                  map: map
                });

                Point.label = new Label({
                  position: new google.maps.LatLng(data.Latitud, data.Longitud),
                  map: map
                });

                Point.label.bindTo('text', Point.layer, 'labelText');
                Point.layer.setMap(map);
                geoLayer.push(Point);
              }

          }
        },
        failure: function (msg) {
          alert('Se ha producido un error.');
        }
      });
    }

    function containsZone(a, obj) {
      var i = a.length;
      while (i--) {
        if (a[i].IdZona === obj) {
          return true;
        }
      }
      return false;
    }

    function containsLabel(a, obj) {
      var i = a.length;
      while (i--) {
        //GetDetalleAreaSeleccionada
        if (a[i].text === obj) {
          return true;
        }
      }
      return false;
    }

    function eraseAllZones() {
      var countZonas = Ext.getCmp('comboZonas').store.count()

      for (i = 0; i < countZonas; i++) {
        var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
        EraseZone(idZona);
      }

      zoneLabels = [];
    }

    function EraseZone(idZona) {
      for (var i = 0; i < geoLayer.length; i++) {
        if (idZona == geoLayer[i].IdZona) {
          geoLayer[i].layer.setMap(null);
          geoLayer[i].label.setMap(null);
          geoLayer.splice(i, 1);
        }
      }

      for (var i = 0; i < zoneLabels.length; i++) {
        if (zoneLabels[i].text == idZona) {
          zoneLabels[i].setMap(null);
          zoneLabels.splice(i, 1);

        }
      }
    }

    function DetenerMotor(row) {

      if (!confirm("Se enviará comando para detención de motor del equipo seleccionado. ¿Desea continuar?")) {
        return;
      }
      else {
        alert('Comando para detención de motor enviado satisfactoriamente.');
      }

    }

  </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
