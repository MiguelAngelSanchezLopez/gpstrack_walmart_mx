﻿var Columnas =
    {
        Id: 0,
        NombreCorto: 1,
        Descripcion: 2,
        Liga: 3,
        Activo: 4,
        Acciones: 5
    };

var elementoTableroTemporal = new Object();

var objetoJSONTableros = new Object();

var idRegistroOperacionTemporal = "";

var filtroId = "";

var filtroIdTablero = "";

var arregloCamposValidacion =
    [
        "txtNombreCortoAdm",
        "txtDescripcionAdm",
        "txtLigaAdm"
    ];

function LimpiarElementoTableroTemporal() {
    elementoTableroTemporal = {
        "Id": "0",
        "NombreCorto": "",
        "Descripcion": "",
        "Liga": "",
        "Activo": "False"
    };
}

var arregloImagenesTablero = [];

function LlenarArregloImagenesTablero() {
    //arregloImagenesTablero.push("Images/panel1.jpg");
    //arregloImagenesTablero.push("Images/panel2.jpg");
}

function ObtenerURLImagenTablero(indice) {
    return arregloImagenesTablero[indice];
}

function CrearContenedorImagenTablero(indice) {
    var imagenTablerosHTML = [];

    imagenTablerosHTML.push('<div>');
    imagenTablerosHTML.push('<img data-u="image" src="' + ObtenerURLImagenTablero(indice) + '" />');
    imagenTablerosHTML.push('</div>');

    return imagenTablerosHTML.join("");
}

function LlenarContenedorImagenesTableros() {
    var imagenesTablerosHTML = [];

    for (var indice = 0; indice < arregloImagenesTablero.length; indice++)
        imagenesTablerosHTML.push(CrearContenedorImagenTablero(indice));

    $("#contenedorImagenesTableros").html(imagenesTablerosHTML.join(""));
    
}

function EstablecerTituloTablero() {
    $("#tituloTablero").html('Tablero 1 a');
}

function EstablecerEnlaceTablero() {
    $("#enlaceTablero").html('https://espanol.yahoo.com/a');
}

function InicializarMuestrarioTablero() {
    EstablecerTituloTablero();
    EstablecerEnlaceTablero();
}

function SiActivoVacioPorDefaultTrue(campoContenedor) {
    if (campoContenedor.val() == "")
        return true;
}

function LlenarElementoTableroTemporal() {
    
    ValidarCamposInformacion();

    elementoTableroTemporal = {
        "Id": idRegistroOperacionTemporal,
        "NombreCorto": $("#txtNombreCortoAdm").val(),
        "Descripcion": $("#txtDescripcionAdm").val(),
        "Liga": $("#txtLigaAdm").val(),
        "Activo": SiActivoVacioPorDefaultTrue($("#txtActivoAdm"))
    };

    $("#" + CONTENEDOR_ASPNET + "hidden_JSONTableroTemporal").val(JSON.stringify(elementoTableroTemporal));
}

function LlenarSelectUsuarios() {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(objetoJSONUsuarios, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        var ApellidosTemporal = "";
        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                IdTemporal = value;

            if (key == "Nombre")
                NombreTemporal = value;

            if (key == "Apellidos")
                ApellidosTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + " " + ApellidosTemporal + "</option>");

    });

    $("#usuariosSelect").html(elementosSelectGeneral.join(""));
    $("#usuariosSelectAdm").html(elementosSelectGeneral.join(""));
}

function ObtenerDatosPorId(idRegistroBuscado) {
    $.each(objetoJSONTableros, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idRegistroBuscado)
                    ObtenerRegistroCompleto(item);

        });
    });
}

function LimpiarCamposAdministracion() {

    $("#txtNombreCortoAdm").val("");
    $("#txtDescripcionAdm").val("");
    $("#txtLigaAdm").val("");
    
}

function ObtenerRegistroCompleto(elemento) {
    
    idRegistroOperacionTemporal = elemento.Id;
    $("#txtNombreCortoAdm").val(elemento.NombreCorto);
    $("#txtDescripcionAdm").val(elemento.Descripcion);
    $("#txtLigaAdm").val(elemento.Liga);
    
    $("#txtActivoAdm").val(elemento.Activo);

    $("#cbActivoAdm").prop('checked', ObtenerBooleanoDeCadena(elemento.Activo));
}

function ConsumirObjetoJSON() {

    var elementosTabla = [];

    $.each(objetoJSONTableros, function (key, value) {
        item = value;

        elementosTabla.push("<tr>");

        var IdTemporal = "";
        var NombreCortoTemporal = "";
        var DescripcionTemporal = "";
        var LigaTemporal = "";
        var ActivoTemporal = "";
        
        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "NombreCorto")
                NombreCortoTemporal = value;

            if (key == "Descripcion")
                DescripcionTemporal = value;

            if (key == "Liga")
                LigaTemporal = value;

            if (key == "Activo")
                ActivoTemporal = value;

        });
        
        elementosTabla.push("<td>" + IdTemporal + "</td>");
        elementosTabla.push("<td>" + NombreCortoTemporal + "</td>");
        elementosTabla.push("<td>" + DescripcionTemporal + "</td>");
        elementosTabla.push("<td>" + LigaTemporal + "</td>");
        elementosTabla.push("<td>" + ActivoTemporal + "</td>");
        elementosTabla.push("<td>");
        elementosTabla.push("<a data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEliminarRegistro' href='#' role='button'><i class='fa fa-trash'></i></a>");
        elementosTabla.push("&nbsp;&nbsp;&nbsp;");
        elementosTabla.push("<a data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEditarRegistro' href='#' role='button'><i class='fa fa-pencil'></i></a>");
        elementosTabla.push("</td>");
        elementosTabla.push("</tr>");

    });

    return elementosTabla.join("");
}

function AsignarComportamientoBotonAgregarRegistro() {
    $("#btnAgregar").on("click", function () {
        LimpiarCamposAdministracion();

        $("#seccionAdministraDatos").show();
        $("#seccionConsultaDatos").hide();
        
        $(".OcultarPorInsercion").css("visibility", "hidden");

        MostrarBotonInsercionNuevoRegistro(true);

    });
}

function DesignarImagenesGaleria(idTablero) {

    console.log("DesignarImagenesGaleria(idTablero) -->" + idTablero);

    $("#IframeObtenedorImagenesPorTablero").attr("src", "");
    setTimeout(
        function () {
            $("#IframeObtenedorImagenesPorTablero").attr("src", "ObtenedorImagenesPorTablero.aspx?IdTablero=" + idTablero);
        }, 500
    );

    setTimeout(
        function () {
            var arregloImagenesCookieRecibido = JSON.parse($.cookie('arregloImagenesCookie'));
            AlimentarCargarImagenesPorTablero(arregloImagenesCookieRecibido);
        }, 2000
    );
}

function MostrarseccionConsultaDatos(idTableroTemporal) {

    

    DesignarImagenesGaleria(idTableroTemporal);
    ObtenerDatosPorId(idTableroTemporal);

    LlenarContenedorListaImagenesActual();

    $("#seccionAdministraDatos").show();
    $("#seccionConsultaDatos").hide();
    MostrarBotonInsercionNuevoRegistro(false);
    $(".OcultarPorInsercion").css("visibility", "visible");
}

function AsignarComportamientoBotonEditarRegistro() {

    $('#table_id tbody').on('click', '.btnEditarRegistro', function () {

        $.cookie('idTableroTemporal', $(this).attr("data-IdRegistro"));

        MostrarseccionConsultaDatos($.cookie('idTableroTemporal'));
        
    });
}

function AsignarComportamientoCambioSelectTableros() {
    AsignarComportamientoCambioSelectGeneralTexto("TablerosSelect", "txtIdTablero");
    AsignarComportamientoCambioSelectGeneralId("TablerosSelectAdm", "txtIdTableroAdm");

    AsignarComportamientoCambioSelectGeneralHTML("TablerosSelectAdm", "tituloTablero", "text");
    AsignarComportamientoCambioSelectGeneralHTML("TablerosSelectAdm", "enlaceTablero", "url");

    AsignarComportamientoCambioSelectGaleria("TablerosSelectAdm", "contenedorImagenesTableros");
}

function AsignarComportamientoCambioCheckBoxGeneral(nombreCampo) {
    $('#' + nombreCampo).on('change', function () {
        $("#txtActivoAdm").val(ObtenerValorActivo($(this)));
    });
}

function EstablecerObjetoJSONTableros() {
    EstablecerObjetoJSONGeneral("contenedorJSONTableros", "objetoJSONTableros");
}
//funciones ejecutan procesos
function BorrarRegistroSeleccionado() {
    $("#" + CONTENEDOR_ASPNET + "hidden_idTablero").val(idRegistroOperacionTemporal);
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnBorrarTablero").click();
        }, 500
    );

}

function ActualizarRegistroSeleccionado() {
    LlenarElementoTableroTemporal();
    setTimeout(
        function () {
            console.log("se supone que debo ejecutar la actuializacion del lado server");
            $("#" + CONTENEDOR_ASPNET + "btnActualizarTablero").click();
        }, 500
    );
}

function InsertarRegistroSeleccionado() {
    idRegistroOperacionTemporal = 0;
    $("#txtIdUsuarioCreoAdm").val(idUsuarioLogueado);//poner el id del usuario de la sesion actual
    $("#txtActivoAdm").val("True");
    LlenarElementoTableroTemporal();
    setTimeout(
        function () {
            console.log("se supone que debo ejecutar la insercion del lado server");
            $("#" + CONTENEDOR_ASPNET + "btnInsertarTablero").click();
        }, 500
    );
}
//fin de funciones ejecutan procesos

var InicializarBusquedaAjenaTabla = function () {
}

function ProcesosUnaSolaVezPorPantalla() {
    InicializarBusquedaAjenaTabla();
    InicializarSelect();
    
    LimpiarElementoTableroTemporal();
}

var InicializarTabla = function () {

    $('#table_id').DataTable({
        "columnDefs": [
            {
                "targets": [Columnas.Id, Columnas.Liga, Columnas.Activo],
                "visible": false
            }
        ]
    });
}

function TodosProcesosPorPantalla() {
    EstablecerObjetoJSONTableros();

    AsignarComportamientoCambioCheckBoxGeneral("cbActivoAdm");

    ReiniciarTablaDatos();

    LlenarDatosTabla();
    InicializarTabla();
    
    MostrarOcultarSeccionAdministraDatos();

    AsegurarMuestroCorrectoSeccionAdministraDatos();

    AsignarComportamientoBotonAgregarRegistro();
    AsignarComportamientoBotonCancelarAgregacionRegistro();
    AsignarComportamientoBotonGuardarRegistro();
    AsignarComportamientoBotonInsertarRegistro();
    AsignarComportamientoBotonEliminarRegistro();
    AsignarComportamientoBotonEditarRegistro();

    LlenarArregloImagenesTablero();
    //InicializarMuestrarioTablero();
}

var arregloImagenesCookie = [];
var arregloImagenesJSON = null;

function AlimentarArregloImagenesCookie(arregloImagenesRecibido) {
    arregloImagenesCookie = arregloImagenesRecibido;
}

function CreacionArregloImagenesJSON() {
    arregloImagenesJSON = JSON.stringify(arregloImagenesCookie);
}

function AsignacionCookie() {
    $.cookie('arregloImagenesCookie', arregloImagenesJSON);
}

function CargarIframeImagenesPorTablero() {
    $("#IframeImagenesPorTablero").attr("src", "ImagenesPorTablero.aspx");
}

var AlimentarCargarImagenesPorTablero = function (arregloImagenesRecibido) {
    AlimentarArregloImagenesCookie(arregloImagenesRecibido);
    CreacionArregloImagenesJSON();
    AsignacionCookie();
    CargarIframeImagenesPorTablero();
}

var AsignarComportamientoBotonCancelarAgregacionRegistro = function () {
    $("#btnCancelar").on("click", function () {
        SoloMostrarPanelBotoneraOpcionesGaleria();
        LimpiarCamposAdministracion();

        $("#seccionAdministraDatos").hide();
        $("#seccionConsultaDatos").show();

        RehabilitarBotonesEdicionEliminacionRegistros();
    });
}

function OcultarPanelEliminadorImagenesPorTablero() {
    $("#EliminadorImagenesPorTablero").hide();
}

function OcultarPanelAsignadorImagenesPorTablero() {
    $("#AsignadorImagenesPorTablero").hide();
}

function OcultarPanelBotoneraOpcionesGaleria() {
    $("#botoneraOpcionesGaleria").hide();
}

function MostrarPanelEliminadorImagenesPorTablero() {
    $("#EliminadorImagenesPorTablero").show();
}

function MostrarPanelAsignadorImagenesPorTablero() {
    $("#AsignadorImagenesPorTablero").show();
}

function MostrarPanelBotoneraOpcionesGaleria() {
    $("#botoneraOpcionesGaleria").show();
}

function AsignarComportamientoBotonEnlaceSubirImagenes() {
    $("#enlaceSubirImagenes").on("click", function () {
        MostrarPanelAsignadorImagenesPorTablero();
        OcultarPanelBotoneraOpcionesGaleria();
    });
}

function AsignarComportamientoBotonEnlaceEliminarImagenes() {
    $("#enlaceEliminarImagenes").on("click", function () {
        MostrarPanelEliminadorImagenesPorTablero();
        OcultarPanelBotoneraOpcionesGaleria();
        LlenarContenedorListaImagenesActual();
    });
}

function SoloMostrarPanelBotoneraOpcionesGaleria() {
    OcultarPanelEliminadorImagenesPorTablero();
    OcultarPanelAsignadorImagenesPorTablero();
    MostrarPanelBotoneraOpcionesGaleria();
}

function AsignarComportamientoBotonEnlaceCancelarOperacionGaleriaImagenes() {
    $(".cancelarOperacionGaleriaImagenes").on("click", function () {
        SoloMostrarPanelBotoneraOpcionesGaleria();
    });
}

function InicializarPanelesProcesosGaleriasImagenes() {
    OcultarPanelEliminadorImagenesPorTablero();
    OcultarPanelAsignadorImagenesPorTablero();
    MostrarPanelBotoneraOpcionesGaleria();
    AsignarComportamientoBotonEnlaceSubirImagenes();
    AsignarComportamientoBotonEnlaceEliminarImagenes();
    AsignarComportamientoBotonEnlaceCancelarOperacionGaleriaImagenes();
}


function ObtenerListaImagenesAsignadasAlTablero(idTablero) {
    $.ajax({
        url: "FileUploadHandler.ashx?ObtenerListaImagenesAsignadasAlTablero=" + idTablero,//+"DELIMITADOR",
    }).then(function (data) {
        var objetoJSONTemporal = JSON.parse(JSON.stringify(data));

        var listadoImagenesGeneral = [];

        var informacionContenidaJSON = JSON.parse(objetoJSONTemporal.InformacionContenida);

        $.each(informacionContenidaJSON, function (key, value) {
            item = value;

            var IdTemporal = "";
            var URLTemporal = "";
            $.each(item, function (key, value) {

                if (key == "Id")
                    IdTemporal = value;

                if (key == "URL")
                    URLTemporal = value;

            });

            listadoImagenesGeneral.push("<div nuevoidimagen='" + IdTemporal + "'>");
            listadoImagenesGeneral.push("<br>");
            listadoImagenesGeneral.push("<br>");
            listadoImagenesGeneral.push('<img src="' + URLTemporal + '" width="150" height="87">');
            listadoImagenesGeneral.push("&nbsp;&nbsp;&nbsp;");
            listadoImagenesGeneral.push("<a class='retirarImgActual' href='#'>Quitar esta imagen</a>");
            listadoImagenesGeneral.push("</div>");

        });

        $("#contenedorListaImagenesAEliminar").html(listadoImagenesGeneral.join(""));

        AsignarListenerBorrarImagenActual();
    });
}

function LlenarContenedorListaImagenesActual() {

    ObtenerListaImagenesAsignadasAlTablero($.cookie('idTableroTemporal'));//1);

}

function AsignarListenerBorrarImagenActual() {
    $(".retirarImgActual").on("click", function () {
        var elementoABorrar = $(this).parent();
        MostrarMensajeBorradoImagenActual(elementoABorrar);

    });
}

function MostrarMensajeBorradoImagenActual(elementoABorrar) {
    swal({
        title: 'Esta Segur@ de querer eliminar esta imagen?',
        text: "No podra deshacer esta acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        ProcesarConfirmacionMensajeBorradoImagenActual(result, elementoABorrar);
    })
}

function ProcesarConfirmacionMensajeBorradoImagenActual(resultado, elementoABorrar) {
    if (resultado.value) {
        var idImagen = elementoABorrar.attr("nuevoidimagen");

        RemoverImagenARelacionarConElTableroActual(idImagen);

        elementoABorrar.remove();
        MostrarseccionConsultaDatos($.cookie('idTableroTemporal'));
        MostrarAlertaGeneralImagenActual('Imagen Borrada', 'La Imagen ha sido borrada.', 'success');
    }
}

function RemoverImagenARelacionarConElTableroActual(idImagen) {

    $.ajax({
        url: "FileUploadHandler.ashx?EliminarRegistroImagenPorTableroPorId=" + idImagen,//+"DELIMITADOR",
    }).then(function (data) {
        //console.log("terminado 1");
    });

}

function MostrarAlertaGeneralImagenActual(titulo, descripcion, tipo) {
    swal(
        titulo, descripcion, tipo
    );
}

LlenarContenedorListaImagenesActual();

$(document).ready(function () {
    TodosProcesosPorPantalla();
    ProcesosUnaSolaVezPorPantalla();
    InicializarPanelesProcesosGaleriasImagenes();
});