﻿//cargar libreria responsable de cookies
/*
$.ajax({
    type: "GET",
    url: "../Scripts/Plugins/jquery.cookie.js",
    dataType: "script"
});
*/

var imageMenu = {
    id: 'imageMenu',
    xtype: 'image',
    src: 'Images/logo_transparent_80x37.png',
    height: 37,
    width: 80

};

var btnPageHome = {
    id: 'btnPageHome',
    xtype: 'button',
    text: 'Inicio',
    iconAlign: 'left',
    icon: 'Images/home_white_24x24.png',
    height: 35,
    width: 80,
    handler: function () {
        window.location = "Default.aspx";
    }
};

var btnVisualizador = {
    id: 'btnVisualizador',
    xtype: 'button',
    text: 'Vista General',
    iconAlign: 'left',
    icon: 'Images/earth_20x20.png',
    height: 35,
    width: 110,
    handler: function () {
        window.location = "Visualizador.aspx";
    }
};

var menuViajes = Ext.create('Ext.menu.Menu', {
    id: 'menuViajes'
});

/*nuevo 150618 */
/*
var listadoPaginasPermitidasAlUsuario = [
    "Default.aspx",
    "DashboardControl.aspx",
    "Visualizador.aspx",
];
*/
var agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = null;

function RevisarSiSeAgregaMenu(paginaParaRevisar) {
    for (var indice = 0; indice < listadoPaginasPermitidasAlUsuario.length; indice++)
        if (listadoPaginasPermitidasAlUsuario[indice] == paginaParaRevisar)
            return true;
    return false;
}

function ProcesoAgregacionMenu(paginaParaRevisar, funcionAgregacionMenu) {
    if (RevisarSiSeAgregaMenu(paginaParaRevisar))
        funcionAgregacionMenu();
}

/*fin de nuevo 150618 */

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({
        text: 'Dashboard de Control',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "DashboardControl.aspx";
        }
    })
};
ProcesoAgregacionMenu("DashboardControl.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({
        text: 'Control de Viajes',
        icon: 'Images/historico_gray_24x24.png',
        handler: function () {
            window.location = "ViajesRuta.aspx";
        }
    })
};
ProcesoAgregacionMenu("ViajesRuta.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({
        text: 'BackHaul',
        icon: 'Images/store_black_24x24.png',
        handler: function () {
            window.location = "ViajesBackhaul.aspx";
        }
    })
};
ProcesoAgregacionMenu("ViajesBackhaul.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);
/*
agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () { 
    menuViajes.add({
      text: 'Generación de Rutas',
      icon: 'Images/route_black_24x24.png',
      handler: function () {
        window.location = "ConfigRutas.aspx";
      }
    })
};
ProcesoAgregacionMenu("ConfigRutas.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () { 
    menuViajes.add({
      text: 'Rotación',
      icon: 'Images/informeViajes_white_24x24.png',
      handler: function () {
        window.location = "AsignacionRotacion.aspx";
      }
    })
};
ProcesoAgregacionMenu("AsignacionRotacion.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);
*/

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({  //xtype: 'button',
        id: 'menuViajesConfigViajes',
        text: 'Creación de Viajes',
        hidden: true,
        icon: 'Images/edittrip_black_26x26.png',
        handler: function () {
            window.location = "ConfigViajes.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigViajes.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({  //xtype: 'button',
        id: 'menuViajesConfigViajes_v2',
        text: 'Creación de Viajes',
        hidden: true,
        icon: 'Images/edittrip_black_26x26.png',
        handler: function () {
            window.location = "ConfigViajes_v2.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigViajes_v2.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({  //xtype: 'button',
        id: 'menuViajesConfigViajes_v3',
        text: 'Creación de Viajes otros destinos',
        hidden: true,
        icon: 'Images/edittrip_black_26x26.png',
        handler: function () {
            window.location = "ConfigViajes_v3.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigViajes_v3.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

var btnMenuViajes = {
    id: 'btnMenuViajes',
    xtype: 'button',
    text: 'Viajes',
    iconAlign: 'left',
    icon: 'Images/truck_gray_24x24.png',
    height: 35,
    width: 100,
    menu: menuViajes
};

var menuGPS = Ext.create('Ext.menu.Menu', {
    id: 'menuGPS'
});

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Visualización de Flota',
        icon: 'Images/monitoreoOnline_black_24x24.png',
        handler: function () {
            window.location = "MonitoreoOnline.aspx";
        }
    })
};
ProcesoAgregacionMenu("MonitoreoOnline.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Posiciones GPS',
        icon: 'Images/broadcast_black_24x24.png',
        handler: function () {
            window.location = "PosicionesGPS.aspx";
        }
    })
};
ProcesoAgregacionMenu("PosicionesGPS.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Reportabilidad',
        icon: 'Images/fronttruck_black_24x24.png',
        handler: function () {
            window.location = "ReportabilidadGPSTrack.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReportabilidadGPSTrack.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Estado Flota',
        icon: 'Images/fronttruck_black_24x24.png',
        handler: function () {
            window.location = "LastPositions.aspx";
        }
    })
};
ProcesoAgregacionMenu("LastPositions.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

var btnMenuGPS = {
    id: 'btnMenuGPS',
    xtype: 'button',
    text: 'GPS',
    iconAlign: 'left',
    icon: 'Images/gps_gray_24x24.png',
    height: 35,
    width: 90,
    menu: menuGPS
};

var menuReportes = Ext.create('Ext.menu.Menu', {
    id: 'menuReportes'
});

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Informe de Viajes',
        icon: 'Images/informeViajes_white_24x24.png',
        handler: function () {
            window.location = "InformeViajes.aspx";
        }
    })
};
ProcesoAgregacionMenu("InformeViajes.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Kms recorridos',
        icon: 'Images/road_black_24x25.png',
        handler: function () {
            window.location = "Rpt_KmsRecorridos.aspx";
        }
    })
};
ProcesoAgregacionMenu("Rpt_KmsRecorridos.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Temático de alertas',
        icon: 'Images/flag_black_24x24.png',
        handler: function () {
            window.location = "Rpt_Alertas.aspx";
        }
    })
};
ProcesoAgregacionMenu("Rpt_Alertas.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Tractos',
        icon: 'Images/truck_black_24x24.png',
        handler: function () {
            window.location = "ReporteEstadiaTractos.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaTractos.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Tractos Histórico',
        icon: 'Images/truck_black_24x24.png',
        handler: function () {
            window.location = "ReporteEstadiaTractos_Historico.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaTractos_Historico.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Remolques',
        icon: 'Images/trailer_black_26x26.png',
        handler: function () {
            window.location = "ReporteEstadiaRemolques.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaRemolques.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Remolques Histórico',
        icon: 'Images/trailer_black_26x26.png',
        handler: function () {
            window.location = "ReporteEstadiaRemolques_Historico.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaRemolques_Historico.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Reporte Comercial',
        icon: 'Images/informeViajes_white_24x24.png',
        handler: function () {
            window.location = "ReporteComercial.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteComercial.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

var btnMenuReportes = {
    id: 'btnMenuReportes',
    xtype: 'button',
    text: 'Reportes',
    iconAlign: 'left',
    icon: 'Images/reports_gray_24x24.png',
    height: 35,
    width: 100,
    menu: menuReportes
};

var menuConfiguracion = Ext.create('Ext.menu.Menu', {
    id: 'menuConfiguracion'
});

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Zonas',
        icon: 'Images/zona_gray_22x22.png',
        handler: function () {
            window.location = "ConfigZonas.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigZonas.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Usuarios',
        icon: 'Images/user_black_24x24.png',
        handler: function () {
            window.location = "ConfigUsuarios.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigUsuarios.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Páginas por perfil',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "PaginasPorPerfil.aspx";
        }
    })
};
ProcesoAgregacionMenu("PaginasPorPerfil.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Páginas por usuario',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "PaginasPorUsuario.aspx";
        }
    })
};
ProcesoAgregacionMenu("PaginasPorUsuario.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Tableros Por Perfil',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "TablerosPorPerfil.aspx";
        }
    })
};
ProcesoAgregacionMenu("TablerosPorPerfil.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Tableros Por Usuario',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "TablerosPorUsuario.aspx";
        }
    })
};
ProcesoAgregacionMenu("TablerosPorUsuario.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Tableros',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "Tableros.aspx";
        }
    })
};
ProcesoAgregacionMenu("Tableros.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

var btnMenuConfig = {
    id: 'btnMenuConfig',
    xtype: 'button',
    text: 'Configuración',
    iconAlign: 'left',
    icon: 'Images/gear_white_24x24.png',
    height: 35,
    width: 120,
    menu: menuConfiguracion
};

var btnLogout = {
    id: 'btnLogout',
    xtype: 'button',
    text: 'Cerrar Sesión',
    iconAlign: 'left',
    height: 35,
    width: 120,
    icon: 'Images/logout_grey_24x24.png',
    handler: function () {
        window.location = "Login.aspx";
    }
};

var menuPaneles = Ext.create('Ext.menu.Menu', {
    id: 'menuPaneles'
});

agregacionTablerosDinamica();

//fin de hacer esta seccion dinamica
var btnMenuPaneles = {
    id: 'btnMenuPaneles',
    xtype: 'button',
    text: 'Paneles',
    iconAlign: 'left',
    icon: 'Images/control_black_24x24.png',
    height: 35,
    width: 120,
    menu: menuPaneles
};

var imageContacto = {
    id: 'imageContacto',
    xtype: 'image',
    src: 'Images/contacto.png',
    height: 37,
    width: 230
};

var viewWidth = Ext.getBody().getViewSize().width;
/*
var toolbarMenu = Ext.create('Ext.toolbar.Toolbar', {
  id: 'toolbarMenu',
  height: 40,
    items: [
        imageMenu,
        btnPageHome,
        { xtype: 'tbseparator' },
        btnVisualizador,
        { xtype: 'tbseparator' },
        btnMenuViajes,
        { xtype: 'tbseparator' },
        btnMenuGPS,
        { xtype: 'tbseparator' },
        btnMenuReportes,
        { xtype: 'tbseparator' },
        btnMenuConfig,
        { xtype: 'tbseparator' },
        btnLogout,
        { xtype: 'tbspacer', width: viewWidth - 1100 },
        imageContacto
    ]
});
*/
/*nuevo 150618*/

var listaPaginasBtnVisualizador = [
    "Visualizador.aspx"
];

var listaPaginasBtnMenuViajes = [
    "DashboardControl.aspx",
    "ViajesRuta.aspx",
    "ViajesBackhaul.aspx",
    "ConfigViajes.aspx",
    "ConfigViajes_v2.aspx",
    "ConfigViajes_v3.aspx"
];

var listaPaginasBtnMenuGPS = [
    "MonitoreoOnline.aspx",
    "PosicionesGPS.aspx",
    "ReportabilidadGPSTrack.aspx",
    "LastPositions.aspx"
];

var listaPaginasBtnMenuReportes = [
    "InformeViajes.aspx",
    "Rpt_KmsRecorridos.aspx",
    "Rpt_Alertas.aspx",
    "ReporteEstadiaTractos.aspx",
    "ReporteEstadiaTractos_Historico.aspx",
    "ReporteEstadiaRemolques.aspx",
    "ReporteEstadiaRemolques_Historico.aspx",
    "ReporteComercial.aspx"
];

var listaPaginasBtnMenuConfig = [
    "ConfigZonas.aspx",
    "ConfigUsuarios.aspx",
    "PaginasPorPerfil.aspx",
    "PaginasPorUsuario.aspx"
];

var listaPaginasBtnMenuPaneles = [
    "PanelExterno.aspx"
];

var botonesToolBarMenu = [];
function armarBotonesToolBarMenu() {
    botonesToolBarMenu.push(imageMenu);
    botonesToolBarMenu.push(btnPageHome);
    botonesToolBarMenu.push({ xtype: 'tbseparator' });
    RevisarSiSeAgregaElBoton(btnVisualizador, listaPaginasBtnVisualizador);
    RevisarSiSeAgregaElSeparador(btnVisualizador, listaPaginasBtnVisualizador);
    RevisarSiSeAgregaElBoton(btnMenuViajes, listaPaginasBtnMenuViajes);
    RevisarSiSeAgregaElSeparador(btnMenuViajes, listaPaginasBtnMenuViajes);
    RevisarSiSeAgregaElBoton(btnMenuGPS, listaPaginasBtnMenuGPS);
    RevisarSiSeAgregaElSeparador(btnMenuGPS, listaPaginasBtnMenuGPS);
    RevisarSiSeAgregaElBoton(btnMenuReportes, listaPaginasBtnMenuReportes);
    RevisarSiSeAgregaElSeparador(btnMenuReportes, listaPaginasBtnMenuReportes);

    //nuevas lineas para menu paneles
    RevisarSiSeAgregaElBoton(btnMenuPaneles, listaPaginasBtnMenuPaneles);
    RevisarSiSeAgregaElSeparador(btnMenuPaneles, listaPaginasBtnMenuPaneles);
    //fin nuevas lineas para menu paneles

    RevisarSiSeAgregaElBoton(btnMenuConfig, listaPaginasBtnMenuConfig);
    RevisarSiSeAgregaElSeparador(btnMenuConfig, listaPaginasBtnMenuConfig);
    

    botonesToolBarMenu.push(btnLogout);
    botonesToolBarMenu.push({ xtype: 'tbspacer', width: viewWidth - 1100 });
    botonesToolBarMenu.push(imageContacto);

    setTimeout(function () {
        ReubicarImagenContacto();
    }, 500);
    
}
armarBotonesToolBarMenu();

function ReubicarImagenContacto() {
    var anchuraImagenContacto = 230;
    var idBuscado = $("[src='Images/contacto.png']").attr('id');
    var windowWidth = $(window).width();
    var pocisionIdeal = windowWidth - anchuraImagenContacto;

    $("#" + idBuscado).parent().css({ position: 'relative' });
    $("#" + idBuscado).css({ left: pocisionIdeal, position: 'absolute' });
}

function RevisarSiSeAgregaElBoton(botonParaAgregar, listaPaginasSubMenu) {
    for (var indice = 0; indice < listadoPaginasPermitidasAlUsuario.length; indice++)
        for (var indiceListaPaginas = 0; indiceListaPaginas < listaPaginasSubMenu.length; indiceListaPaginas++)
            if (listadoPaginasPermitidasAlUsuario[indice] == listaPaginasSubMenu[indiceListaPaginas]) {
                botonesToolBarMenu.push(botonParaAgregar);
                return;
            }
}

function RevisarSiSeAgregaElSeparador(botonParaAgregar, listaPaginasSubMenu) {
    var seDebeAgregarSeparador = false;

    for (var indice = 0; indice < listadoPaginasPermitidasAlUsuario.length; indice++)
        for (var indiceListaPaginas = 0; indiceListaPaginas < listaPaginasSubMenu.length; indiceListaPaginas++)
            if (listadoPaginasPermitidasAlUsuario[indice] == listaPaginasSubMenu[indiceListaPaginas])
                seDebeAgregarSeparador = true;

    if (seDebeAgregarSeparador)
        botonesToolBarMenu.push({ xtype: 'tbseparator' });
};

/*fin nuevo 150618 */

var toolbarMenu = Ext.create('Ext.toolbar.Toolbar', {
    id: 'toolbarMenu',
    height: 40,
    items: [
        botonesToolBarMenu
    ]
});

var topMenu = new Ext.FormPanel({
    id: 'topMenu',
    region: 'north',
    border: true,
    height: 40,
    tbar: toolbarMenu
});

//var almacenadorMenuViajes;

//CODIGO PARA EVITAR QUE EL MENU DE VIAJES SE OCULTE
$(document).ready(function () {

    //$.cookie('hola', "hola miguel");
    //console.log("hola:-->" + $.cookie('hola'));

    //setTimeout(function () {
    /*
        var pathname = window.location.pathname;

        //console.log("pathname: " + pathname);

    $("#btnMenuViajes").on("click mouseover", function () {
        //if (typeof $.cookie('almacenadorMenuViajes') == "undefined") {//if (typeof $.session.get('almacenadorMenuViajes') == "undefined") {

            //console.log('click menuviaje');
            //almacenadorMenuViajes = $("#menuViajes").html();
            if (pathname.indexOf("Default.aspx") > -1) {

                //console.log("contenido --> " + $("#menuViajes").html());

                $.cookie('almacenadorMenuViajes', $("#menuViajes").html());
            }
            //$.session.set('almacenadorMenuViajes', $("#menuViajes").html());//almacenadorMenuViajes = $("#menuViajes").html();

            //console.log('ya hay algo en  almacenadorMenuViajes 1');

        //} else {
            //console.log('ya hay algo en  almacenadorMenuViajes');
        //}

        //console.log("almacenadorMenuViajes:-->" + $.cookie('almacenadorMenuViajes'));//console.log("almacenadorMenuViajes:-->" + $.session.get('almacenadorMenuViajes') )

        $("#menuViajes").html($.cookie('almacenadorMenuViajes'));
        $("#menuViajes").attr("style", "visibility: visible; position: absolute; left: 293px; top: 39px; z-index: 19011; width: 200px;height: 150px;");

        $("#menuViajes").children().attr("style", "height: 201px;");
        $('#menuViajes').find('.x-vertical-box-overflow-body:first').attr("style", "height: 201px;");

        //$("#menuitem-1014").attr("style", "z-index: 19012;");
        //$("#menuitem-1015").attr("style", "z-index: 19013;");
    });

    */
    //}, 5000);

    //$("#btnMenuViajes").on("click mouseover", function () {

    

    

    
    //console.log("almacenadorMenuViajes: ");
    //console.log(almacenadorMenuViajes);
    
    
        /*
        $("#menuViajes").attr("style", "visibility: visible; position: absolute; left: 293px; top: 39px; z-index: 19011; width: 200px;height: 150px;");
        
        $("#menuViajes").children().attr("style", "height: 201px;");
        $('#menuViajes').find('.x-vertical-box-overflow-body:first').attr("style", "height: 201px;");
        $("#menuViajesConfigViajes_v2").attr("style", "margin: 0px; width: 144px; height: 27px; left: 0px; top: 78px;");
        $("#menuViajesConfigViajes_v2").children().attr("style", "height: 181px;");
        */

        //var pocisionMenuViajes = $("#btnMenuViajes").position();
        //$("#menuViajes").attr("style", "visibility: visible; position: absolute; left: " + pocisionMenuViajes.left + "px; top: 39px;  z-index: 19011; ");//height: 110px; width: 180px;

        //$("#menuViajes").children().attr("style", "height: 181px;");
        //$('#menuViajes').find('.x-vertical-box-overflow-body:first').attr("style", "height: 181px;");

        //console.log("altura pocisionMenuViajes : " + pocisionMenuViajes.height);

        //$("#menuViajesConfigViajes_v2").attr("style", "margin: 0px; width: 144px; height: 27px; left: 0px; z-index: 99999 !important; top:79px;   position: relative!important;");
        //$("#menuViajesConfigViajes_v2").children().attr("style", "height: 181px;");

        //$("#menuViajes").html($.cookie('almacenadorMenuViajes'));//$("#menuViajes").html($.session.get('almacenadorMenuViajes'));//almacenadorMenuViajes);
    //});
    
    /*
    $("#btnMenuViajes").on("mouseout", function () {
        $("#menuViajes").attr("style", "visibility: hidden;");
    });
    */
});
//ESTO OCURRE PORQUE LOS ESTILOS CSS QUE CARGA ESTA SECCION ENTRAN EN CONFLICTO CON LOS DEL MENU