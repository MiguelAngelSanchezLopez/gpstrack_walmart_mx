﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Track_Web._Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>

<script type="text/javascript">

Ext.onReady(function () {

    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    if (Ext.isIE) { Ext.enableGarbageCollector = false; }

    Ext.Ajax.timeout = 3600000;
    Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
    Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
    Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

    //Verifica si se debe controlar tiempo de expiración de sesión
    Ext.Ajax.request({
        url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
        success: function (data, success) {
            if (data != null) {
                data = Ext.decode(data.responseText);

                if (data > 0) {
                    Ext.ns('App');

                    //Session timeout in secons     
                    App.SESSION_TIMEOUT = data;

                    // Helper that converts minutes to milliseconds.
                    App.toMilliseconds = function (minutes) {
                        return minutes * 60 * 1000;
                    }

                    // Notifies user that her session has timed out.
                    App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                        Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                        Ext.MessageBox.show({
                            title: "Sesión expirada.",
                            msg: "Su sesión ha expirado.",
                            icon: Ext.MessageBox.WARNING,
                            buttons: Ext.MessageBox.OK,
                            fn: function () {
                                window.location = "Login.aspx";
                            }
                        });

                    });

                    // Starts the session timeout workflow after an AJAX request completes.
                    Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                        // Reset the client-side session timeout timers.
                        App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                    });

                }
            }
        }
    })

    Ext.Ajax.request({
        url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
        success: function (data, success) {
            if (data != null) {
                data = Ext.decode(data.responseText);
                var i;
                for (i = 0; i < data.length; i++) {
                    if (data[i].MenuPadre == 0) {
                       /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                        toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                    }
                    else {
                        var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                        listmenu.items.get(data[i].IdJavaScript).show();
                    }
                }
            }
        }
    });

    var centerPanel = new Ext.FormPanel({
      id: 'centerPanel',
      region: 'center',
      border: true,
      margins: '0 5 0 0',
      anchor: '100% 100%',
      html: '<img src="Images/background_gray_1366x768.png" style="height:100%; width:100%"/>'
    });

    var viewport = Ext.create('Ext.container.Viewport', {
      layout: 'border',
      items: [topMenu, centerPanel]
    });

  });

</script>
</asp:Content>