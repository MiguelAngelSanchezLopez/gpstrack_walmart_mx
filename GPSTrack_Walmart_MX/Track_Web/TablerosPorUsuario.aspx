﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TablerosPorUsuario.aspx.cs" Inherits="Track_Web.TablerosPorUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <script src="Scripts/TopMenu.js" type="text/javascript"></script>

    <script type="text/javascript">

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.form.Field.prototype.msgTarget = 'side';
            if (Ext.isIE) { Ext.enableGarbageCollector = false; }

            Ext.Ajax.timeout = 3600000;
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            //Verifica si se debe controlar tiempo de expiración de sesión
            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);

                        if (data > 0) {
                            Ext.ns('App');

                            //Session timeout in secons     
                            App.SESSION_TIMEOUT = data;

                            // Helper that converts minutes to milliseconds.
                            App.toMilliseconds = function (minutes) {
                                return minutes * 60 * 1000;
                            }

                            // Notifies user that her session has timed out.
                            App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                                Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                                Ext.MessageBox.show({
                                    title: "Sesión expirada.",
                                    msg: "Su sesión ha expirado.",
                                    icon: Ext.MessageBox.WARNING,
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        window.location = "Login.aspx";
                                    }
                                });

                            });

                            // Starts the session timeout workflow after an AJAX request completes.
                            Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                                // Reset the client-side session timeout timers.
                                App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                            });

                        }
                    }
                }
            })

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                                /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                 toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                /*
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                                */
                            }
                        }
                    }
                }
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                region: 'center',
                border: true,
                margins: '0 5 0 0',
                anchor: '100% 100%',
                html: ''//'<img src="Images/background_gray_1366x768.png" style="height:100%; width:100%"/>'
            });

            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, centerPanel]
            });

        });

    </script>


    <script type="text/javascript" src="Scripts/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap.js"></script>
    <script type="text/javascript" src="Scripts/Plugins/js/jquery.dataTables.js"></script>

    <script type="text/javascript" src="Scripts/Plugins/js/sweetalert2.all.js"></script>
    <!--
    <link href="Scripts/bootstrap.css" rel="stylesheet" />
    -->
    <link href="Scripts/Plugins/css/jquery.dataTables.css" rel="stylesheet" />
    <!--
    <link href="Scripts/Plugins/css/dataTables.bootstrap.css" rel="stylesheet" />
    -->
    <link rel="stylesheet" href="Scripts/Plugins/css/font-awesome.min.css">
    <link rel="stylesheet" href="Scripts/Plugins/css/font-awesome.min.css">

    <link href="Scripts/Plugins/css/select2.min.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/Plugins/js/select2.min.js"></script>

    <link rel="stylesheet" href="Scripts/Plugins/css/jquery-ui.css" />

    <script type="text/javascript" src="Scripts/Plugins/js/jquery-ui.js"></script>
    <script type="text/javascript" src="Scripts/funcionesControladores/Utilidades.js"></script>
    <script type="text/javascript" src="Scripts/funcionesControladores/funcionesGeneralesAdministrador.js"></script>

    <input type="text" runat="server" id="contenedorJSONTablerosPorUsuario" disabled="disabled" style="display: none;"  /><!--disabled="disabled" style="display: none;"-->
    <input type="text" runat="server" id="contenedorJSONPerfilesPorUsuario" disabled="disabled" style="display: none;" /><!---->
    <input type="text" runat="server" id="contenedorJSONUsuarios" disabled="disabled" style="display: none;" />
    <input type="text" runat="server" id="contenedorJSONPerfiles" disabled="disabled" style="display: none;" />
    <input type="text" runat="server" id="contenedorJSONTableros" disabled="disabled" style="display: none;" />
    <input type="hidden" runat="server" id="hidden_idTemporalTableroPorUsuario" />
    <input type="hidden" runat="server" id="hidden_JSONTableroPorUsuarioTemporal" />

    <input type="hidden" runat="server" id="hidden_IdsTablerosSeleccionadas" />
    <input type="hidden" runat="server" id="hidden_IdsPerfilesSeleccionados" />
    <input type="hidden" runat="server" id="hidden_IdsUsuariosSeleccionados" />

    <asp:Button ID="btnBorrarTablerosPorUsuario" OnClick="btnBorrarTablerosPorUsuario_Click" runat="server" Text="Borrar" Style="display: none;" />
    <asp:Button ID="btnActualizarTablerosPorUsuario" OnClick="btnActualizarTablerosPorUsuario_Click" runat="server" Text="Actualizar" Style="display: none;" />
    <asp:Button ID="btnInsertarTablerosPorUsuario" OnClick="btnInsertarTablerosPorUsuario_Click" runat="server" Text="Insertar" Style="display: none;" />

    <asp:Button ID="btnInsertarRegistrosTablerosPerfilesUsuario" OnClick="btnInsertarRegistrosTablerosPerfilesUsuario_Click" runat="server" Text="Insertar" Style="display: none;" />
    <br />
    <br />
    <div id="seccionFiltros">
        <div class="row">
            <div class="col-sm-12">
                <br />
            </div>
        </div>


        <div class="row" style="display: none;">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>Id:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtId" />
            </div>

            <div class="col-sm-2">
                <b>Tablero:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdTablero" />
            </div>

            <div class="col-sm-1">
            </div>

        </div>

        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
            </div>
            <div class="col-sm-3">
            </div>

            <div class="col-sm-2">
            </div>
            <div class="col-sm-3">
            </div>



            <div class="col-sm-1">
            </div>

        </div>
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-2">
                <b>Perfil:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdPerfil" />
            </div>

            <div class="col-sm-2">
            </div>
            <div class="col-sm-3">
            </div>

            <div class="col-sm-1">
            </div>

        </div>

        <table style="width: 100%">
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Tablero:</b>
                </td>
                <td>
                    <select class="selectDinamico" id="TablerosSelect" name="TablerosSelect" style="width: 100%;">
                    </select>
                </td>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Perfil:</b>
                </td>
                <td>
                    <select class="selectDinamico" id="perfilesSelect" name="perfilesSelect" style="width: 100%;">
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Usuario:</b>
                </td>
                <td>
                    <select class="selectDinamico" id="usuariosSelect" name="usuariosSelect" style="width: 100%;">
                    </select>
                </td>
                <td>&nbsp;&nbsp;
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>

        <div class="row">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
            </div>
            <div class="col-sm-3">
            </div>

            <div class="col-sm-1">
            </div>

        </div>

        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-2">
                <b>Usuario:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdUsuario" />
            </div>


        </div>

        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-9">
            </div>

            <div class="col-sm-1">
                <input type="button" id="btnLimpiar" value="Limpiar" />
            </div>

            <div class="col-sm-1">
            </div>

        </div>
        <hr />
    </div>
    <div id="seccionConsultaDatos">
        <div class="row">

            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <table id="table_id" class="display">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Usuario</th>
                            <th>Tablero</th>
                            <th>Perfil</th>
                            <th>Fecha Creacion</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="datosTabla">
                    </tbody>
                </table>
            </div>
            <div class="col-sm-1">
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <br />
            </div>
        </div>
        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-8">
            </div>

            <div class="col-sm-2">
                <input type="button" id="btnAgregar" value="Agregar Registro" />
            </div>

            <div class="col-sm-1">
            </div>

        </div>
    </div>
    <div id="seccionAdministraDatos">
        <br />
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>Id:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>IdPerfil:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdPerfilAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>IdTablero:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdTableroAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>IdUsuario:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdUsuarioAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <table style="width: 100%">
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Perfil:</b>
                </td>
                <td>
                    <select class="selectDinamico" id="perfilesSelectAdm" name="perfilesSelectAdm" style="width: 100%;" multiple="multiple">
                        <!--disabled="disabled"-->
                    </select>
                </td>
                <td>&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Tablero:</b>
                </td>
                <td>
                    <select class="selectDinamico" id="TablerosSelectAdm" name="TablerosSelectAdm" style="width: 100%;" multiple="multiple">
                    </select>
                </td>
                <td>&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Usuario:</b>
                </td>
                <td>
                    <select class="selectDinamico" id="usuariosSelectAdm" name="usuariosSelectAdm" style="width: 100%;" disabled="disabled">
                        <!---->
                    </select>
                </td>
                <td>&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <input type="button" id="btnCancelar" value="Cancelar" />
                </td>
                <td>
                    <input type="button" id="btnGuardar" value="Actualizar" />
                    <input type="button" id="btnInsertar" value="Insertar" />
                </td>
                <td>&nbsp;&nbsp;
                </td>
            </tr>
        </table>

    </div>
    <script type="text/javascript" src="Scripts/funcionesControladores/funcionesTableroPorUsuario.js"></script>

    <style type="text/css">
        .x-box-inner {
            position: static;
            height: 62px;
        }

        #centerPanel {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body2" runat="server">
</asp:Content>
