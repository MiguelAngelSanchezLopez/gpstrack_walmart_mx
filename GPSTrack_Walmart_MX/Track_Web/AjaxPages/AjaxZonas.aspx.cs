﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;
using System.Configuration;

using System.IO;

namespace Track_Web.AjaxPages
{
    public partial class AjaxZonas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (Request.QueryString["Metodo"])
            {
                case "GetTipoZonas":
                    GetTipoZonas();
                    break;
                case "GetZonas":
                    GetZonas();
                    break;
                case "GetZonasNuevaSeparacion":
                    GetZonasNuevaSeparacion();
                    break;
                case "EliminaZona":
                    EliminaZona(Request.Form["idZona"].ToString());
                    break;
                case "GetVerticesZona":
                    GetVerticesZona(Request.Form["IdZona"].ToString());
                    break;
                case "GetAllVerticesZona":
                    GetAllVerticesZona(Request.Form["IdZona"].ToString());
                    break;
                case "ValidarIdZona":
                    ValidarIdZona(Request.Form["IdZona"].ToString());
                    break;
                case "NuevaZona":
                    NuevaZona(Request.Form["IdZona"].ToString(), Request.Form["NombreZona"].ToString(), Request.Form["IdTipoZona"].ToString(), Request.Form["Vertices"].Split(','));
                    break;
                case "EditarZona":
                    EditarZona(Request.Form["IdZona"].ToString(), Request.Form["NombreZona"].ToString(), Request.Form["IdTipoZona"].ToString(), Request.Form["Vertices"].Split(','));
                    break;
                case "GetZonasToDraw":
                    GetZonasToDraw();
                    break;
                case "GetZonasToDrawModuloMapa":
                    GetZonasToDrawModuloMapa();
                    break;
                case "GetZonasToDrawFromCenter":
                    GetZonasToDrawFromCenter();
                    break;
                case "GetLocalesBackhaul":
                    GetLocalesBackhaul();
                    break;
                case "getStatesMexico":
                    getStatesMexico();
                    break;
                case "getTiendas":
                    getTiendas();
                    break;

            }
        }

        public void GetTipoZonas()
        {
            string _todos = "" + Request.QueryString["Todos"];
            string _result = "";
            Methods_Zonas _obj = new Methods_Zonas();

            if (_todos == "True")
            {
                var _list = (from i in _obj.GetTipoZonas(true)
                             select new
                             {
                                 i.IdTipoZona,
                                 i.NombreTipoZona
                             }).ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetTipoZonas()
                             select new
                             {
                                 i.IdTipoZona,
                                 i.NombreTipoZona
                             }).ToList();
                _result = JsonConvert.SerializeObject(_list);
            }


            Response.Write(_result);
        }
        //
        public void GetZonasNuevaSeparacion()
        {
            string idTipoZona = "" + Request.QueryString["idTipoZona"];
            string nombreZona = "" + Request.QueryString["nombreZona"];
            string EstadoMX = "" + Request.QueryString["EstadoMx"];

            int _idTipoZona;
            int _estadomx;
            int.TryParse(idTipoZona, out _idTipoZona);
            int.TryParse(EstadoMX, out _estadomx);

            Methods_Zonas _obj = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_obj.GetZonasNuevaSeparacion(_idTipoZona, nombreZona, _estadomx));

            Response.Write(_response);
        }

        public void GetZonas()
        {
            string idTipoZona = "" + Request.QueryString["idTipoZona"];
            string nombreZona = "" + Request.QueryString["nombreZona"];
            string EstadoMX = "" + Request.QueryString["EstadoMx"];

            int _idTipoZona;
            int _estadomx;
            int.TryParse(idTipoZona, out _idTipoZona);
            int.TryParse(EstadoMX, out _estadomx);

            Methods_Zonas _obj = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_obj.GetZonas(_idTipoZona, nombreZona, _estadomx));

            Response.Write(_response);
        }

        public void EliminaZona(string idZona)
        {
            string idusuario = Utilities.GetIdUsuarioSession(Session);
            int _idUsuario = 0;

            int.TryParse(idusuario, out _idUsuario);

            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            string _resp = _obj.EliminaZona(_idZona, _idUsuario);

            Response.Write(_resp);
        }

        public void GetVerticesZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            Track_Zonas _zona = _obj.GetZonaById(_idZona);

            var json = new
            {
                IdZona = _zona.IdZona,
                NombreZona = _zona.NombreZona,
                Latitud = _zona.Latitud,
                Longitud = _zona.Longitud,
                idTipoZona = _zona.IdTipoZona,
                Vertices = _obj.GetVerticesZona(_idZona)
            };

            string _response = JsonConvert.SerializeObject(json);

            Response.Write(_response);
        }

        public void GetAllVerticesZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            Track_Zonas _zona = _obj.GetZonaById(_idZona);

            var json = new
            {
                IdZona = _zona.IdZona,
                NombreZona = _zona.NombreZona,
                Latitud = _zona.Latitud,
                Longitud = _zona.Longitud,
                idTipoZona = _zona.IdTipoZona,
                Vertices = _obj.GetAllVerticesZona(_idZona)
            };

            string _response = JsonConvert.SerializeObject(json);

            Response.Write(_response);
        }

        public void ValidarIdZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            Response.Write(_obj.ValidarIdZona(_idZona).ToString());
        }

        public void NuevaZona(string idZona, string nombreZona, string idTipoZona, string[] vertices)
        {
            string idusuario = Utilities.GetIdUsuarioSession(Session);
            int _idUsuario = 0;

            int.TryParse(idusuario, out _idUsuario);

            int _idZona;
            int _idTipoZona;
            int.TryParse(idZona, out _idZona);
            int.TryParse(idTipoZona, out _idTipoZona);

            Methods_Zonas _obj = new Methods_Zonas();
            try
            {
                List<Tuple<decimal, decimal>> _vertices = new List<Tuple<decimal, decimal>>();
                for (int i = 0; i < vertices.Length; i++)
                {
                    string[] temp = vertices[i].Split(';');
                    Tuple<decimal, decimal> _ver;

                    string deployDestination = ConfigurationManager.AppSettings["DeployDestination"].ToString();

                    switch (deployDestination)
                    {
                        case "CL":
                            _ver = new Tuple<decimal, decimal>(decimal.Parse(temp[0].Replace('.', ',')), decimal.Parse(temp[1].Replace('.', ',')));
                            break;
                        case "US":
                            _ver = new Tuple<decimal, decimal>(Convert.ToDecimal(temp[0].Replace('.', ',')), Convert.ToDecimal(temp[1].Replace('.', ',')));
                            break;

                        //Valor por defecto CL      
                        default:
                            _ver = new Tuple<decimal, decimal>(decimal.Parse(temp[0].Replace('.', ',')), decimal.Parse(temp[1].Replace('.', ',')));
                            break;
                    }

                    _vertices.Add(_ver);
                }
                string _resp = _obj.NuevaZona(_idZona, nombreZona, _idTipoZona, _vertices, _idUsuario);

                Response.Write(_resp);
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void EditarZona(string idZona, string nombreZona, string idTipoZona, string[] vertices)
        {
            string idusuario = Utilities.GetIdUsuarioSession(Session);
            int _idUsuario = 0;

            int.TryParse(idusuario, out _idUsuario);

            int _idZona;
            int _idTipoZona;
            int.TryParse(idZona, out _idZona);
            int.TryParse(idTipoZona, out _idTipoZona);

            Methods_Zonas _obj = new Methods_Zonas();
            try
            {
                List<Tuple<decimal, decimal>> _vertices = new List<Tuple<decimal, decimal>>();
                for (int i = 0; i < vertices.Length; i++)
                {
                    string[] temp = vertices[i].Split(';');
                    Tuple<decimal, decimal> _ver;

                    string deployDestination = ConfigurationManager.AppSettings["DeployDestination"].ToString();

                    switch (deployDestination)
                    {
                        case "CL":
                            _ver = new Tuple<decimal, decimal>(decimal.Parse(temp[0].Replace('.', ',')), decimal.Parse(temp[1].Replace('.', ',')));
                            break;
                        case "US":
                            _ver = new Tuple<decimal, decimal>(Convert.ToDecimal(temp[0].Replace('.', ',')), Convert.ToDecimal(temp[1].Replace('.', ',')));
                            break;

                        //Valor por defecto CL      
                        default:
                            _ver = new Tuple<decimal, decimal>(decimal.Parse(temp[0].Replace('.', ',')), decimal.Parse(temp[1].Replace('.', ',')));
                            break;
                    }

                    _vertices.Add(_ver);
                }
                string _resp = _obj.EditarZona(_idZona, nombreZona, _idTipoZona, _vertices, _idUsuario);

                Response.Write(_resp);
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");
            }
        }

        public void GetZonasToDraw()
        {
            string fechaDesde = "" + Request.QueryString["fechaDesde"];
            string fechaHasta = "" + Request.QueryString["fechaHasta"];
            string patente1 = "" + Request.QueryString["patente1"];
            string patente2 = "" + Request.QueryString["patente2"];

            DateTime _fechaDesde;
            DateTime _fechaHasta;

            _fechaDesde = Utilities.convertDateIc(fechaDesde);// Convert.ToDateTime(fechaDesde);
            _fechaHasta = Utilities.convertDateIc(fechaHasta);// Convert.ToDateTime(fechaHasta);

            Methods_Zonas _objMethosZonas = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_objMethosZonas.GetZonasToDraw(_fechaDesde, _fechaHasta, patente1, patente2));

            Response.Write(_response);
        }

        public void GetZonasToDrawModuloMapa()
        {
            string idAlerta = "" + Request.QueryString["IdAlerta"];

            int _idAlerta;

            int.TryParse(idAlerta, out _idAlerta);

            Methods_Zonas _objMethosZonas = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_objMethosZonas.GetZonasToDrawModuloMapa(_idAlerta));

            Response.Write(_response);
        }

        public void GetZonasToDrawFromCenter()
        {
            string latitud = "" + Request.QueryString["latitud"];
            string longitud = "" + Request.QueryString["longitud"];

            string deployDestination = ConfigurationManager.AppSettings["DeployDestination"].ToString();

            latitud = latitud.Replace(',', '.');
            longitud = longitud.Replace(',', '.');

            Methods_Zonas _objMethosZonas = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_objMethosZonas.GetZonasToDrawFromCenter(latitud, longitud));

            Response.Write(_response);
        }

        public void GetLocalesBackhaul()
        {
            string idTipoZona = "" + Request.QueryString["idTipoZona"];
            string nombreZona = "" + Request.QueryString["nombreZona"];
            int _idTipoZona;

            int.TryParse(idTipoZona, out _idTipoZona);

            Methods_Zonas _obj = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_obj.GetLocalesBackhaul(_idTipoZona, nombreZona));

            Response.Write(_response);
        }

        public void getStatesMexico()
        {

            Methods_Zonas _obj = new Methods_Zonas();
            string _response = JsonConvert.SerializeObject(_obj.GetEstadosMexico());
            Response.Write(_response);
        }

        public void getTiendas()
        {
            Methods_Zonas _obj = new Methods_Zonas();
            string _response = JsonConvert.SerializeObject(_obj.GetTiendas());
            Response.Write(_response);
        }


    }
}