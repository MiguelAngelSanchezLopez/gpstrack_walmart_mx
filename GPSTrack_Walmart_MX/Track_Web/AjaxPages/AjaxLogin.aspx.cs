﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;
using System.Reflection;

using System.IO;

namespace Track_Web.AjaxPages
{
    public partial class AjaxLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (Request.QueryString["Metodo"])
            {
                case "ValidarUsuario":
                    ValidarUsuario(Request.Form["Usuario"].ToString(), Request.Form["Password"].ToString());
                    break;
                case "GetVersion":
                    GetVersion();
                    break;
                case "GetPerfilUsuarioConectado":
                    GetPerfilUsuarioConectado();
                    break;
                case "GetNameUsuarioConectado":
                    GetNameUsuarioConectado();
                    break;
                case "GetCedisAsociado":
                    GetCedisAsociado();
                    break;
                case "GetDeterminantesAsociados":
                    GetDeterminantesAsociados();
                    break;
                case "GetDiferenciaHoraria":
                    GetDiferenciaHoraria();
                    break;
                case "GetUsuarios":
                    GetUsuarios();
                    break;
                case "GuardarUsuario":
                    GuardarUsuario(Request.Form["idUsuario"].ToString(), Request.Form["userName"].ToString(), Request.Form["password"].ToString(), Request.Form["nombre"].ToString(), Request.Form["apellidos"].ToString(), Request.Form["eMail"].ToString(), Request.Form["agrupacion"].ToString(), Request.Form["activo"].ToString(), Request.Form["listCEDISAsociados"].ToString());
                    break;
                case "getTopMenu":
                    getTopMenu();
                    break;
                case "GetSessionExpirationTimeout":
                    GetSessionExpirationTimeout();
                    break;
                case "GrabarCierreSesion":
                    GrabarCierreSesion();
                    break;
            }


        }

        public void GrabarCierreSesion()
        {
            Methods_User _obj = new Methods_User();
            try
            {
                Session.Timeout = 1440;
                _obj.guardarlog(Convert.ToInt32(Session["idusuario"]), "GPSTrack. Logout");
                Response.Write("Ok");// "Sesión cerrada.");
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");
            }
        }

        public void ValidarUsuario(string usuario, string password)
        {
            Methods_User _obj = new Methods_User();
            try
            {
                string respuesta = _obj.ValidarUsuario(usuario, password).ToString();

                Session.Timeout = 1440;

                if (respuesta == "Ok")
                {
                    Track_Usuarios userlogado = _obj.getUsuarioConectado(usuario);

                    Session["userName"] = usuario;
                    Session["idusuario"] = userlogado.IdUsuario.ToString();

                    string transportista = _obj.getTransportista(usuario).ToString();

                    Session["transportista"] = transportista;

                    string perfilUsuarioConectado = _obj.getPerfilUsuarioConectado(usuario).ToString();
                    string nameUsuarioConectado = _obj.getNameUsuarioConectado(usuario).ToString();

                    Session["perfilUsuarioConectado"] = perfilUsuarioConectado;
                    Session["nameUsuarioConectado"] = nameUsuarioConectado;

                    int idRol = int.Parse(userlogado.IdRol.ToString()); //_obj.getidRol(usuario);
                    Session["idpefil"] = idRol.ToString();
                    Session["menu"] = JsonConvert.SerializeObject(_obj.getModulos(idRol));

                    _obj.guardarlog(userlogado.IdUsuario, "GPSTrack. Login");
                }

                Response.Write(respuesta);

            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void GetVersion()
        {
            string assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString() + '.' + Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString() + '.' + Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

            Response.Write(assemblyVersion);
        }

        public void GetPerfilUsuarioConectado()
        {
            string perfilUsuarioConectado = Session["perfilUsuarioConectado"].ToString();

            Response.Write(perfilUsuarioConectado);
        }

        public void GetNameUsuarioConectado()
        {
            string nameUsuarioConectado = Session["nameUsuarioConectado"].ToString();

            Response.Write(nameUsuarioConectado);
        }

        public void GetCedisAsociado()
        {
            Methods_User _obj = new Methods_User();

            string cedisAsociado = _obj.getCEDISAsociado(Session["userName"].ToString());

            Response.Write(cedisAsociado);
        }

        public void GetDeterminantesAsociados()
        {
            string idUsuario = "" + Request.QueryString["idUsuario"];

            int _idUsuario;

            int.TryParse(idUsuario, out _idUsuario);

            Methods_User _obj = new Methods_User();

            string _response = JsonConvert.SerializeObject(_obj.getDeterminantesAsociados(_idUsuario));

            Response.Write(_response);
        }

        public void GetDiferenciaHoraria()
        {
            Methods_User _obj = new Methods_User();

            string diferenciaHoraria = _obj.webDiferenciaHoraria().ToString(); ;

            Response.Write(diferenciaHoraria);
        }

        public void GetUsuarios()
        {
            string userName = Utilities.GetUsuarioSession(Session);

            Methods_User _objMethodsUser = new Methods_User();

            string _response = JsonConvert.SerializeObject(_objMethodsUser.GetUsuarios(userName));

            Response.Write(_response);
        }

        public void GuardarUsuario(string idUsuario, string userName, string password, string nombre, string apellidos, string eMail, string agrupacion, string activo, string listCEDISAsociados)
        {
            string idusuarioConectado = Utilities.GetIdUsuarioSession(Session);
            int _idUsuarioConectado = 0;

            int.TryParse(idusuarioConectado, out _idUsuarioConectado);

            int _idUsuario;
            int _activo;

            int.TryParse(idUsuario, out _idUsuario);

            if (activo == "true")
            {
                _activo = 1;
            }
            else
            {
                _activo = 0;
            }


            Methods_User _obj = new Methods_User();
            try
            {
                string _resp = _obj.GuardarUsuario(_idUsuario, userName, password, nombre, apellidos, eMail, agrupacion, _activo, listCEDISAsociados, _idUsuarioConectado);

                Response.Write(_resp);
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void getTopMenu()
        {
            try
            {
                Methods_User _obj = new Methods_User();
                string response = string.Empty;
                if (!string.IsNullOrEmpty(Session["menu"].ToString()))
                    response = Session["menu"].ToString();
                else
                {
                    Session["menu"] = JsonConvert.SerializeObject(_obj.getModulos(int.Parse(Session["idpefil"].ToString())));
                    response = Session["menu"].ToString();
                }
                Response.Write(response);

            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
            }
        }

        public void GetSessionExpirationTimeout()
        {
            Methods_User _obj = new Methods_User();
            try
            {
                string respuesta = _obj.SessionExpirationTimeout().ToString();

                Response.Write(respuesta);

            }
            catch (Exception)
            {
                Response.Write("0");

            }
        }

    }
}