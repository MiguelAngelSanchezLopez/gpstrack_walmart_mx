﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Web.SessionState;
using System.Web;
using System.Net;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace BusinessLayer
{
    public class Methods_User
    {
        private ModelEntities _context = new ModelEntities();

        /*funciones para validar si el usuario es de tipo backhaul */
        #region validarUsuarioTipoBackHaul

        protected SqlConnection conexionSQL;
        protected SqlCommand comandoSQL;
        protected SqlDataReader lectorDatosSQL;

        public string CADENA_CONEXION = ConfigurationManager.ConnectionStrings["ConexionPerfilamiento"].ConnectionString;

        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado()
        {
            comandoSQL.ExecuteNonQuery();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro, string descripcionTipo, string nombreParametro)
        {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo)
        {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo)
            {
                case "int":
                    tipoDatoDevolver = SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL()
        {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool EjecutarProcedimientoAlmacenadoRegresaBooleano() {
            int conteoUsuariosCoincidentes = Convert.ToInt32(comandoSQL.ExecuteScalar());
            return (conteoUsuariosCoincidentes > 0);
        }

        public bool ValidarUsuarioTipoBackHaul(string usuario, string password) {
            bool usuarioEsTipoBackHaul = false;
            try {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_ValidarUsuarioEsTipoBackhaul");

                EstablecerParametroProcedimientoAlmacenado(usuario, "string", "@usuario");
                EstablecerParametroProcedimientoAlmacenado(password, "string", "@password");

                usuarioEsTipoBackHaul=EjecutarProcedimientoAlmacenadoRegresaBooleano();

                conexionSQL.Close();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return usuarioEsTipoBackHaul;
        }

        public void EstablecerTipoBackHaulUsuario(string usuario, string password) {
            UsuarioBackHaulRegistrado.EstablecerTipoBackHaul(ValidarUsuarioTipoBackHaul(usuario, password));
        }

        #endregion validarUsuarioTipoBackHaul

        /*fin de funciones para validar si el usuario es de tipo backhaul */

        public string ValidarUsuario(string usuario, string password)
        {
            try
            {
                string userIpAddress = GetUserIP();

                if (userIpAddress == "::1")
                {
                    userIpAddress = "127.0.0.1";
                }

                try
                {
                    //validar si el usuario es de tipo BackHaul
                    EstablecerTipoBackHaulUsuario(usuario, password);
                }
                catch (Exception ex) {
                }
                

                List<Track_ValidarUsuario_Result> _result = _context.Track_ValidarUsuario(usuario, password, userIpAddress).ToList();
                return _result.FirstOrDefault().Respuesta;

            }
            catch (Exception e)
            {
                return "Se ha producido un error.";
            }
        }

        public string getTransportista(string usuario)
        {
            try
            {
                string transportista = "";

                int rolUsuario = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().IdRol;

                if (rolUsuario == 5) //Rol Transportista
                {
                    transportista = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Transportista;
                }

                return transportista;

            }
            catch (Exception)
            {
                return "";
            }
        }

        public int getPerfilUsuarioConectado(string usuario)
        {
            try
            {
                int perfil = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().IdRol;

                return perfil;

            }
            catch (Exception)
            {
                return 0;
            }
        }

        public string getNameUsuarioConectado(string usuario)
        {
            try
            {
                string fulName = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Nombre + " " + (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Apellidos;

                return fulName;

            }
            catch (Exception)
            {
                return "";
            }
        }

        public string getCEDISAsociado(string usuario)
        {
            try
            {
                string cedis = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().CEDIS.ToString();

                return cedis;

            }
            catch (Exception)
            {
                return "";
            }
        }

        public List<Track_GetDeterminantesAsociados_Result> getDeterminantesAsociados(int idUsuario)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

                List<Track_GetDeterminantesAsociados_Result> _list = _context.Track_GetDeterminantesAsociados(idUsuario).ToList();

                return _list;

            }
            catch (Exception)
            {
                return new List<Track_GetDeterminantesAsociados_Result>();
            }
        }

        public int webDiferenciaHoraria()
        {
            try
            {
                int webDiferenciaHoraria = Convert.ToInt32((from a in _context.Configuracion where a.Clave == "webDiferenciaHoraria" select a).FirstOrDefault().Valor);
                return webDiferenciaHoraria;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void CerrarSesion(string userName)
        {
            _context.Track_CerrarSesion(userName);
        }


        public List<Track_GetUsuarios_Result> GetUsuarios(string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetUsuarios_Result> _listUsuarios = _context.Track_GetUsuarios(userName).ToList();
                return _listUsuarios;

            }
            catch (Exception)
            {
                return new List<Track_GetUsuarios_Result>();
            }
        }

        public string GuardarUsuario(int idUsuario, string userName, string password, string nombre, string apellidos, string eMail, string agrupacion, int activo, string listCedisASociados, int idUsuarioConectado)
        {
            try
            {

                string _resp = _context.Track_GuardarUsuario(idUsuario, userName, password, nombre, apellidos, eMail, agrupacion, activo, listCedisASociados).FirstOrDefault().Response;

                Methods_User _user = new Methods_User();

                _user.guardarlog(idUsuarioConectado, "Creación / modificación de usuario : " + userName.ToString());

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public string GetUserIP()
        {

            string ipList = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public Track_Usuarios getUsuarioConectado(string pUsuario)
        {
            try
            {
                return (from a in _context.Track_Usuarios where a.Usuario == pUsuario select a).FirstOrDefault();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new Track_Usuarios();
            }
        }

        public void guardarlog(int idUsuario, string action)
        {
            try
            {
                string userIpAddress = GetUserIP();

                if (userIpAddress == "::1")
                {
                    userIpAddress = "127.0.0.1";
                }

                _context.Track_GuardarLog(idUsuario, userIpAddress, action);
            }
            catch (Exception ex)
            {

            }
        }

        public List<Track_Aplicacion_Modulos_Result> getModulos(int idPefil)
        {
            try
            {
                return _context.Track_Aplicacion_Modulos(idPefil).ToList();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<Track_Aplicacion_Modulos_Result>();
            }
        }

        public string SessionExpirationTimeout()
        {
            try
            {
                Configuracion _configuracion = (from c in _context.Configuracion where c.Clave == "Track_SessionTimeout" select c).FirstOrDefault();

                string _sessionExpirationStatus = _configuracion.Valor;

                return _sessionExpirationStatus;
            }
            catch (Exception e)
            {
                return "0";
            }
        }


    }
}
