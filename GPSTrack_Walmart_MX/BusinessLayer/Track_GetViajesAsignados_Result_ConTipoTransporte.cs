﻿using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Track_GetViajesAsignados_Result_ConTipoTransporte: Track_GetViajesAsignados_Result
    {
        //NUEVO CAMPO TIPO TRANSPORTE 040718 348PM
        public string TipoTransporte { get; set; }
    }
}
