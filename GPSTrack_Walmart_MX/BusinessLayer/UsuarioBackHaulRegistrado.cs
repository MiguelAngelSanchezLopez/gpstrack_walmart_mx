﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class UsuarioBackHaulRegistrado
    {
        private static UsuarioBackHaulRegistrado unicaInstancia = new UsuarioBackHaulRegistrado();
        private static Int32 idUsuario;
        private static bool tipoBackHaul;

        private UsuarioBackHaulRegistrado()
        {
            idUsuario = 0;
            tipoBackHaul = false;
        }

        public static void EstablecerTipoBackHaul(bool estipoBackHaul)
        {
            tipoBackHaul = estipoBackHaul;
        }

        public static bool ObtenerTipoBackHaul()
        {
            return tipoBackHaul;
        }

        public UsuarioBackHaulRegistrado ObtenerInstancia()
        {
            return unicaInstancia;
        }
    }
}
