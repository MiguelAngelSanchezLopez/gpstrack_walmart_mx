﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Web.SessionState;
using UtilitiesLayer;
using System.Configuration;
using CrossCutting;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Web;
using System.Net;

namespace BusinessLayer
{
    public class Methods_Viajes
    {
        private ModelEntities _context = new ModelEntities();

        public List<Track_GetViajesRuta_Result> GetViajesRuta(int nroTransporte, string patente, string estadoViaje, string transportista)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetViajesRuta_Result> _listViajes = _context.Track_GetViajesRuta(nroTransporte, patente, estadoViaje, transportista).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetViajesRuta_Result>();
            }
        }

        public List<Track_GetNroTransportesRuta_Result> GetNroTransportesRuta()
        {
            try
            {
                List<Track_GetNroTransportesRuta_Result> _listNroTransportes = _context.Track_GetNroTransportesRuta().ToList();
                return _listNroTransportes;

            }
            catch (Exception)
            {
                return new List<Track_GetNroTransportesRuta_Result>();
            }
        }

        public List<Track_GetPatentesRuta_Result> GetPatentesRuta()
        {
            try
            {
                List<Track_GetPatentesRuta_Result> _listPatentesRuta = _context.Track_GetPatentesRuta().ToList();
                return _listPatentesRuta;

            }
            catch (Exception)
            {
                return new List<Track_GetPatentesRuta_Result>();
            }
        }

        public List<Track_GetTransportistasRuta_Result> GetTransportistasRuta()
        {
            try
            {
                List<Track_GetTransportistasRuta_Result> _listTransportistasRuta = _context.Track_GetTransportistasRuta().ToList();
                return _listTransportistasRuta;

            }
            catch (Exception)
            {
                return new List<Track_GetTransportistasRuta_Result>();
            }
        }

        public List<Track_GetPosicionesRuta_Result> GetPosicionesRuta(string patenteTracto, string patenteTrailer, DateTime? fechaHoraCreacion, DateTime? fechaHoraSalidaOrigen, DateTime? fechaHoraLlegadaDestino, long nroTransporte, long idEmbarque, int destino, string estadoViaje)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

                List<Track_GetPosicionesRuta_Result> _listaPosicionesRuta = _context.Track_GetPosicionesRuta(patenteTracto, patenteTrailer, fechaHoraCreacion, fechaHoraSalidaOrigen, fechaHoraLlegadaDestino, nroTransporte, idEmbarque, destino, estadoViaje).ToList();
                return _listaPosicionesRuta;

            }
            catch (Exception e)
            {
                CommonUtilities.WriteInfo("Message: " + e.Message + " Inner Exception: " + e.InnerException + " StackTrace: " + e.StackTrace, CommonUtilities.EventType.Error, ConfigurationManager.AppSettings["LogPath"].ToString(), true);
                return new List<Track_GetPosicionesRuta_Result>();
            }
        }

        public List<Track_Movil> GetAllPatentes(string transportista, string userName, bool _all = false)
        {
            try
            {
                List<Track_Movil> _listaPatentes = (from c in _context.Track_Movil where (c.Transportista == transportista || transportista == "Todos" || transportista == "") select c).ToList();
                List<Track_Movil> _listaPatentesFiltradas;

                int idUsuario;
                string[] CEDISAsociado;

                int perfil = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdRol).FirstOrDefault();
                if (perfil > 2)
                {
                    idUsuario = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdUsuario).FirstOrDefault();

                    CEDISAsociado = (from c in _context.Track_UsuariosCEDIS where (c.IdUsuario == idUsuario) select c.DeterminanteCEDIS.ToString()).ToArray();

                    List<PoolPlaca> _patentesPool = (from c in _context.PoolPlaca where (CEDISAsociado.Contains(c.Determinante.ToString())) select c).ToList();
                    List<PoolPlacaRemolque> _patentesPoolRemolques = (from c in _context.PoolPlacaRemolque where (CEDISAsociado.Contains(c.Determinante.ToString())) select c).ToList();

                    _listaPatentesFiltradas = new List<Track_Movil>();

                    for (int i = 0; i < _listaPatentes.Count(); i++)
                    {
                        int _encontrado = 0;

                        for (int j = 0; j < _patentesPool.Count(); j++)
                        {
                            if (_listaPatentes[i].Patente == _patentesPool[j].Placa)
                            {
                                _listaPatentesFiltradas.Add(_listaPatentes[i]);
                                _encontrado = 1;
                                break;
                            }
                        }

                        if (_encontrado == 0)
                        {
                            for (int j = 0; j < _patentesPoolRemolques.Count(); j++)
                            {
                                if (_listaPatentes[i].Patente == _patentesPoolRemolques[j].Placa)
                                {
                                    _listaPatentesFiltradas.Add(_listaPatentes[i]);
                                    break;
                                }
                            }
                        }

                    }
                }
                else
                {
                    _listaPatentesFiltradas = _listaPatentes;
                }

                if (_all)
                {
                    Track_Movil newItem = new Track_Movil { Patente = "Todas" };
                    _listaPatentesFiltradas.Insert(0, newItem);
                }

                return _listaPatentesFiltradas;

            }
            catch (Exception)
            {
                return new List<Track_Movil>();
            }
        }

        public List<PoolPlaca> GetAllPatentesPool(string transportista, bool _all = false)
        {
            try
            {
                List<PoolPlaca> _listaPatentes = (from c in _context.PoolPlaca where (c.Transporte == transportista || transportista == "Todos" || transportista == "Todas" || transportista == "") select c).ToList();
                if (_all)
                {
                    PoolPlaca newItem = new PoolPlaca { Placa = "Todas" };
                    _listaPatentes.Insert(0, newItem);
                }

                return _listaPatentes;

            }
            catch (Exception)
            {
                return new List<PoolPlaca>();
            }
        }

        public List<PoolPlacaRemolque> GetAllPatentesPoolRemolques(string transportista, bool _all = false)
        {
            try
            {
                List<PoolPlacaRemolque> _listaPatentes = (from c in _context.PoolPlacaRemolque where (c.Carrier == transportista || transportista == "Todos" || transportista == "Todas" || transportista == "") select c).ToList();
                if (_all)
                {
                    PoolPlacaRemolque newItem = new PoolPlacaRemolque { Placa = "Todas" };
                    _listaPatentes.Insert(0, newItem);
                }

                return _listaPatentes;

            }
            catch (Exception)
            {
                return new List<PoolPlacaRemolque>();
            }
        }

        public List<PoolPlaca> GetAllCedisPool(string transportista, string userName, bool _all = false)
        {
            try
            {
                int idUsuario;
                string[] CEDISAsociado;

                List<PoolPlaca> _listaCedis = new List<PoolPlaca>();

                int perfil = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdRol).FirstOrDefault();

                //con el proposito de verificar la carga correcta de placas por cedis procedo a comentar //020718
                //la siguiente programacion //020718
                
                if (perfil > 2)
                {
                    idUsuario = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdUsuario).FirstOrDefault();

                    CEDISAsociado = (from c in _context.Track_UsuariosCEDIS where (c.IdUsuario == idUsuario) select c.DeterminanteCEDIS.ToString()).ToArray();

                    _listaCedis = (from c in _context.PoolPlaca where (CEDISAsociado.Contains(c.Determinante.ToString())) && (c.Transporte == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
                }
                else
                {
                    _listaCedis = (from c in _context.PoolPlaca where (c.Transporte == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
                }

                

                //programacion prueba //020718
                /*
                idUsuario = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdUsuario).FirstOrDefault();

                CEDISAsociado = (from c in _context.Track_UsuariosCEDIS where (c.IdUsuario == idUsuario) select c.DeterminanteCEDIS.ToString()).ToArray();

                _listaCedis = (from c in _context.PoolPlaca where (CEDISAsociado.Contains(c.Determinante.ToString())) && (c.Transporte == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
                */
                //fin de programacion prueba //020718


                if (_all)
                {
                    PoolPlaca newItem = new PoolPlaca { Cedis = "Todas" };
                    _listaCedis.Insert(0, newItem);
                }

                return _listaCedis;

            }
            catch (Exception e)
            {
                return new List<PoolPlaca>();
            }
        }
        /*
            public List<PoolPlacaRemolque> GetAllCedisPoolRemolque(string transportista, string userName, bool _all = false)
            {
                try
                {
                    int idUsuario;
                    string[] CEDISAsociado;

                    List<PoolPlacaRemolque> _listaCedis;

                    int perfil = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdRol).FirstOrDefault();

                    if (perfil > 2)
                    {
                        idUsuario = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdUsuario).FirstOrDefault();

                        CEDISAsociado = (from c in _context.Track_UsuariosCEDIS where (c.IdUsuario == idUsuario) select c.DeterminanteCEDIS.ToString()).ToArray();

                        _listaCedis = (from c in _context.PoolPlacaRemolque where ((CEDISAsociado.Contains(c.Determinante.ToString())) && (c.Carrier == transportista || transportista == "Todos" || transportista == "")) select c).Distinct().ToList();
                    }
                    else
                    {
                        _listaCedis = (from c in _context.PoolPlacaRemolque where (c.Carrier == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
                    }

                    if (_all)
                    {
                        PoolPlacaRemolque newItem = new PoolPlacaRemolque { Cedis = "Todas" };
                        _listaCedis.Insert(0, newItem);
                    }

                    return _listaCedis;

                }
                catch (Exception e)
                {
                    return new List<PoolPlacaRemolque>();
                }
            }
            */
        public List<PoolPlacaRemolque> GetAllCarrierPool(bool _all = false)
        {
            try
            {
                List<PoolPlacaRemolque> _listaCarrier = (from c in _context.PoolPlacaRemolque select c).Distinct().ToList();
                if (_all)
                {
                    PoolPlacaRemolque newItem = new PoolPlacaRemolque { Carrier = "Todos" };
                    _listaCarrier.Insert(0, newItem);
                }

                return _listaCarrier;

            }
            catch (Exception e)
            {
                return new List<PoolPlacaRemolque>();
            }
        }

        public List<PoolPlaca> GetTransportistabyCedis(string cedis)
        {
            try
            {
                List<PoolPlaca> _listTransportistas = (from c in _context.PoolPlaca where (cedis.Contains(c.Cedis) || cedis == "Todos" || cedis == "Todas") select c).Distinct().ToList();
                return _listTransportistas;
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<PoolPlaca>();
            }
        }

        public List<Track_Movil> GetAllTransportistas(bool _all, HttpSessionState _session)
        {
            try
            {
                List<Track_Movil> _listaTransportistas = new List<Track_Movil>();
                string transportista = Utilities.GetTransportistaSession(_session);

                if (transportista != "")
                {
                    Track_Movil newItem = new Track_Movil { Transportista = transportista };
                    _listaTransportistas.Insert(0, newItem);
                }
                else
                {
                    _listaTransportistas = (from c in _context.Track_Movil select c).ToList();
                    if (_all)
                    {
                        Track_Movil newItem = new Track_Movil { Transportista = "Todos" };
                        _listaTransportistas.Insert(0, newItem);
                    }
                }
                return _listaTransportistas;

            }
            catch (Exception)
            {
                return new List<Track_Movil>();
            }
        }

        public List<Track_GetViajesHistoricos_Result> GetViajesHistoricos(DateTime desde, DateTime hasta, long nroTransporte, string patente, string estadoViaje, string transportista, int codLocal, string tipoViaje, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetViajesHistoricos_Result> _listViajes = _context.Track_GetViajesHistoricos(desde, hasta, transportista, patente, estadoViaje, nroTransporte, codLocal, tipoViaje, userName).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetViajesHistoricos_Result>();
            }
        }

        public List<Track_GetPosicionesGPS_Result> GetPosicionesGPS(DateTime fechaDesde, DateTime fechaHasta, string patente)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

                List<Track_GetPosicionesGPS_Result> _listaPosicionesGPS = _context.Track_GetPosicionesGPS(fechaDesde, fechaHasta, patente).ToList();
                return _listaPosicionesGPS;

            }
            catch (Exception)
            {
                return new List<Track_GetPosicionesGPS_Result>();
            }
        }

        public List<Track_GetPosicionesGPS_Ruta_Result> GetPosicionesGPS_Ruta(DateTime fechaDesde, DateTime fechaHasta, string patente)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

                List<Track_GetPosicionesGPS_Ruta_Result> _listaPosicionesGPS_Ruta = _context.Track_GetPosicionesGPS_Ruta(fechaDesde, fechaHasta, patente).ToList();
                return _listaPosicionesGPS_Ruta;

            }
            catch (Exception)
            {
                return new List<Track_GetPosicionesGPS_Ruta_Result>();
            }
        }

        public List<Track_GetInformeViajes_Result> GetInformeViajes(DateTime desde, DateTime hasta, long nroTransporte, string patente, string transportista, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetInformeViajes_Result> _listViajes = _context.Track_GetInformeViajes(desde, hasta, transportista, patente, nroTransporte, userName).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetInformeViajes_Result>();
            }
        }

        public List<Track_GetDetalleTrayecto_Result> GetDetalleTrayecto(long nroTransporte)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetDetalleTrayecto_Result> _listaDetalleTrayecto = _context.Track_GetDetalleTrayecto(nroTransporte).ToList();
                return _listaDetalleTrayecto;

            }
            catch (Exception)
            {
                return new List<Track_GetDetalleTrayecto_Result>();
            }
        }

        public List<Track_GetNroTransportes_Result> GetNroTransportes(DateTime desde, DateTime hasta)
        {
            try
            {
                List<Track_GetNroTransportes_Result> _listNroTransportes = _context.Track_GetNroTransportes(desde, hasta).ToList();
                return _listNroTransportes;

            }
            catch (Exception)
            {
                return new List<Track_GetNroTransportes_Result>();
            }
        }

        public List<Track_GetFlotaOnline_Result> GetFlotaOnline(string patente, string transportista, int ignicion, string estadoViaje, string estadoGPS, string proveedorGPS)
        {
            try
            {
                List<Track_GetFlotaOnline_Result> _listaFlotaOnline = _context.Track_GetFlotaOnline(patente, transportista, ignicion, estadoViaje, estadoGPS, proveedorGPS).ToList();
                return _listaFlotaOnline;

            }
            catch (Exception)
            {
                return new List<Track_GetFlotaOnline_Result>();
            }
        }

        public string CrearCSV_ViajesAsignados(string userName) {

            List<Track_GetViajesAsignados_Result_ConTipoTransporte> _listViajes = new List<Track_GetViajesAsignados_Result_ConTipoTransporte>();

            
            try
            {
                //consultado el sp de esta manera por NUEVO CAMPO TIPO TRANSPORTE 040718 348PM

                
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetViajesAsignados");

                EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");

                EjecutarProcedimientoAlmacenado();

                LlenarLectorDatosSQL();

                _listViajes.AddRange(LlenarListadoViajesAsignados());

                conexionSQL.Close();

                //return _listViajes;
                

            }
            catch (Exception e)
            {
                throw e;
                //return new List<Track_GetViajesAsignados_Result>();
                //return new List<Track_GetViajesAsignados_Result_ConTipoTransporte>();
            }
            
            string Fechafile = DateTime.Now.ToString("ddMMyyyyHHmmss");
            string rutaSitio = Utilities.GetExePath();

            string nombreArchivo= "ViajesAsignados" + Fechafile + ".csv";

            string filePath = @"" + rutaSitio + "FileTemp\\"+ nombreArchivo;

            
            string delimiter = ",";

            List<List<string>> conjuntoDatos = new List<List<string>>();

            List<string> columnas = new List<string>();
            
                 columnas.Add("IdViaje");
                columnas.Add("NroTransporte");
                columnas.Add("SecuenciaDestino");
                columnas.Add("RutConductor");
                columnas.Add("NombreConductor");
                columnas.Add("PatenteTracto");
                columnas.Add("PatenteTrailer");
                columnas.Add("RutTransportista");
                columnas.Add("NombreTransportista");
                columnas.Add("CodigoOrigen");
                columnas.Add("NombreOrigen");
                columnas.Add("CodigoDestino");
                columnas.Add("NombreDestino");
                columnas.Add("FechaAsignacion");
                columnas.Add("IdEmbarque");
                columnas.Add("Comentarios");
                 columnas.Add("TipoTransporte");
                 
            conjuntoDatos.Add(columnas);
            
            foreach (Track_GetViajesAsignados_Result_ConTipoTransporte viaje in _listViajes) {
                List<string> datosViaje = new List<string>();

                datosViaje.Add(viaje.IdViaje.ToString());
                
                datosViaje.Add(viaje.NroTransporte.ToString());
                datosViaje.Add(viaje.SecuenciaDestino.ToString());
                datosViaje.Add(viaje.RutConductor.ToString());
                datosViaje.Add(viaje.NombreConductor.ToString());
                datosViaje.Add(viaje.PatenteTracto.ToString());
                datosViaje.Add(viaje.PatenteTrailer.ToString());
                datosViaje.Add(viaje.RutTransportista.ToString());
                datosViaje.Add(viaje.NombreTransportista.ToString());
                datosViaje.Add(viaje.CodigoOrigen.ToString());
                datosViaje.Add(viaje.NombreOrigen.ToString());
                datosViaje.Add(viaje.CodigoDestino.ToString());
                datosViaje.Add(viaje.NombreDestino.ToString());
                datosViaje.Add(viaje.FechaAsignacion.ToString());
                datosViaje.Add(viaje.IdEmbarque.ToString());
                datosViaje.Add(viaje.Comentarios.ToString());
                datosViaje.Add(viaje.TipoTransporte.ToString());
             
                conjuntoDatos.Add(datosViaje);
            }
            
            int length = conjuntoDatos.Count;
            StringBuilder sb = new StringBuilder();

            for (int index = 0; index < length; index++)
                sb.AppendLine(string.Join(delimiter, conjuntoDatos[index]));
            
            File.WriteAllText(filePath, sb.ToString());
            
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.ClearContent();
            byte[] data = Encoding.ASCII.GetBytes(nombreArchivo); 
            response.BinaryWrite(data);
            response.End();

            return filePath;
        }

        //public List<Track_GetViajesAsignados_Result> GetViajesAsignados(string userName)
        public List<Track_GetViajesAsignados_Result_ConTipoTransporte> GetViajesAsignados(string userName)
        {
            try
            {
                //consultado el sp de esta manera por NUEVO CAMPO TIPO TRANSPORTE 040718 348PM
                
                List<Track_GetViajesAsignados_Result_ConTipoTransporte> _listViajes = new List<Track_GetViajesAsignados_Result_ConTipoTransporte>();
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetViajesAsignados");
                
                EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");

                EjecutarProcedimientoAlmacenado();

                LlenarLectorDatosSQL();

                _listViajes.AddRange(LlenarListadoViajesAsignados());

                conexionSQL.Close();

                return _listViajes;
                



                //List<Track_GetViajesAsignados_Result> _listViajes = _context.Track_GetViajesAsignados(userName).ToList();
                //return _listViajes;
                
            }
            catch (Exception e)
            {
                //return new List<Track_GetViajesAsignados_Result>();
                return new List<Track_GetViajesAsignados_Result_ConTipoTransporte>();
            }
        }

        public List<Track_GetViajesControlPanel_Result> GetViajesControlPanel(DateTime desde, DateTime hasta, int nroTransporte, string patente, string estadoViaje, string transportista, string alertas, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetViajesControlPanel_Result> _listViajes = _context.Track_GetViajesControlPanel(desde, hasta, transportista, patente, estadoViaje, nroTransporte, alertas, userName).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetViajesControlPanel_Result>();
            }
        }

        public string EliminarViaje(long nroTransporte, long idEmbarque, int codLocal)
        {
            try
            {
                string _resp = _context.Track_EliminarViaje(nroTransporte, idEmbarque, codLocal).FirstOrDefault().Respuesta;

                return _resp;
            }
            catch (Exception)
            {
                return "Error al intentar eliminar el viaje.";
            }

        }

        public string AgregarDestino(long nroTransporte, long idEmbarque, int codLocal)
        {
            try
            {
                string _resp = _context.Track_AgregarDestino(nroTransporte, idEmbarque, codLocal).FirstOrDefault().Respuesta;

                return _resp;
            }
            catch (Exception)
            {
                return "Error al intentar agregar el destino.";
            }

        }

        public int ValidarMovilCD(string patente)
        {
            try
            {
                int _resp = _context.Track_ValidarMovilCD(patente).FirstOrDefault().Respuesta.Value;

                return _resp;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public bool ValidarNroTransporte(int nroTransporte)
        {
            try
            {
                TrazaViaje _existe = _context.TrazaViaje.Where(C => C.NroTransporte == nroTransporte).FirstOrDefault();

                if (_existe != null)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //public string NuevoViaje(int nroTransporte, int idEmbarque, string transportista, string tipoViaje, string idNave, string tracto, string trailer, string rutConductor, int codOrigen, string listaDestinos, string Comentarios, DateTime fechaCita, string userName, int idUsuario)
        public string NuevoViaje(int nroTransporte, int idEmbarque, string transportista, string tipoViaje, string idNave, string tracto, string trailer, string rutConductor, int codOrigen, string listaDestinos, string Comentarios, DateTime fechaCita, string userName, int idUsuario,string tipoTransporte ="")
        {
            try
            {

                string _resp = _context.Track_NuevoViaje_2(nroTransporte, idEmbarque, transportista, tipoViaje, idNave, tracto, trailer, rutConductor, codOrigen, listaDestinos, Comentarios, userName, tipoTransporte).FirstOrDefault().Respuesta;

                Methods_User _user = new Methods_User();
                _user.guardarlog(idUsuario, "Creación de viaje: " + nroTransporte.ToString());

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public string EditarViaje(int nroTransporte, string transportista, string trailer, string tracto, int codOrigen, int codDestino, string rutConductor, string nombreConductor, int idUsuario)
        {
            try
            {

                string _resp = _context.Track_EditarViaje(nroTransporte, transportista, trailer, tracto, codOrigen, codDestino, rutConductor, nombreConductor).FirstOrDefault().Respuesta;

                Methods_User _user = new Methods_User();
                _user.guardarlog(idUsuario, "Edición de viaje: " + nroTransporte.ToString());


                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public List<Track_GetPosicionesRutaModuloMapa_Result> GetPosicionesRutaModuloMapa(int idAlerta)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

                List<Track_GetPosicionesRutaModuloMapa_Result> _listaPosicionesRuta = _context.Track_GetPosicionesRutaModuloMapa(idAlerta).ToList();
                return _listaPosicionesRuta;

            }
            catch (Exception)
            {
                return new List<Track_GetPosicionesRutaModuloMapa_Result>();
            }
        }

        public List<Track_GetProveedoresGPS_Result> GetProveedoresGPS(bool _all, string rutTransportista)
        {
            try
            {
                List<Track_GetProveedoresGPS_Result> _listProveedores = _context.Track_GetProveedoresGPS(rutTransportista).ToList();

                if (_all)
                {
                    Track_GetProveedoresGPS_Result item = new Track_GetProveedoresGPS_Result { ProveedorGPS = "Todos" };
                    _listProveedores.Insert(0, item);
                }

                return _listProveedores;

            }
            catch (Exception)
            {
                return new List<Track_GetProveedoresGPS_Result>();
            }
        }

        public List<Track_GetRutasGeneradas_Result> GetRutasGeneradas(int idRuta)
        {
            try
            {
                List<Track_GetRutasGeneradas_Result> _listRutas = _context.Track_GetRutasGeneradas(idRuta).ToList();
                return _listRutas;

            }
            catch (Exception)
            {
                return new List<Track_GetRutasGeneradas_Result>();
            }
        }

        public List<Track_GetDetalleRuta_Result> GetDetalleRuta(int idRuta)
        {
            try
            {
                List<Track_GetDetalleRuta_Result> _listDetalleRuta = _context.Track_GetDetalleRuta(idRuta).ToList();
                return _listDetalleRuta;

            }
            catch (Exception)
            {
                return new List<Track_GetDetalleRuta_Result>();
            }
        }

        public string EliminarRutaGenerada(int IdRuta)
        {
            try
            {
                string _resp = _context.Track_EliminarRutaGenerada(IdRuta).FirstOrDefault().ToString();

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public string EliminarDestinoRuta(int IdZona)
        {
            try
            {
                string _resp = _context.Track_EliminarDestinoRuta(IdZona).FirstOrDefault().ToString();

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public string AgrgarRutaGenerada(string nombreRuta)
        {
            try
            {
                string _resp = _context.Track_AgregarRutaGenerada(nombreRuta).FirstOrDefault().ToString();

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public string AgregarDestinoRuta(int idRuta, int idZona)
        {
            try
            {
                string _resp = _context.Track_AgregarDestinoRuta(idRuta, idZona).FirstOrDefault().ToString();

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public string NuevoViajeDestinoUnico(int nroTransporte, string transportista, int codOrigen, int codDestino, string patenteTrailer, string patenteTracto, string rutConductor, string nombreConductor)
        {
            try
            {
                string _resp = _context.Track_NuevoViajeDestinoUnico(nroTransporte, transportista, patenteTrailer, patenteTracto, codOrigen, codDestino, rutConductor, nombreConductor, 1).FirstOrDefault().ToString();

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }
        
        public Track_GetViajesDashboard_Result LlenarElementoViajeAsignadoDashboardUsuariosBackHaul()
        {
            Track_GetViajesDashboard_Result elementoViajeAsignado = new Track_GetViajesDashboard_Result();

            try
            {
                elementoViajeAsignado.IdViaje = Convert.ToInt32(lectorDatosSQL["IdViaje"]);
            }
            catch (Exception ex) {
                elementoViajeAsignado.IdViaje = 0;
            }
            
            try
            {
                elementoViajeAsignado.NroTransporte = Convert.ToInt64(lectorDatosSQL["NroTransporte"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.NroTransporte = 0;
            }

            try
            {
                elementoViajeAsignado.IdEmbarque = Convert.ToInt64(lectorDatosSQL["IdEmbarque"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.IdEmbarque = 0;
            }

            

            

            try
            {
                elementoViajeAsignado.Fecha = Convert.ToDateTime(lectorDatosSQL["Fecha"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.Fecha = new DateTime();
            }

            try
            {
                elementoViajeAsignado.SecuenciaDestino = Convert.ToInt32(lectorDatosSQL["SecuenciaDestino"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.SecuenciaDestino =0;
            }
            
            elementoViajeAsignado.PatenteTracto = lectorDatosSQL["PatenteTracto"].ToString();
            elementoViajeAsignado.PatenteTrailer = lectorDatosSQL["PatenteTrailer"].ToString();
            elementoViajeAsignado.Transportista = lectorDatosSQL["Transportista"].ToString();
            
            try
            {
                elementoViajeAsignado.FHCargaPlanificada = Convert.ToDateTime(lectorDatosSQL["FHCargaPlanificada"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHCargaPlanificada = new DateTime();
            }

            try
            {
                elementoViajeAsignado.FHCargaReal = Convert.ToDateTime(lectorDatosSQL["FHCargaReal"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHCargaReal = new DateTime();
            }

            try
            {
                elementoViajeAsignado.FHSalidaPlanificada = Convert.ToDateTime(lectorDatosSQL["FHSalidaPlanificada"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHSalidaPlanificada = new DateTime();
            }

            try
            {
                elementoViajeAsignado.FHSalidaReal = Convert.ToDateTime(lectorDatosSQL["FHSalidaReal"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHSalidaReal = null;//new DateTime();
            }

            try
            {
                elementoViajeAsignado.FHClientePlanificada = Convert.ToDateTime(lectorDatosSQL["FHClientePlanificada"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHClientePlanificada = new DateTime();
            }

            try
            {
                elementoViajeAsignado.FHClienteReal = Convert.ToDateTime(lectorDatosSQL["FHClienteReal"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHClienteReal = null;//new DateTime();
            }

            try
            {
                elementoViajeAsignado.FechaHoraCreacion = Convert.ToDateTime(lectorDatosSQL["FechaHoraCreacion"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FechaHoraCreacion = new DateTime();
            }

            try
            {
                elementoViajeAsignado.FHSalidaDestino = Convert.ToDateTime(lectorDatosSQL["FHSalidaDestino"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHSalidaDestino = null;// new DateTime();
            }
            
            elementoViajeAsignado.EstadoViaje = lectorDatosSQL["EstadoViaje"].ToString();

            try
            {
                elementoViajeAsignado.CodigoOrigen = Convert.ToInt32(lectorDatosSQL["CodigoOrigen"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.CodigoOrigen = 0;
            }
            
            elementoViajeAsignado.NombreOrigen = lectorDatosSQL["NombreOrigen"].ToString();

            try
            {
                elementoViajeAsignado.CodigoDestino = Convert.ToInt32(lectorDatosSQL["CodigoDestino"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.CodigoDestino = 0;
            }
            
            elementoViajeAsignado.NombreDestino = lectorDatosSQL["NombreDestino"].ToString();

            try
            {
                elementoViajeAsignado.FHCierreSistema = Convert.ToDateTime(lectorDatosSQL["FHCierreSistema"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.FHCierreSistema = new DateTime();
            }
            
            elementoViajeAsignado.TiempoViaje = lectorDatosSQL["TiempoViaje"].ToString();

            try
            {
                elementoViajeAsignado.CantidadAlertas = Convert.ToInt32(lectorDatosSQL["CantidadAlertas"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.CantidadAlertas = 0;
            }
            
            elementoViajeAsignado.EstadoLat = lectorDatosSQL["EstadoLat"].ToString();
            elementoViajeAsignado.EstadoLon = lectorDatosSQL["EstadoLon"].ToString();
            elementoViajeAsignado.DestinoLat = lectorDatosSQL["DestinoLat"].ToString();
            elementoViajeAsignado.DestinoLon = lectorDatosSQL["DestinoLon"].ToString();

            try
            {
                elementoViajeAsignado.AlertasGestionadas = Convert.ToInt32(lectorDatosSQL["AlertasGestionadas"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.AlertasGestionadas = 0;
            }

            try
            {
                elementoViajeAsignado.AlertasPorGestionar = Convert.ToInt32(lectorDatosSQL["AlertasPorGestionar"]);
            }
            catch (Exception ex)
            {
                elementoViajeAsignado.AlertasPorGestionar = 0;
            }
            
            elementoViajeAsignado.GestionEntrega = lectorDatosSQL["GestionEntrega"].ToString();
            elementoViajeAsignado.RutConductor = lectorDatosSQL["RutConductor"].ToString();
            elementoViajeAsignado.NombreConductor = lectorDatosSQL["NombreConductor"].ToString();
            elementoViajeAsignado.Folio = lectorDatosSQL["Folio"].ToString();
            elementoViajeAsignado.Factura = lectorDatosSQL["Factura"].ToString();
            elementoViajeAsignado.Marchamo = lectorDatosSQL["Marchamo"].ToString();
            elementoViajeAsignado.Gestionadas = lectorDatosSQL["Gestionadas"].ToString();
            
            return elementoViajeAsignado;
        }
             

        public List<Track_GetViajesDashboard_Result> LlenarListadoViajesAsignadosDashboardUsuariosBackHaul()
        {
            
            List<Track_GetViajesDashboard_Result> listaViajesAsignados = new List<Track_GetViajesDashboard_Result>();

            while (lectorDatosSQL.Read())
                listaViajesAsignados.Add(LlenarElementoViajeAsignadoDashboardUsuariosBackHaul());

            return listaViajesAsignados;
        }

        public List<Track_GetViajesDashboard_Result> GetViajesDashboardControl_Usuarios_NO_BackHaul(DateTime desde, DateTime hasta, long nroTransporte, string patenteTracto, string patenteTrailer, string estadoViaje, string transportista, string alertas, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetViajesDashboard_Result> _listViajes = _context.Track_GetViajesDashboard(desde, hasta, transportista, patenteTracto, patenteTrailer, estadoViaje, nroTransporte, alertas, userName).ToList();
                //return _listViajes;

                int conteoCoincidencias = _listViajes.Count;
                /*proceso para eliminar viajes repetidos*/
                List<Track_GetViajesDashboard_Result> listaViajesNoRepetidos = new List<Track_GetViajesDashboard_Result>();

                Int32 idViajeTemporal = new Int32();
                idViajeTemporal = 0;

                if (conteoCoincidencias > 0)
                {
                    foreach (Track_GetViajesDashboard_Result viaje in _listViajes)
                    {
                        int nuevoIdViaje = Convert.ToInt32(viaje.IdViaje);

                        if (idViajeTemporal != nuevoIdViaje)
                        {
                            idViajeTemporal = Convert.ToInt32(viaje.IdViaje);
                            listaViajesNoRepetidos.Add(viaje);
                        }
                    }
                }


                /*fin de proceso para eliminar viajes repetidos*/
                return listaViajesNoRepetidos;

            }
            catch (Exception)
            {
                return new List<Track_GetViajesDashboard_Result>();
            }
        }

        public List<Track_GetViajesDashboard_Result> GetViajesDashboardControl_Usuarios_SI_BackHaul(DateTime desde, DateTime hasta, long nroTransporte, string patenteTracto, string patenteTrailer, string estadoViaje, string transportista, string alertas, string userName)
        {
            try
            {
                //consultado el sp de esta manera por cedis para obtener viajes de usuarios backhaul 200718 338pm

                List<Track_GetViajesDashboard_Result> _listViajes = new List<Track_GetViajesDashboard_Result>();
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetViajesDashboard_USUARIOS_BACKHAUL_200718");

                //EstablecerParametroProcedimientoAlmacenado(new DateTime(2018, 7, 1), "datetime", "@desde");//desde "01/07/2018"
                EstablecerParametroProcedimientoAlmacenado(new DateTime(2018, 1, 1), "datetime", "@desde");//desde "01/07/2018"
                //DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, hours, minute, second
                EstablecerParametroProcedimientoAlmacenado(new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59), "datetime", "@hasta");
                //EstablecerParametroProcedimientoAlmacenado(new DateTime(2018, 7, 26), "datetime", "@hasta");//hasta "26/07/2018"
                EstablecerParametroProcedimientoAlmacenado(transportista, "string", "@transportista");
                EstablecerParametroProcedimientoAlmacenado(patenteTracto, "string", "@patenteTracto");
                EstablecerParametroProcedimientoAlmacenado(patenteTrailer, "string", "@patenteTrailer");
                //EstablecerParametroProcedimientoAlmacenado("Todos", "string", "@estadoViaje");//estadoViaje
                EstablecerParametroProcedimientoAlmacenado(estadoViaje, "string", "@estadoViaje");
                EstablecerParametroProcedimientoAlmacenado(nroTransporte, "int", "@nroTransporte");
                EstablecerParametroProcedimientoAlmacenado(alertas, "string", "@alertas");
                EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");

                EjecutarProcedimientoAlmacenado();

                LlenarLectorDatosSQL();

                _listViajes.AddRange(LlenarListadoViajesAsignadosDashboardUsuariosBackHaul());

                int conteoCoincidencias = _listViajes.Count;

                /*proceso para eliminar viajes repetidos*/
                List<Track_GetViajesDashboard_Result> listaViajesNoRepetidos = new List<Track_GetViajesDashboard_Result>();

                Int32 idViajeTemporal = new Int32();
                idViajeTemporal = 0;

                if (conteoCoincidencias > 0) {
                    foreach (Track_GetViajesDashboard_Result viaje in _listViajes) {
                        int nuevoIdViaje= Convert.ToInt32(viaje.IdViaje);

                        if (idViajeTemporal != nuevoIdViaje) {
                            idViajeTemporal = Convert.ToInt32(viaje.IdViaje);
                            listaViajesNoRepetidos.Add(viaje);
                        }
                    }
                }
                    

                /*fin de proceso para eliminar viajes repetidos*/

                conexionSQL.Close();

                //return _listViajes;
                return listaViajesNoRepetidos;

            }
            catch (Exception e)
            {
                return new List<Track_GetViajesDashboard_Result>();
            }
        }

        public List<Track_GetViajesDashboard_Result> GetViajesDashboardControl(DateTime desde, DateTime hasta, long nroTransporte, string patenteTracto, string patenteTrailer, string estadoViaje, string transportista, string alertas, string userName)
        {
            //revisar si el usuario logueado es de backhaul
            bool usuarioEsTipoBackhaul = UsuarioBackHaulRegistrado.ObtenerTipoBackHaul();

            //si lo es consultar resultados con la nueva forma
            //de lo contrario usar la vieja forma

            if (usuarioEsTipoBackhaul)
                return GetViajesDashboardControl_Usuarios_SI_BackHaul(desde, hasta, nroTransporte, patenteTracto, patenteTrailer, estadoViaje, transportista, alertas, userName);
            else
                return GetViajesDashboardControl_Usuarios_NO_BackHaul(desde, hasta, nroTransporte, patenteTracto, patenteTrailer, estadoViaje, transportista, alertas, userName);
            
        }

        public List<Track_GetMonitoreoOnline_Result> GetMonitoreoOnline(string patente, string transportista, int ignicion, string estadoViaje, string estadoGPS, string proveedorGPS, long nroTransporte, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetMonitoreoOnline_Result> _listaMonitoreoOnline = _context.Track_GetMonitoreoOnline(patente, transportista, ignicion, estadoViaje, estadoGPS, proveedorGPS, nroTransporte, userName).ToList();
                return _listaMonitoreoOnline;

            }
            catch (Exception ex)
            {
                return new List<Track_GetMonitoreoOnline_Result>();
            }
        }

        public List<Track_GetViajesBackhaul_Result> GetViajesBackhaul(DateTime desde, DateTime hasta, long nroTransporte, string patente, string transportista, int codLocal, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetViajesBackhaul_Result> _listViajes = _context.Track_GetViajesBackhaul(desde, hasta, transportista, patente, nroTransporte, codLocal, userName).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetViajesBackhaul_Result>();
            }
        }

        public List<Track_GetCamionesCercanosBackhaul_Result> GetCamionesCercanosBackhaul()
        {
            try
            {
                List<Track_GetCamionesCercanosBackhaul_Result> _lista = _context.Track_GetCamionesCercanosBackhaul().ToList();
                return _lista;

            }
            catch (Exception)
            {
                return new List<Track_GetCamionesCercanosBackhaul_Result>();
            }
        }

        public string NuevoViajeBackhaul(long nroTransporte, long idEmbarque, string rutTransportista, string tracto, string trailer, int codDeterminante, int codproveedor, int codCEDIS)
        {
            try
            {
                string _resp = _context.Track_NuevoViajeBackhaul(nroTransporte, idEmbarque, rutTransportista, tracto, trailer, codDeterminante, codproveedor, codCEDIS).FirstOrDefault().ToString();

                return _resp;
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public List<Track_GetUltimaPosicion_Result> GetUltimaPosicion(int idAlerta)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetUltimaPosicion_Result> _listViajes = _context.Track_GetUltimaPosicion(idAlerta).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetUltimaPosicion_Result>();
            }
        }

        public List<Track_GetRotacionTracto_Result> getRotacionTracto()
        {
            try
            {
                return _context.Track_GetRotacionTracto().ToList();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<Track_GetRotacionTracto_Result>();
            }
        }

        public List<Track_GetRotacionRemolque_Result> getRotacionRemolque(int cantidad)
        {
            try
            {
                List<Track_GetRotacionRemolque_Result> _result;

                if (cantidad > 3)
                    _result = _context.Track_GetRotacionRemolque().Where(x => x.CantidadViajes > 3).ToList();
                else
                    _result = _context.Track_GetRotacionRemolque().Where(x => x.CantidadViajes == cantidad).ToList();

                return _result;
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<Track_GetRotacionRemolque_Result>();
            }
        }

        public void SaveProgramacion(string tracto, string embarque, string determinante, string remolque, string rotacion, string zona, string UsuarioCreacion)
        {
            _context.Track_saveProgrmacion(tracto, embarque, determinante, remolque, rotacion, zona, UsuarioCreacion);
        }

        public int InsideVentanaHoraria(DateTime fecha, int codLocal)
        {
            try
            {
                int _resp = (int)_context.Track_InsideVentanaHoraria(fecha, codLocal).FirstOrDefault().Response;

                return _resp;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public List<Track_InforWindows_Result> InfoWindows(long nroTransporte)
        {
            try
            {
                return _context.Track_InforWindows(nroTransporte).ToList();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<Track_InforWindows_Result>();
            }
        }

        #region nuevocodigopruebasp030718
        //codigo para probar nuevo sp //030718
        
        protected SqlConnection conexionSQL;
        protected SqlCommand comandoSQL;
        protected SqlDataReader lectorDatosSQL;

        public string CADENA_CONEXION = ConfigurationManager.ConnectionStrings["ConexionPerfilamiento"].ConnectionString;

        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado()
        {
            comandoSQL.CommandTimeout = 120;
            comandoSQL.ExecuteNonQuery();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro, string descripcionTipo, string nombreParametro)
        {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo)
        {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo)
            {
                case "int":
                    tipoDatoDevolver = SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL()
        {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Track_GetViajesAsignados_Result_ConTipoTransporte> LlenarListadoViajesAsignados()
        {//public List<Track_GetViajesAsignados_Result> LlenarListadoViajesAsignados() {
            //List<Track_GetViajesAsignados_Result> listaViajesAsignados = new List<Track_GetViajesAsignados_Result>();
            List<Track_GetViajesAsignados_Result_ConTipoTransporte> listaViajesAsignados = new List<Track_GetViajesAsignados_Result_ConTipoTransporte>();

            while (lectorDatosSQL.Read())
                listaViajesAsignados.Add(LlenarElementoViajeAsignado());

            return listaViajesAsignados;
        }

        public List<Track_GetReporteEstadiaTractos_Result> LlenarListadoReporteEstadiaTractos()
        {
            List<Track_GetReporteEstadiaTractos_Result> listaReporteEstadiaTractos = new List<Track_GetReporteEstadiaTractos_Result>();

            while (lectorDatosSQL.Read())
                listaReporteEstadiaTractos.Add(LlenarElementoReporteEstadiaTracto());

            return listaReporteEstadiaTractos;
        }

        //public Track_GetViajesAsignados_Result LlenarElementoViajeAsignado() {
        public Track_GetViajesAsignados_Result_ConTipoTransporte LlenarElementoViajeAsignado()
        {
            Track_GetViajesAsignados_Result_ConTipoTransporte elementoViajeAsignado = new Track_GetViajesAsignados_Result_ConTipoTransporte();
            elementoViajeAsignado.IdViaje = Convert.ToInt32(lectorDatosSQL["IdViaje"]);
            elementoViajeAsignado.NroTransporte = Convert.ToInt64(lectorDatosSQL["NroTransporte"]);
            elementoViajeAsignado.SecuenciaDestino = Convert.ToInt32(lectorDatosSQL["SecuenciaDestino"]);

            elementoViajeAsignado.RutConductor = lectorDatosSQL["RutConductor"].ToString();
            elementoViajeAsignado.NombreConductor = lectorDatosSQL["NombreConductor"].ToString();

            elementoViajeAsignado.PatenteTracto = lectorDatosSQL["PatenteTracto"].ToString();
            elementoViajeAsignado.PatenteTrailer = lectorDatosSQL["PatenteTrailer"].ToString();
            elementoViajeAsignado.RutTransportista = lectorDatosSQL["RutTransportista"].ToString();
            elementoViajeAsignado.NombreTransportista = lectorDatosSQL["NombreTransportista"].ToString();

            elementoViajeAsignado.CodigoOrigen = Convert.ToInt32(lectorDatosSQL["CodigoOrigen"]);

            elementoViajeAsignado.NombreOrigen = lectorDatosSQL["NombreOrigen"].ToString();

            elementoViajeAsignado.CodigoDestino = Convert.ToInt32(lectorDatosSQL["CodigoDestino"]);

            elementoViajeAsignado.NombreDestino = lectorDatosSQL["NombreDestino"].ToString();

            elementoViajeAsignado.FechaAsignacion = Convert.ToDateTime(lectorDatosSQL["FechaAsignacion"].ToString());

            elementoViajeAsignado.IdEmbarque = Convert.ToInt64(lectorDatosSQL["IdEmbarque"]);

            elementoViajeAsignado.Comentarios = lectorDatosSQL["Comentarios"].ToString();

            elementoViajeAsignado.TipoTransporte = lectorDatosSQL["TipoTransporte"].ToString();
            
            return elementoViajeAsignado;
        }

        public Track_GetReporteEstadiaTractos_Result LlenarElementoReporteEstadiaTracto()
        {
            Track_GetReporteEstadiaTractos_Result elementoReporteEstadiaTracto = new Track_GetReporteEstadiaTractos_Result();

            elementoReporteEstadiaTracto.Placa = lectorDatosSQL["Placa"].ToString();
            elementoReporteEstadiaTracto.Transportista = lectorDatosSQL["Transportista"].ToString();
            elementoReporteEstadiaTracto.EstadoEventos = lectorDatosSQL["EstadoEventos"].ToString();
            elementoReporteEstadiaTracto.FechaEvento = lectorDatosSQL["FechaEvento"].ToString();
            elementoReporteEstadiaTracto.Antiguedad = lectorDatosSQL["Antiguedad"].ToString();
            elementoReporteEstadiaTracto.Velocidad = Convert.ToInt32(lectorDatosSQL["Velocidad"]);
            elementoReporteEstadiaTracto.Latitud = Convert.ToDecimal(lectorDatosSQL["Latitud"]);
            elementoReporteEstadiaTracto.Longitud = Convert.ToDecimal(lectorDatosSQL["Longitud"]);

            elementoReporteEstadiaTracto.Referencia = lectorDatosSQL["Referencia"].ToString();
            elementoReporteEstadiaTracto.Detenido = lectorDatosSQL["Detenido"].ToString();
            elementoReporteEstadiaTracto.TiempoDetenido = lectorDatosSQL["TiempoDetenido"].ToString();
            elementoReporteEstadiaTracto.FechaDetencion = lectorDatosSQL["FechaDetencion"].ToString();
            elementoReporteEstadiaTracto.UltimaEntrada = lectorDatosSQL["UltimaEntrada"].ToString();
            elementoReporteEstadiaTracto.UltimaSalida = lectorDatosSQL["UltimaSalida"].ToString();
            elementoReporteEstadiaTracto.EnZona = lectorDatosSQL["EnZona"].ToString();

            elementoReporteEstadiaTracto.Zona = Convert.ToInt32(lectorDatosSQL["Zona"]);

            elementoReporteEstadiaTracto.TiempoEnZona = lectorDatosSQL["TiempoEnZona"].ToString();

            elementoReporteEstadiaTracto.CodZonaCercana = Convert.ToInt32(lectorDatosSQL["CodZonaCercana"]);

            elementoReporteEstadiaTracto.NombreZonaCercana = lectorDatosSQL["NombreZonaCercana"].ToString();
            elementoReporteEstadiaTracto.DistanciaZonaCercana = lectorDatosSQL["DistanciaZonaCercana"].ToString();

            elementoReporteEstadiaTracto.CEDIS = Convert.ToInt32(lectorDatosSQL["CEDIS"]);

            elementoReporteEstadiaTracto.NombreCEDIS = lectorDatosSQL["NombreCEDIS"].ToString();
            elementoReporteEstadiaTracto.TipoUnidad = lectorDatosSQL["TipoUnidad"].ToString();
            elementoReporteEstadiaTracto.FechaReporte = lectorDatosSQL["FechaReporte"].ToString();

            elementoReporteEstadiaTracto.DesfaseReporte = Convert.ToInt32(lectorDatosSQL["DesfaseReporte"]);
            elementoReporteEstadiaTracto.HorasDetenido = Convert.ToInt32(lectorDatosSQL["HorasDetenido"]);

            elementoReporteEstadiaTracto.UltimaZona = lectorDatosSQL["UltimaZona"].ToString();
            elementoReporteEstadiaTracto.NombreZona = lectorDatosSQL["NombreZona"].ToString();
            elementoReporteEstadiaTracto.Formato = lectorDatosSQL["Formato"].ToString();
            elementoReporteEstadiaTracto.EstadoCarga = lectorDatosSQL["EstadoCarga"].ToString();
            elementoReporteEstadiaTracto.EstadoViaje = lectorDatosSQL["EstadoViaje"].ToString();
            elementoReporteEstadiaTracto.RegionZona = lectorDatosSQL["RegionZona"].ToString();
            elementoReporteEstadiaTracto.HorasDetenidoTxt = lectorDatosSQL["HorasDetenidoTxt"].ToString();
            
            return elementoReporteEstadiaTracto;
        }
        
        //fin de codigo para porbar nuevo sp //030718
        #endregion nuevocodigopruebasp030718

        public List<Track_GetReporteEstadiaTractos_Result> getReporteEstadiaTractos(string Transportista, string patente, string Cedis, string userName)
        {
            try
            {

                //aqui debo vinular a mi sp Track_GetReporteEstadiaTractos_nuevo_040718
                //y si detecto que tiene un todas ese debe ser el unico a atender, no los demas cedis x,Todas,y = Todas

                //codigo comentado para probar de modo seguro el nuevo sp //030718
                //((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                //return _context.Track_GetReporteEstadiaTractos(Transportista, patente, Cedis, userName).ToList();

                List<Track_GetReporteEstadiaTractos_Result> listaReporteEstadiaTractos = new List<Track_GetReporteEstadiaTractos_Result>();

                Methods_Viajes _obj = new Methods_Viajes();
                List<string> _listaCedis = (from listadoTodosCedisDePoolPlacas in _obj.GetAllCedisPool("", userName, true)
                                            select
                                                listadoTodosCedisDePoolPlacas.Cedis
                             ).Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

                _listaCedis.RemoveAt(0);//quitar el valor "Todas"

                

                //solo hace una busqueda de Todos los cedis si selecciono Todas en los cedis, ignora los demas elementos seleccionados
                if (Cedis.IndexOf("Todas") > -1) {
                    Cedis = "";
                    foreach (var cedis in _listaCedis)
                        Cedis += cedis.ToString() + ",";

                    Cedis = Cedis.Substring(0, Cedis.Length - 1);
                }
                

                List<string> listaIdsCedis = Cedis.Split(',').ToList<string>();

                foreach (string cedis in listaIdsCedis) {
                    
                    InicializarConexion();
                    conexionSQL.Open();

                    EstablecerProcedimientoAlmacenado("Track_GetReporteEstadiaTractos_nuevo_040718");

                    EstablecerParametroProcedimientoAlmacenado(Transportista, "string", "@Transportista");
                    EstablecerParametroProcedimientoAlmacenado(patente, "string", "@patente");
                    //EstablecerParametroProcedimientoAlmacenado(Cedis, "string", "@idCedis");
                    EstablecerParametroProcedimientoAlmacenado(cedis, "string", "@idCedis");
                    EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");
                
                    EjecutarProcedimientoAlmacenado();

                    LlenarLectorDatosSQL();

                    listaReporteEstadiaTractos.AddRange(LlenarListadoReporteEstadiaTractos());

                    conexionSQL.Close();
                    
                    //((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                    //listaReporteEstadiaTractos.AddRange(_context.Track_GetReporteEstadiaTractos(Transportista, patente, cedis, userName).ToList());
                }

                //return listaReporteEstadiaTractos;
                
                
                //evitar que las placas se repitan
                List<Track_GetReporteEstadiaTractos_Result> listaReporteEstadiaTractosSinPlacasRepetidas = new List<Track_GetReporteEstadiaTractos_Result>();

                //ordenando placas para evitar repetidos
                listaReporteEstadiaTractos = listaReporteEstadiaTractos.OrderBy(reporteEstadiaTracto => reporteEstadiaTracto.Placa).ToList();

                string placaTemporal = "";
                foreach (Track_GetReporteEstadiaTractos_Result reporteEstadiaTracto in listaReporteEstadiaTractos)
                    if (placaTemporal != reporteEstadiaTracto.Placa) {
                        placaTemporal = reporteEstadiaTracto.Placa;
                        listaReporteEstadiaTractosSinPlacasRepetidas.Add(reporteEstadiaTracto);
                    }

                //ordenando por cedis
                listaReporteEstadiaTractosSinPlacasRepetidas = listaReporteEstadiaTractosSinPlacasRepetidas.OrderBy(reporteEstadiaTracto => reporteEstadiaTracto.CEDIS).ToList();

                return listaReporteEstadiaTractosSinPlacasRepetidas;
                
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaTractos_Result>();
            }
        }
        /*
        public List<Track_GetReporteEstadiaRemolques_Result> getReporteEstadiaRemolques(string patente, string Cedis, string listCarrier, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteEstadiaRemolques(patente, Cedis, listCarrier, userName).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaRemolques_Result>();
            }
        }
        */
        public List<Track_GetReporteEstadiaRemolques_Result> getReporteEstadiaRemolques(string patente, string Cedis, string listCarrier, string userName)
        {
            try
            {
                List<Track_GetReporteEstadiaRemolques_Result> listaReporteEstadiaRemolques = new List<Track_GetReporteEstadiaRemolques_Result>();

                Methods_Viajes _obj = new Methods_Viajes();
                List<string> _listaCedis = (from listadoTodosCedisDePoolPlacas in _obj.GetAllCedisPool("", userName, true)
                                            select
                                                listadoTodosCedisDePoolPlacas.Cedis
                             ).Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

                _listaCedis.RemoveAt(0);//quitar el valor "Todas"

                //solo hace una busqueda de Todos los cedis si selecciono Todas en los cedis, ignora los demas elementos seleccionados
                if (Cedis.IndexOf("Todas") > -1)
                {
                    Cedis = "";
                    foreach (var cedis in _listaCedis)
                        Cedis += cedis.ToString() + ",";

                    Cedis = Cedis.Substring(0, Cedis.Length - 1);
                }


                List<string> listaIdsCedis = Cedis.Split(',').ToList<string>();

                foreach (string cedis in listaIdsCedis)
                {

                    InicializarConexion();
                    conexionSQL.Open();

                    EstablecerProcedimientoAlmacenado("Track_GetReporteEstadiaRemolques_nuevo_160818");
                    //EstablecerProcedimientoAlmacenado("Track_GetReporteEstadiaRemolques");

                    EstablecerParametroProcedimientoAlmacenado(patente, "string", "@patente");
                    EstablecerParametroProcedimientoAlmacenado(cedis, "string", "@idCedis");
                    
                    EstablecerParametroProcedimientoAlmacenado(listCarrier, "string", "@listCarrier");
                    EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");
                    
                    EjecutarProcedimientoAlmacenado();

                    LlenarLectorDatosSQL();
                    try
                    {
                        listaReporteEstadiaRemolques.AddRange(LlenarListadoReporteEstadiaRemolques());
                    }
                    catch (Exception ex) {
                    }

                    

                    conexionSQL.Close();

                    //((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                    //listaReporteEstadiaTractos.AddRange(_context.Track_GetReporteEstadiaTractos(Transportista, patente, cedis, userName).ToList());
                }

                //return listaReporteEstadiaRemolques;
                
                //evitar que las placas se repitan
                List<Track_GetReporteEstadiaRemolques_Result> listaReporteEstadiaRemolquesSinPlacasRepetidas = new List<Track_GetReporteEstadiaRemolques_Result>();

                //ordenando placas para evitar repetidos
                listaReporteEstadiaRemolques = listaReporteEstadiaRemolques.OrderBy(reporteEstadiaRemolque => reporteEstadiaRemolque.Placa).ToList();

                string placaTemporal = "";
                foreach (Track_GetReporteEstadiaRemolques_Result reporteEstadiaRemolque in listaReporteEstadiaRemolques)
                    if (placaTemporal != reporteEstadiaRemolque.Placa)
                    {
                        placaTemporal = reporteEstadiaRemolque.Placa;
                        listaReporteEstadiaRemolquesSinPlacasRepetidas.Add(reporteEstadiaRemolque);
                    }
                //ordenando por cedis
                listaReporteEstadiaRemolquesSinPlacasRepetidas = listaReporteEstadiaRemolquesSinPlacasRepetidas.OrderBy(reporteEstadiaRemolque => reporteEstadiaRemolque.CEDIS).ToList();

                return listaReporteEstadiaRemolquesSinPlacasRepetidas;
                
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaRemolques_Result>();
            }
        }

        public List<Track_GetReporteEstadiaRemolques_Result> LlenarListadoReporteEstadiaRemolques()
        {
            List<Track_GetReporteEstadiaRemolques_Result> listaReporteEstadiaRemolques = new List<Track_GetReporteEstadiaRemolques_Result>();

            while (lectorDatosSQL.Read())
                listaReporteEstadiaRemolques.Add(LlenarElementoReporteEstadiaRemolque());

            return listaReporteEstadiaRemolques;
        }

        public Track_GetReporteEstadiaRemolques_Result LlenarElementoReporteEstadiaRemolque()
        {
            Track_GetReporteEstadiaRemolques_Result elementoReporteEstadiaRemolque = new Track_GetReporteEstadiaRemolques_Result();

            
             elementoReporteEstadiaRemolque.Placa = lectorDatosSQL["Placa"].ToString();
             elementoReporteEstadiaRemolque.Transportista = lectorDatosSQL["Transportista"].ToString();
             elementoReporteEstadiaRemolque.EstadoEventos = lectorDatosSQL["EstadoEventos"].ToString();
            elementoReporteEstadiaRemolque.FechaEvento = lectorDatosSQL["FechaEvento"].ToString();
            elementoReporteEstadiaRemolque.Antiguedad = lectorDatosSQL["Antiguedad"].ToString();
            elementoReporteEstadiaRemolque.Velocidad = Convert.ToInt32(lectorDatosSQL["Velocidad"]);
            elementoReporteEstadiaRemolque.Latitud = Convert.ToDecimal(lectorDatosSQL["Latitud"]);
            elementoReporteEstadiaRemolque.Longitud = Convert.ToDecimal(lectorDatosSQL["Longitud"]);
            elementoReporteEstadiaRemolque.Referencia = lectorDatosSQL["Referencia"].ToString();
            elementoReporteEstadiaRemolque.Detenido = lectorDatosSQL["Detenido"].ToString();
            elementoReporteEstadiaRemolque.TiempoDetenido = lectorDatosSQL["TiempoDetenido"].ToString();
            elementoReporteEstadiaRemolque.FechaDetencion = lectorDatosSQL["FechaDetencion"].ToString();
            elementoReporteEstadiaRemolque.UltimaEntrada = lectorDatosSQL["UltimaEntrada"].ToString();
            elementoReporteEstadiaRemolque.UltimaSalida = lectorDatosSQL["UltimaSalida"].ToString();
            elementoReporteEstadiaRemolque.EnZona = lectorDatosSQL["EnZona"].ToString();
            elementoReporteEstadiaRemolque.Zona = Convert.ToInt32(lectorDatosSQL["Zona"]);//Nullable
            elementoReporteEstadiaRemolque.TiempoEnZona = lectorDatosSQL["TiempoEnZona"].ToString();
            elementoReporteEstadiaRemolque.CodZonaCercana = Convert.ToInt32(lectorDatosSQL["CodZonaCercana"]);//Nullable
            elementoReporteEstadiaRemolque.NombreZonaCercana = lectorDatosSQL["NombreZonaCercana"].ToString();
            elementoReporteEstadiaRemolque.DistanciaZonaCercana = lectorDatosSQL["DistanciaZonaCercana"].ToString();
            elementoReporteEstadiaRemolque.CEDIS = Convert.ToInt32(lectorDatosSQL["CEDIS"]);//Nullable
            elementoReporteEstadiaRemolque.NombreCEDIS = lectorDatosSQL["NombreCEDIS"].ToString();
            elementoReporteEstadiaRemolque.TipoUnidad = lectorDatosSQL["TipoUnidad"].ToString();
            elementoReporteEstadiaRemolque.FechaReporte = lectorDatosSQL["FechaReporte"].ToString();
            elementoReporteEstadiaRemolque.DesfaseReporte = Convert.ToInt32(lectorDatosSQL["DesfaseReporte"]);//Nullable
            elementoReporteEstadiaRemolque.HorasDetenido = Convert.ToInt32(lectorDatosSQL["HorasDetenido"]);//Nullable
            elementoReporteEstadiaRemolque.UltimaZona = lectorDatosSQL["UltimaZona"].ToString();
        
            elementoReporteEstadiaRemolque.NombreZona = lectorDatosSQL["NombreZona"].ToString();
            elementoReporteEstadiaRemolque.Formato = lectorDatosSQL["Formato"].ToString();
            elementoReporteEstadiaRemolque.EstadoCarga = lectorDatosSQL["EstadoCarga"].ToString();
            elementoReporteEstadiaRemolque.EstadoViaje = lectorDatosSQL["EstadoViaje"].ToString();
            elementoReporteEstadiaRemolque.RegionZona = lectorDatosSQL["RegionZona"].ToString();
            elementoReporteEstadiaRemolque.HorasDetenidoTxt = lectorDatosSQL["HorasDetenidoTxt"].ToString();
        
             

            /*
            elementoReporteEstadiaTracto.Placa = lectorDatosSQL["Placa"].ToString();
            elementoReporteEstadiaTracto.Transportista = lectorDatosSQL["Transportista"].ToString();
            elementoReporteEstadiaTracto.EstadoEventos = lectorDatosSQL["EstadoEventos"].ToString();
            elementoReporteEstadiaTracto.FechaEvento = lectorDatosSQL["FechaEvento"].ToString();
            elementoReporteEstadiaTracto.Antiguedad = lectorDatosSQL["Antiguedad"].ToString();
            elementoReporteEstadiaTracto.Velocidad = Convert.ToInt32(lectorDatosSQL["Velocidad"]);
            elementoReporteEstadiaTracto.Latitud = Convert.ToDecimal(lectorDatosSQL["Latitud"]);
            elementoReporteEstadiaTracto.Longitud = Convert.ToDecimal(lectorDatosSQL["Longitud"]);

            elementoReporteEstadiaTracto.Referencia = lectorDatosSQL["Referencia"].ToString();
            elementoReporteEstadiaTracto.Detenido = lectorDatosSQL["Detenido"].ToString();
            elementoReporteEstadiaTracto.TiempoDetenido = lectorDatosSQL["TiempoDetenido"].ToString();
            elementoReporteEstadiaTracto.FechaDetencion = lectorDatosSQL["FechaDetencion"].ToString();
            elementoReporteEstadiaTracto.UltimaEntrada = lectorDatosSQL["UltimaEntrada"].ToString();
            elementoReporteEstadiaTracto.UltimaSalida = lectorDatosSQL["UltimaSalida"].ToString();
            elementoReporteEstadiaTracto.EnZona = lectorDatosSQL["EnZona"].ToString();

            elementoReporteEstadiaTracto.Zona = Convert.ToInt32(lectorDatosSQL["Zona"]);

            elementoReporteEstadiaTracto.TiempoEnZona = lectorDatosSQL["TiempoEnZona"].ToString();

            elementoReporteEstadiaTracto.CodZonaCercana = Convert.ToInt32(lectorDatosSQL["CodZonaCercana"]);

            elementoReporteEstadiaTracto.NombreZonaCercana = lectorDatosSQL["NombreZonaCercana"].ToString();
            elementoReporteEstadiaTracto.DistanciaZonaCercana = lectorDatosSQL["DistanciaZonaCercana"].ToString();

            elementoReporteEstadiaTracto.CEDIS = Convert.ToInt32(lectorDatosSQL["CEDIS"]);

            elementoReporteEstadiaTracto.NombreCEDIS = lectorDatosSQL["NombreCEDIS"].ToString();
            elementoReporteEstadiaTracto.TipoUnidad = lectorDatosSQL["TipoUnidad"].ToString();
            elementoReporteEstadiaTracto.FechaReporte = lectorDatosSQL["FechaReporte"].ToString();

            elementoReporteEstadiaTracto.DesfaseReporte = Convert.ToInt32(lectorDatosSQL["DesfaseReporte"]);
            elementoReporteEstadiaTracto.HorasDetenido = Convert.ToInt32(lectorDatosSQL["HorasDetenido"]);

            elementoReporteEstadiaTracto.UltimaZona = lectorDatosSQL["UltimaZona"].ToString();
            elementoReporteEstadiaTracto.NombreZona = lectorDatosSQL["NombreZona"].ToString();
            elementoReporteEstadiaTracto.Formato = lectorDatosSQL["Formato"].ToString();
            elementoReporteEstadiaTracto.EstadoCarga = lectorDatosSQL["EstadoCarga"].ToString();
            elementoReporteEstadiaTracto.EstadoViaje = lectorDatosSQL["EstadoViaje"].ToString();
            elementoReporteEstadiaTracto.RegionZona = lectorDatosSQL["RegionZona"].ToString();
            elementoReporteEstadiaTracto.HorasDetenidoTxt = lectorDatosSQL["HorasDetenidoTxt"].ToString();
            */
            return elementoReporteEstadiaRemolque;
        }

        public List<Track_GetReporteEstadiaTractos_Historico_Result> getReporteEstadiaTractos_Historico(DateTime desde, DateTime hasta, string Transportista, string patente, string Cedis, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteEstadiaTractos_Historico(desde, hasta, Transportista, patente, Cedis, userName).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaTractos_Historico_Result>();
            }
        }

        public List<Track_GetReporteEstadiaRemolques_Historico_Result> getReporteEstadiaRemolques_Historico(DateTime desde, DateTime hasta, string patente, string Cedis, string listCarrier, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteEstadiaRemolques_Historico(desde, hasta, patente, Cedis, listCarrier, userName).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaRemolques_Historico_Result>();
            }
        }

        public List<Track_GetReporteComercial_Result> getReporteComercial(DateTime? fecDesde, DateTime? fecHasta, string Cedis)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteComercial(fecDesde, fecHasta, Cedis).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteComercial_Result>();
            }
        }

        public List<Track_GetPosicionesGPS_Viaje_Result> GetPosicionesViaje(long nroTransporte)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

                List<Track_GetPosicionesGPS_Viaje_Result> _listaPosicionesRuta = _context.Track_GetPosicionesGPS_Viaje(nroTransporte).ToList();
                return _listaPosicionesRuta;

            }
            catch (Exception e)
            {

                return new List<Track_GetPosicionesGPS_Viaje_Result>();
            }
        }

        public List<Track_TipoViajes> getTipoViajes()
        {
            try
            {
                List<Track_TipoViajes> _result;

                _result = (from c in _context.Track_TipoViajes select c).ToList();

                return _result;
            }
            catch (Exception ex)
            {
                return new List<Track_TipoViajes>();
            }
        }

        public List<Track_GetNavesUsuario_Result> getNavesUsuario(int idUsuario)
        {
            try
            {
                return _context.Track_GetNavesUsuario(idUsuario).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetNavesUsuario_Result>();
            }
        }

        public List<Track_GetPlacas_Result> getPlacasMaestro(int tipoMovil)
        {
            try
            {
                return _context.Track_GetPlacas(tipoMovil).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetPlacas_Result>();
            }
        }

    }
}
