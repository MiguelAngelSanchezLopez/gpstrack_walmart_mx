﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Data.Objects;

namespace BusinessLayer
{
  public class Methods_Reportes
  {
    private ModelEntities _context = new ModelEntities();

    public List<Track_GetRpt_KmsRecorridos_Result> GetRpt_KmsRecorridos(DateTime? fecDesde, DateTime? fecHasta, string transportista, string patente)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

        List<Track_GetRpt_KmsRecorridos_Result> _listReporte = _context.Track_GetRpt_KmsRecorridos(fecDesde, fecHasta, transportista, patente).ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetRpt_KmsRecorridos_Result>();
      }
    }

    public List<Track_GetRpt_Alertas_Result> GetRpt_Alertas(DateTime? fecDesde, DateTime? fecHasta, string transportista, string patente, string scoreConductor, string rutConductor, string tipoAlerta, string descripcionAlerta, int idFormato, int codigoLocal, string permiso, string estadoViaje, string proveedorGPS)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

        List<Track_GetRpt_Alertas_Result> _listReporte = _context.Track_GetRpt_Alertas(fecDesde, fecHasta, transportista, patente, scoreConductor, rutConductor, tipoAlerta, descripcionAlerta, idFormato, codigoLocal, permiso, estadoViaje, proveedorGPS).ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetRpt_Alertas_Result>();
      }
    }

    public List<Track_GetDashboard_Result> GetDashboard(int year, int month, string transportista)
    {
      try
      {
        List<Track_GetDashboard_Result> _listReporte = _context.Track_GetDashboard(year, month, transportista).ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetDashboard_Result>();
      }
    }

    public List<Usuario> GetConductores(bool _all = false)
    {
      try
      {
        List<Usuario> _listConductores = (from c in _context.Usuario where c.IdPerfil == 2 && c.Estado == 1 select c).ToList();
        if (_all)
        {
          Usuario newItem = new Usuario { Rut = "Todos", DV = "", Nombre = "Todos", Paterno = "" };
          _listConductores.Insert(0, newItem);
        }

        return _listConductores;

      }
      catch (Exception)
      {
        return new List<Usuario>();
      }
    }

    public List<Formato> GetFormatos(bool _all = false)
    {
      try
      {
        List<Formato> _listFormatos = (from c in _context.Formato select c).ToList();
        if (_all)
        {
          Formato newItem = new Formato { Id = 0, Nombre = "Todos" };
          _listFormatos.Insert(0, newItem);
        }

        return _listFormatos;

      }
      catch (Exception)
      {
        return new List<Formato>();
      }
    }

    public List<Local> GetLocales()
    {
      try
      {
        List<Local> _listLocales = (from c in _context.Local select c).ToList();
        Local newItem = new Local { CodigoInterno = 0, IdFormato = 0, NumeroLocal = "Todos" };
        _listLocales.Insert(0, newItem);

        return _listLocales;

      }
      catch (Exception)
      {
        return new List<Local>();
      }
    }

    public List<Track_GetRpt_Alertas_DetalleArea_Result> GetRpt_AlertasDetalleArea(DateTime? fecDesde, DateTime? fecHasta, string transportista, string patente, string scoreConductor, string rutConductor, string tipoAlerta, int idFormato, int codigoLocal, string permiso, string estadoViaje, string proveedorGPS, string puntosPolygon)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

        List<Track_GetRpt_Alertas_DetalleArea_Result> _listReporte = _context.Track_GetRpt_Alertas_DetalleArea(fecDesde, fecHasta, transportista, patente, scoreConductor, rutConductor, tipoAlerta, idFormato, codigoLocal, permiso, estadoViaje, proveedorGPS, puntosPolygon).ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetRpt_Alertas_DetalleArea_Result>();
      }
    }

    public List<Track_GetRpt_MonitoreoDiario_Result> GetRpt_MonitoreoDiario(DateTime? fecDesde, DateTime? fecHasta)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

        List<Track_GetRpt_MonitoreoDiario_Result> _listReporte = _context.Track_GetRpt_MonitoreoDiario(fecDesde, fecHasta).ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetRpt_MonitoreoDiario_Result>();
      }
    }

    public List<Track_GetDashboardAlertas_Result> GetDashboardAlertas()
    {
      try
      {
        List<Track_GetDashboardAlertas_Result> _listReporte = _context.Track_GetDashboardAlertas().ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetDashboardAlertas_Result>();
      }
    }

    public List<Track_GetDashboardUtilizacionFlota_Result> GetDashboardUtilizacionFlota()
    {
      try
      {
        List<Track_GetDashboardUtilizacionFlota_Result> _listReporte = _context.Track_GetDashboardUtilizacionFlota().ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetDashboardUtilizacionFlota_Result>();
      }
    }

    public List<Track_GetDashboardTendenciaIntegracion_Result> GetDashboardTendenciaIntegracion()
    {
      try
      {
        List<Track_GetDashboardTendenciaIntegracion_Result> _listReporte = _context.Track_GetDashboardTendenciaIntegracion().ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetDashboardTendenciaIntegracion_Result>();
      }
    }

    public List<Track_GetDashboardTendenciaAlertas_Result> GetDashboardTendenciaAlertas()
    {
      try
      {
        List<Track_GetDashboardTendenciaAlertas_Result> _listReporte = _context.Track_GetDashboardTendenciaAlertas().ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetDashboardTendenciaAlertas_Result>();
      }
    }

    public List<Track_GetRptTemperatura_Result> GetRpt_Temperatura(DateTime? fecDesde, DateTime? fecHasta, string transportista, string patente, int nroTransporte)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

        List<Track_GetRptTemperatura_Result> _listReporte = _context.Track_GetRptTemperatura(fecDesde, fecHasta, transportista, patente, nroTransporte).ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetRptTemperatura_Result>();
      }
    }

    public List<Track_GetDashboardIntegracion_Result> GetDashboardIntegracion()
    {
      try
      {
        List<Track_GetDashboardIntegracion_Result> _listReporte = _context.Track_GetDashboardIntegracion().ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetDashboardIntegracion_Result>();
      }
    }

    public List<Track_GetEstadoPatente_Result> GetEstadoPatente(string patente)
    {
      try
      {
        List<Track_GetEstadoPatente_Result> _listReporte = _context.Track_GetEstadoPatente(patente).ToList();
        return _listReporte;

      }
      catch (Exception)
      {
        return new List<Track_GetEstadoPatente_Result>();
      }
    }

    public List<Track_ConductoresCEDIS> GetConductoresCEDIS(bool _all = false, int cedis = -1)
    {
      try
      {
        List<Track_ConductoresCEDIS> _listConductores;

        if (cedis == -1)
        {
          _listConductores = (from c in _context.Track_ConductoresCEDIS select c).ToList();
        }
        else
        {
          _listConductores = (from c in _context.Track_ConductoresCEDIS where c.CEDIS == cedis select c).ToList();
        }


        if (_all)
        {
          Track_ConductoresCEDIS newItem = new Track_ConductoresCEDIS { RutConductor = "Todos" };
          _listConductores.Insert(0, newItem);
        }

        return _listConductores;

      }
      catch (Exception)
      {
        return new List<Track_ConductoresCEDIS>();
      }
    }

    public List<Track_GetLastPositions_Result> getUltimoasPosiciones(string pMovil, string pTransportista, int pTipoMovil, int idCedis)
    {
      try
      {
        return _context.Track_GetLastPositions(pMovil, pTransportista, pTipoMovil, idCedis).ToList();
      }
      catch (Exception ex)
      {
        string err = ex.Message.ToString();
        return new List<Track_GetLastPositions_Result>();
      }
    }

  }
}
